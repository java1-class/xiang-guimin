# git项目同时上传到多个git仓库

## git项目代码一次push,同时上传到多个git仓库

### 方法一

#### 在配置文件里边配置

https://img2020.cnblogs.com/blog/1868218/202109/1868218-20210912141931132-1884358623.png

![image-20210912140730299](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210912140730299.png)

#### 配置如下 

```properties
[core]
	repositoryformatversion = 0
	filemode = false
	bare = false
	logallrefupdates = true
	symlinks = false
	ignorecase = true
[remote "origin"]
	url = https://gitee.com/a_xiang_xiang_xiang/comprehensive-training-.git
	fetch = +refs/heads/*:refs/remotes/origin/*
	url = https://gitee.com/java1-class/xiang-guimin.git

[branch "master"]
	remote = origin
	merge = refs/heads/master

```

### 方法二

#### 用命令行配置

1. 命令行输入添加远程仓库的命令：

```properties
git remote set-url --add origin https://gitee.com/java1-class/xiang-guimin.git
```

2. 查看远程仓库地址是否添加成功： 

```properties
git remote -v
```

3. 最后push提交代码

```properties
git push origin master -f
```



### 测试截图

https://img2020.cnblogs.com/blog/1868218/202109/1868218-20210912142012347-1605004884.png

https://img2020.cnblogs.com/blog/1868218/202109/1868218-20210912142048589-2037385791.png

https://img2020.cnblogs.com/blog/1868218/202109/1868218-20210912142121921-690287438.png

![image-20210912141615709](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210912141615709.png)

![image-20210912141719782](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210912141719782.png)

![image-20210912141743534](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210912141743534.png)





+ 至此，就已经完成了整个git项目一次push，多个git仓库地址统一同步。

+ 其实就是在原有本地项目git仓库地址下新增了一个远程git仓库地址。