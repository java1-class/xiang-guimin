# MyBatis

## 学习网址

+ https://mybatis.org/mybatis-3/zh/index.html

## 介绍

+ MyBatis 是一款优秀的持久层框架，它支持定制化 SQL、存储过程以及高级映射。
+ MyBatis 避免了几乎所有的 JDBC 代码和手动设置参数以及获取结果集。
+ MyBatis 可以使用简单的 XML 或注解来配置和映射原生信息，将接口和 Java 的 POJOs(Plain Ordinary Java Object,普通的 Java对象)映射成数据库中的记录。

## 功能架构

我们把Mybatis的功能架构分为三层：

+ (1)API接口层：
  + 提供给外部使用的接口API，
  + 开发人员通过这些本地API来操纵数据库。
  + 接口层一接收到调用请求就会调用数据处理层来完成具体的数据处理。

+ (2)数据处理层：
  + 负责具体的SQL查找、SQL解析、SQL执行和执行结果映射处理等。
  + 它主要的目的是根据调用的请求完成一次数据库操作。

+ (3)基础支撑层：
  + 负责最基础的功能支撑，包括连接管理、事务管理、配置加载和缓存处理，这些都是共用的东西，将他们抽取出来作为最基础的组件。
  + 为上层的数据处理层提供最基础的支撑。

## 框架架构

+ (1)加载配置：
  + 配置来源于两个地方，一处是配置文件，一处是Java代码的注解，
  + 将SQL的配置信息加载成为一个个MappedStatement对象（包括了传入参数映射配置、执行的SQL语句、结果映射配置），存储在内存中。

+ (2)SQL解析：
  + 当API接口层接收到调用请求时，会接收到传入SQL的ID和传入对象（可以是Map、JavaBean或者基本数据类型），
  + Mybatis会根据SQL的ID找到对应的MappedStatement，然后根据传入参数对象对MappedStatement进行解析，
  + 得到最终要执行的SQL语句和参数。
+  (3)SQL执行：
  + 将最终得到的SQL和参数拿到数据库进行执行，
  + 得到操作数据库的结果。
+ (4)结果映射：
  + 将操作数据库的结果按照映射的配置进行转换，
  + 可以转换成HashMap、JavaBean或者基本数据类型，并将最终结果返回。

## ORM的一种实现框架

### 无论是Mybatis、Hibernate都是ORM的一种实现框架，都是对JDBC的一种封装！

![image-20210917230134619](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210917230134619.png)



# MyBatis 动态SQL

+ 动态 SQL 是 MyBatis 的强大特性之一。
  + 如果你使用过 JDBC 或其它类似的框架，你应该能理解根据不同条件拼接 SQL 语句有多痛苦，例如拼接时要确保不能忘记添加必要的空格，还要注意去掉列表最后一个列名的逗号。
  + 利用动态 SQL，可以彻底摆脱这种痛苦。

### 关键字

- if
- choose (when, otherwise)
- trim (where, set)
- foreach






