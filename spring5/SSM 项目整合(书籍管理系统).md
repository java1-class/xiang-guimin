# SSM 项目整合(书籍管理系统)

## 新建项目工程

## 配置Tomcat

![Tomcat](SSM项目整合(书籍管理系统)/Tomcat.png)

## 编写data.sql

```sql
-- 创建数据库
create
database webapp3 charset utf8mb4;
-- 创建用户名、密码
create
user'webapp3'@'localhost'identified by'webapp3';
-- 授权
grant all
on webapp3.*to'webapp3'@'localhost';
-- 用用户名、密码登录
mysql
-uwebapp3 -pwebapp3;

create table user
(
    id       int not null primary key auto_increment,
    username varchar(50), --用户名
    password varchar(50)  -- 密码
);

insert into user
    (`username`, `password`)
values ("xiang", "123456");

create table book
(
    bookId int not null primary key auto_increment, --图书id
    name   varchar(50),                             --图书名
    author varchar(50),                             --图书作者
    press  varchar(50),                             --图书出版社
    price  double(6, 2                              --图书单价
)
    );
insert into book (`name`, `author`, `press`, `price`)
values ("数据库系统实现", "加西亚-莫利纳(Hector Garcia-Molina)", "机械工业出版社", "99.99"),
       ("数据库系统基础教程", "（美）厄尔曼", "机械工业出版社", "89.99"),
       ("Three.js入门指南", "余振华", "人民邮电出版社", "89.99"),
       ("术与道 移动应用UI设计必修课", "张雯莉", "图灵社区", "89.99"),
       ("数学思维导论", "[美] Keith Devlin", "人民邮电出版社", "89.99"),
       ("JSON必知必会", "[美] Lindsay Bassett", "人民邮电出版社", "89.99"),
       ("第一本Docker书", "[澳]詹姆斯•特恩布尔（James Turnbull）", "人民邮电出版社", "89.99"),
       ("HTML5与WebGL编程", "[美] Tony Parisi", "人民邮电出版社", "89.99"),
       ("智能Web算法", "Haralambos Marmanis / Dmitry Babenko", "电子工业出版社", "89.99");

```

## 编写druid.properties

```properties
jdbc.driverClassName=com.mysql.cj.jdbc.Driver
jdbc.url=jdbc:mysql://localhost:3307/webapp3
jdbc.username=webapp3
jdbc.password=webapp3

#这里的每一个key的前面加了jdbc的原因是，如果不加有可能会与电脑的其他路径相冲突
```

## 编写log4j.properties

```properties
#将等级为DEBUG的日志信息输出到console和file这两个目的地，console和file的定义在下面的代码
log4j.rootLogger=DEBUG,console,file

#控制台输出的相关设置
log4j.appender.console = org.apache.log4j.ConsoleAppender 
log4j.appender.console.Target = System.out
log4j.appender.console.Threshold=DEBUG
log4j.appender.console.layout = org.apache.log4j.PatternLayout
log4j.appender.console.layout.ConversionPattern=[%c]-%m%n

#文件输出的相关设置
log4j.appender.file = org.apache.log4j.RollingFileAppender
log4j.appender.file.File=./log/xiang.log
log4j.appender.file.MaxFileSize=10mb
log4j.appender.file.Threshold=DEBUG
log4j.appender.file.layout=org.apache.log4j.PatternLayout
log4j.appender.file.layout.ConversionPattern=[%p][%d{yy-MM-dd}][%c]%m%n
log4j.appender.QCDataWS_LOG.encoding=utf-8

#日志输出级别
log4j.logger.org.mybatis=DEBUG
log4j.logger.java.sql=DEBUG
log4j.logger.java.sql.Statement=DEBUG
log4j.logger.java.sql.ResultSet=DEBUG
log4j.logger.java.sql.PreparedStatement=DEBUG
```

## 编写spring-mybatis.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
       http://www.springframework.org/schema/context
       https://www.springframework.org/schema/context/spring-context.xsd">

    <!--导入druid.properties进行关联-->
    <context:property-placeholder location="classpath:druid.properties"/>

    <!--配置数据库连接池-->
    <bean id="dataSource" class="com.alibaba.druid.pool.DruidDataSource">
        <!--注入连接属性-->
        <property name="driverClassName" value="${jdbc.driverClassName}"/>
        <property name="url" value="${jdbc.url}"/>
        <property name="username" value="${jdbc.username}"/>
        <property name="password" value="${jdbc.password}"/>
    </bean>

    <!--配置SqlSessionFactory对象,该对象可以生成SqlSession对象-->
    <bean id="sqlSessionFactory" class="org.mybatis.spring.SqlSessionFactoryBean">
        <!--连接数据源-->
        <property name="dataSource" ref="dataSource"/>
        <!--绑定Mybatis的配置文件-->
        <property name="configLocation" value="classpath:mybatis-config.xml"/>
    </bean>

    <!--配置MapperScannerConfigurer，将创建的接口的都自动注入到了SqlSessionFactory中，
        给每个接口创建了实例-->
    <bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
        <!--确定哪个包下的接口需要创建实体类-->
        <property name="basePackage" value="com.xiang.mapper"/>
        <!--指定需要注入到哪一个SqlSessionFactory中-->
        <property name="sqlSessionFactoryBeanName" value="sqlSessionFactory"/>
    </bean>

</beans>
```

## 编写spring-service.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context" xmlns:tx="http://www.springframework.org/schema/tx"
       xmlns:jdbc="http://www.springframework.org/schema/jdbc"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
       http://www.springframework.org/schema/context
       https://www.springframework.org/schema/context/spring-context.xsd http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx.xsd http://www.springframework.org/schema/jdbc http://www.springframework.org/schema/jdbc/spring-jdbc.xsd">


    <!-- 配置事务管理器 -->
    <!--1.创建事务管理器-->
    <bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
        <!--注入数据源-->
        <constructor-arg ref="dataSource" />
    </bean>

    <!--2.开启事务注解，需要导入tx命名空间-->
    <!--transaction-manager="transactionManager" : 绑定事务管理器-->
    <tx:annotation-driven transaction-manager="transactionManager"></tx:annotation-driven>

    <jdbc:embedded-database id="dataSource"/>
</beans>
```

## 编写spring-mvc.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
       http://www.springframework.org/schema/context
       https://www.springframework.org/schema/context/spring-context.xsd
       http://www.springframework.org/schema/mvc
       https://www.springframework.org/schema/mvc/spring-mvc.xsd">

    <!--开启注解支持，确定扫描的包-->
    <!--    <context:component-scan base-package="com.xiang"/>-->

    <!--开启mvc的注解支持-->
    <!--<mvc:annotation-driven/>-->
    <!--开启mvc的注解支持，同时解决json乱码问题，这是固定的，记住怎么使用就可以-->
    <mvc:annotation-driven>
        <mvc:message-converters register-defaults="true">
            <bean class="org.springframework.http.converter.StringHttpMessageConverter">
                <constructor-arg value="UTF-8"/>
            </bean>
            <bean class="org.springframework.http.converter.json.MappingJackson2HttpMessageConverter">
                <property name="objectMapper">
                    <bean class="org.springframework.http.converter.json.Jackson2ObjectMapperFactoryBean">
                        <property name="failOnEmptyBeans" value="false"/>
                    </bean>
                </property>
            </bean>
        </mvc:message-converters>
    </mvc:annotation-driven>

    <!-- 让Spring MVC不处理静态资源，比如.css .js .mp4 .jpg .html等 -->
    <!-- 该请求没有对应的@requestmapping时，将该请求交给服务器默认的servlet去处理 -->
    <mvc:default-servlet-handler/>
    <!-- 静态资源处理， css， js， imgs -->
<!--    <mvc:resources mapping="/resource/css/**" location="/resource/css/"/>-->
<!--    <mvc:resources mapping="/resource/js/**" location="/resource/js/"/>-->
<!--    <mvc:resources mapping="/resource/image/**" location="/resource/image/"/>-->

    <!--配置视图解析器-->
    <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver">
        <property name="prefix" value="/"/>
        <property name="suffix" value=".jsp"/>
    </bean>
</beans>
```

## 编写mybatis-config.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>
    <!--    设置-->
    <settings>
        <!--        设置日志-->
        <setting name="logImpl" value="LOG4J"/>
    </settings>
    <!--    别名-->
    <typeAliases>
        <package name="com.xiang"/>
    </typeAliases>
    <!--引入pageHelper分页插件，注意整个plugins标签位置,位置不对会报错！-->
    <plugins>
        <plugin interceptor="com.github.pagehelper.PageInterceptor"></plugin>
    </plugins>
    <!--    关联UserMapper.xml 文件-->
    <mappers>
        <!--        <mapper resource="com/xiana/mapper/UserMapper.xml"/>-->
        <mapper class="com.xiang.mapper.UserMapper"/>
        <mapper class="com.xiang.mapper.BookMapper"/>
    </mappers>


</configuration>
```

## 编写applicationContext.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
       http://www.springframework.org/schema/context
       https://www.springframework.org/schema/context/spring-context.xsd">
    <!--扫描包-->
    <context:component-scan base-package="com.xiang"/>
    <import resource="spring-mvc.xml"/>
    <import resource="spring-service.xml"/>
    <import resource="spring-mybatis.xml"/>


</beans>
```

## 编写User

```java
package com.xiang.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/28 0:58
 */
@Component
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    /**
     * id       int not null primary key auto_increment,
     * username varchar(12), --用户名
     * password varchar(12)  -- 密码
     */
    private int id;
    private String username;
    private String password;
}

```

## 编写Book

```java
package com.xiang.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/28 0:58
 */
@Component
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Book {
    /**
     * bookId int not null primary key auto_increment, --图书id
     * name   varchar(50),                             --图书名
     * author varchar(50),                             --图书作者
     * press  varchar(50),                             --图书出版社
     * price  double(6, 2                              --图书单价
     */
    private int bookId;
    private String name;
    private String author;
    private String press;
    private double price;
}

```

## 编写UserMapper

```java
package com.xiang.mapper;

import com.xiang.pojo.User;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/28 1:04
 */
@Component
public interface UserMapper {
    //登录验证
    User logincheck(User user);
    //注册
    void register(User user);

    //检查是否用户存在
    String checkUserName(String username);
}

```

## 编写UserMapper.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.xiang.mapper.UserMapper">

    <select id="logincheck" parameterType="user" resultType="user">
        select *
        from user
        <where>
            <if test="username != null and username != ''">
                and `username` = #{username}
            </if>
            <if test="password != null and password != ''">
                AND `password` = #{password}
            </if>
        </where>
    </select>

    <select id="checkUserName" parameterType="String" resultType="String">
        select *
        from user
        <where>
            <if test="username != null and username != ''">
                AND `username` = #{username}
            </if>
        </where>
    </select>

    <insert id="register" parameterType="user">
        insert into user(username, password)
            value (#{username},#{password})
    </insert>

</mapper>
```

## 编写BookMapper

```java
package com.xiang.mapper;

import com.xiang.pojo.Book;
import com.xiang.pojo.User;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/28 1:04
 */
@Component
public interface BookMapper {
    /**
     * * bookId int not null primary key auto_increment, --图书id
     * * name   varchar(50),                             --图书名
     * * author varchar(50),                             --图书作者
     * * press  varchar(50),                             --图书出版社
     * * price  double(6, 2                              --图书单价
     *
     * @param book
     * @return
     */

    // 增
    int addBook(Book book);

    // 删
    int deleteBook(int bookId);

    // 改
    int updateBook(Book book);

    // 查所有
    List<Book> queryAllBook();

    // 根据id查一个
    Book queryBookById(int bookId);

    //条件查询
    List<Book> conditionQuery(Book book);

}

```

## 编写BookMapper.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.xiang.mapper.BookMapper">
    <!--    `name`, `author`, `press`, `price`-->
    <!--增-->
    <insert id="addBook" parameterType="book">
        insert into book(name, author, press, price)
        values (#{name}, #{author}, #{press}, #{price})
    </insert>

    <!--删-->
    <delete id="deleteBook" parameterType="int">
        delete
        from book
        where bookId = #{bookId}
    </delete>

    <!--改-->
    <update id="updateBook" parameterType="book">
        update book
        set name=#{name},
            author=#{author},
            press=#{press},
            price=#{price}
        where bookId = #{bookId}
    </update>

    <!--查所有-->
    <select id="queryAllBook" resultType="book">
        select *
        from book
    </select>

    <!--根据id查一个-->
    <select id="queryBookById" resultType="book" parameterType="int">
        select *
        from book
        where bookId = #{bookId}
    </select>

    <!--根据条件查询-->
    <select id="conditionQuery" resultType="book" parameterType="book">
        select * from book
        <where>
            <if test="name != null and name != ''">
                name = #{name}
            </if>
            <if test="author != null and author != ''">
                and author=#{author}
            </if>
            <if test="press != null and press != ''">
                and press=#{press}
            </if>
        </where>
    </select>

</mapper>
```

## 编写UserService

```java
package com.xiang.service;

import com.xiang.pojo.User;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/28 0:58
 */
public interface UserService {
    //登录验证
    User logincheck(User user);
    //注册
    void register(User user);


    //检查是否用户存在
    String checkUserName(String username);
}

```

## 编写UserServiceImpl

```java
package com.xiang.service.impl;

import com.xiang.mapper.UserMapper;
import com.xiang.pojo.User;
import com.xiang.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/28 0:59
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public User logincheck(User user) {
        return userMapper.logincheck(user);

    }

    @Override
    public void register(User user) {
        userMapper.register(user);

    }

    @Override
    public String checkUserName(String username) {
        return userMapper.checkUserName(username);
    }

}

```

## 编写BookService

```xml
package com.xiang.service;

import com.xiang.pojo.Book;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/28 0:58
 */
public interface BookService {
    // 增
    int addBook(Book book);

    // 删
    int deleteBook(int bookId);

    // 改
    int updateBook(Book book);

    // 查所有
    List<Book> queryAllBook();

    // 根据id查一个
    Book queryBookById(int bookId);

    //条件查询
    List<Book> conditionQuery(Book book);
}

```

## 编写BookServiceImpl

```java
package com.xiang.service.impl;

import com.xiang.mapper.BookMapper;
import com.xiang.pojo.Book;
import com.xiang.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/28 0:59
 */
@Service
public class BookServiceImpl  implements BookService {
    @Autowired
    private BookMapper bookMapper;
    @Override
    public int addBook(Book book) {
        return bookMapper.addBook(book);
    }

    @Override
    public int deleteBook(int bookId) {
        return bookMapper.deleteBook(bookId);
    }

    @Override
    public int updateBook(Book book) {
        return bookMapper.updateBook(book);
    }

    @Override
    public List<Book> queryAllBook() {
        return bookMapper.queryAllBook();
    }

    @Override
    public Book queryBookById(int bookId) {
        return bookMapper.queryBookById(bookId);
    }

    @Override
    public List<Book> conditionQuery(Book book) {
        return bookMapper.conditionQuery(book);
    }
}

```

## 编写UserController

```java
package com.xiang.controller;

import com.xiang.pojo.User;
import com.xiang.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/28 1:00
 */
@Controller
public class UserController {
    @Autowired
    private UserService userService;

    //登录验证
    @RequestMapping("/login")
    public String login(@RequestParam("username") String username,
                        @RequestParam("password") String password, Model model, HttpServletRequest request, User user) {
        HttpSession session = request.getSession();
        session.setAttribute("UserSession", user);
//        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        if (userService.logincheck(user) != null) {
            model.addAttribute("username", username);
            return "redirect:/list";
        } else {
            model.addAttribute("error", "账号或密码错误");
            return "index";
        }
    }

    //注册
    @RequestMapping("/register")
    public String register(@RequestParam String username,@RequestParam String password, Model model) {
        User user = new User();
        System.out.println("执行");
        String s = userService.checkUserName(username);
        System.out.println("s"+s);
        if(s==null){
            user.setUsername(username);
            user.setPassword(password);
            userService.register(user);
            return "redirect:/list";
        }else {
            model.addAttribute("fail","该用户存在");
            System.out.println("该用户存在");
            return "/register";
        }

    }

    //注销
    @RequestMapping(value = "/logout")
    public String getLogout(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (session != null) {
            session.removeAttribute("UserSession");
        }
        return "index";

    }

}

```

## 编写BookController

```java
package com.xiang.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xiang.pojo.Book;
import com.xiang.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * book: xiang
 * Date: 2021/9/28 1:01
 */
@Controller
public class BookController {
    @Autowired
    private BookService bookServiceImpl;

    //查询所有并分页显示
    @GetMapping("/list")
    public String queryAllbook(@RequestParam(value = "page", defaultValue = "1") Integer page, Model model){
        //获取指定页数据，每页显示5条数据
        PageHelper.startPage(page, 5);
        //紧跟的第一个select方法被分页
        List<Book> books = bookServiceImpl.queryAllBook();
        model.addAttribute("books",books);
        //使用PageInfo包装数据 navigatePages表示导航标签的数量
        PageInfo pageInfo = new PageInfo(books, 5);
        model.addAttribute("pageInfo", pageInfo);
        return null;
    }



    @PostMapping("/add")
    public String addbook(Book book) {
        int i = bookServiceImpl.addBook(book);
        return "redirect:/list";  // redirect 重定向/跳转
    }

    @GetMapping("/queryBookById/{bookId}")
    public String querybookById(@PathVariable("bookId") Integer bookId, Model model) {
        Book book = bookServiceImpl.queryBookById(bookId);
        model.addAttribute("book",book);
        return "update";  //转发（默认为）
    }

    @PostMapping("/update")
    public String queryBookById(Book book) {
        System.out.println(book);
        int i = bookServiceImpl.updateBook(book);
        return "redirect:/list";
    }

    @GetMapping("/delete/{bookId}")
    public String deletebook(@PathVariable("bookId") int bookId) {
        int i = bookServiceImpl.deleteBook(bookId);
        return "redirect:/list";
    }

    @GetMapping("/conditionQuery")
    public String deletebook(Book book,Model model) {
        List<Book> books = bookServiceImpl.conditionQuery(book);
        model.addAttribute("books",books);
        return "list";
    }




}
```



## 配置web.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">

    <!--配置DispatcherServlet，是SpringMVC的核心，又名前端控制器；用于拦截符合配置的 url 请求、分发请求-->
    <servlet>
        <servlet-name>springmvc</servlet-name>
        <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
        <!--DispatcherServlet需要绑定Spring的配置文件-->
        <init-param>
            <param-name>contextConfigLocation</param-name>
            <param-value>classpath:applicationContext.xml</param-value>
        </init-param>
        <!--设置启动级别1：随着服务器的启动而启动-->
        <load-on-startup>1</load-on-startup>
    </servlet>
    <servlet-mapping>
        <servlet-name>springmvc</servlet-name>
        <url-pattern>/</url-pattern>
<!--        <url-pattern>*.css</url-pattern>-->
<!--        <url-pattern>*.js</url-pattern>-->
<!--        <url-pattern>*.png</url-pattern>-->
<!--        <url-pattern>*.gif</url-pattern>-->
<!--        <url-pattern>*.jpg</url-pattern>-->
    </servlet-mapping>

    <!--配置过滤器，防止乱码-->
    <filter>
        <filter-name>encoding</filter-name>
        <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
        <init-param>
            <param-name>encoding</param-name>
            <param-value>utf-8</param-value>
        </init-param>
    </filter>
    <filter-mapping>
        <filter-name>encoding</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>

</web-app>
```

## 编写登录页CSS样式 login.css

```css
html,
body {
    height: 100%;
}

body {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-align: center;
    align-items: center;
    padding-top: 40px;
    padding-bottom: 40px;
    background-color: #f5f5f5;
}

.form-signin {
    width: 100%;
    max-width: 330px;
    padding: 15px;
    margin: auto;
}

.form-signin .checkbox {
    font-weight: 400;
}

.form-signin .form-control {
    position: relative;
    box-sizing: border-box;
    height: auto;
    padding: 10px;
    font-size: 16px;
}

.form-signin .form-control:focus {
    z-index: 2;
}

.form-signin input[type="email"] {
    margin-bottom: -1px;
    border-bottom-right-radius: 0;
    border-bottom-left-radius: 0;
}

.form-signin input[type="password"] {
    margin-bottom: 10px;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
}

```

## 登录页头像

![images](SSM项目整合(书籍管理系统)/images.png)

## 登录页 index.jsp

```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resource/css/login.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body class="text-center">
<form class="form-signin" action="<%=request.getContextPath()%>/login">
    <img class="mb-4" src="<%=request.getContextPath()%>/resource/image/images.png" alt="" width="100" height="100">
    <h1 class="h3 mb-3 font-weight-normal">请登录</h1>
    <label for="username" class="sr-only">账号</label>
    <input type="text" name="username" id="username" class="form-control" placeholder="账号" required="" autofocus="">
    <label for="password" class="sr-only">密码</label>
    <input type="password" name="password" id="password" class="form-control" placeholder="密码" required="">
    <div class="checkbox mb-3">
        <label>
            <input type="checkbox" value="remember-me"> 记住我
        </label>
    </div>
    <%--    <button class="btn btn-lg btn-primary btn-block" type="submit">登录</button>--%>
    <%--    <a href="register.jsp" class="btn btn-lg btn-primary btn-block" type="submit">注册</a>--%>

    <a href="register.jsp" type="submit" class="btn btn-primary btn-lg">注册</a>
    <button type="submit" class="btn btn-primary btn-lg">登录</button>

    <p class="mt-5 mb-3 text-muted">© 2021</p>
</form>
</body>
</html>

```

## 注册页 register.jsp （与登录页一样）

```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resource/css/login.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script>
        /* function register(){
             let username = document.getElementById("username").value;
             let password  = document.getElementById("password").value;
             alert(username+""+""+password);
             window.location.href="http://localhost:8001/register/";
         }*/
    </script>
</head>


<c:if test="${fail != null} ">
    <div class="alert alert-danger" role="alert">
        <c:out value="${fail}"/>
        <c:remove var="fail" scope="session"/>
    </div>
</c:if>


<div style="color:red;font-size: 30px">
    <c:out value="${fail}"/>
</div>
<body class="text-center">
<form class="form-signin" action="${pageContext.request.contextPath}/register">
    <img class="mb-4" src="<%=request.getContextPath()%>/resource/image/images.png" alt="" width="100" height="100">
    <h1 class="h3 mb-3 font-weight-normal">请登录</h1>
    <label for="username" class="sr-only">账号</label>
    <input type="text" name="username" id="username" class="form-control" placeholder="账号" required="" autofocus="">
    <label for="password" class="sr-only">密码</label>
    <input type="password" name="password" id="password" class="form-control" placeholder="密码" required="">
    <div class="checkbox mb-3">
        <label>
            <input type="checkbox" value="remember-me"> 记住我
        </label>
    </div>
    <%--    <a href="index.jsp" class="btn btn-lg btn-primary btn-block" type="submit">登录</a>--%>
    <%--    <button class="btn btn-lg btn-primary btn-block" type="submit">注册</button>--%>


    <button type="submit" class="btn btn-primary btn-lg">注册</button>
    <a href="index.jsp" class="btn btn-primary btn-lg">登录</a>
    <p class="mt-5 mb-3 text-muted">© 2021</p>
</form>
</body>
</html>

```

## 书箱列表展示页 list.jsp

```jsp
<%--
  Created by IntelliJ IDEA.
  User: Xiang
  Date: 2021/9/28
  Time: 1:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>list</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>
<div style="width: 1200px;margin: auto">
    <div>
        <h4 align="center">书籍列表展示</h4>
    </div>
    <div>
        <form style="float: left;margin: 5px;" class="form-inline"
              action="${pageContext.request.contextPath}/conditionQuery" method="get">
            <div class="form-group">
                <label for="name">图书名</label>
                <input type="text" name="name" class="form-control" id="name">
            </div>
            <div class="form-group">
                <label for="author">图书作者</label>
                <input type="text" name="author" class="form-control" id="author">
            </div>
            <div class="form-group">
                <label for="press">图书出版社</label>
                <input type="text" name="press" class="form-control" id="press">
            </div>

            <button type="submit" class="btn btn-info">查询</button>
        </form>


        <a style="float: right;margin: 5px;" class="btn btn-primary"
           href="${pageContext.request.contextPath}/list">书籍列表页</a>
        <a style="float: right;margin: 5px;" class="btn btn-primary" href="${pageContext.request.contextPath}/add.jsp">添加书籍</a>
        <a style="float: right;margin: 5px;" class="btn btn-primary"
           href="${pageContext.request.contextPath}/logout">注销</a>
    </div>

    <%--    <table class="table table-hover">--%>
    <%--
        bookId int not null primary key auto_increment, --图书id
        name   varchar(50),                             --图书名
        author varchar(50),                             --图书作者
        press  varchar(50),                             --图书出版社
        price  double(6, 2                              --图书单价
        --%>
    <table class="table table-striped">
        <tr>
            <th>序号</th>
            <th>图书名</th>
            <th>图书作者</th>
            <th>图书出版社</th>
            <th>图书单价</th>
            <th>&nbsp;&nbsp;&nbsp;&nbsp;操作</th>
        </tr>
        <c:forEach items="${books}" var="book" varStatus="s">
            <tr>
                <td>${s.count}</td>
                <td>${book.name}</td>
                <td>${book.author}</td>
                <td>${book.press}</td>
                <td>${book.price}</td>
                <td>
                    <a href="${pageContext.request.contextPath}/delete/${book.bookId}" class="btn btn-danger">删除</a>
                    <a href="${pageContext.request.contextPath}/queryBookById/${book.bookId}"
                       class="btn btn-success">修改</a>
                </td>
            </tr>
        </c:forEach>

    </table>

    <%--分页导航标签--%>
    <div class="row">
        <div class="col-md-6">
            第${pageInfo.pageNum}页，共${pageInfo.pages}页，共${pageInfo.total}条记录
        </div>
        <div class="col-md-6 offset-md-4">
            <nav aria-label="Page navigation example">
                <ul class="pagination pagination-sm">
                    <li class="page-item"><a class="page-link"
                                             href="${pageContext.request.contextPath}/list?page=1">首页</a></li>
                    <c:if test="${pageInfo.hasPreviousPage}">
                        <li class="page-item"><a class="page-link"
                                                 href="${pageContext.request.contextPath}/list?page=${pageInfo.pageNum-1}">上一页</a>
                        </li>
                    </c:if>
                    <c:forEach items="${pageInfo.navigatepageNums}" var="page">
                        <c:if test="${page==pageInfo.pageNum}">
                            <li class="page-item active"><a class="page-link" href="#">${page}</a></li>
                        </c:if>
                        <c:if test="${page!=pageInfo.pageNum}">
                            <li class="page-item"><a class="page-link"
                                                     href="${pageContext.request.contextPath}/list?page=${page}">${page}</a>
                            </li>
                        </c:if>
                    </c:forEach>
                    <c:if test="${pageInfo.hasNextPage}">
                        <li class="page-item"><a class="page-link"
                                                 href="${pageContext.request.contextPath}/list?page=${pageInfo.pageNum+1}">下一页</a>
                        </li>
                    </c:if>
                    <li class="page-item"><a class="page-link"
                                             href="${pageContext.request.contextPath}/list?page=${pageInfo.pages}">末页</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>


</div>
</body>
</html>

```

## 添加书籍页 add.jsp

```jsp
<%--
  Created by IntelliJ IDEA.
  User: Xiang
  Date: 2021/9/28
  Time: 9:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>add</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>
<div style="width: 1200px;margin: auto">
    <div>
        <h4 align="center">添加书箱</h4>
    </div>
    <%--
    bookId int not null primary key auto_increment, --图书id
            name   varchar(50),                             --图书名
            author varchar(50),                             --图书作者
            press  varchar(50),                             --图书出版社
            price  double(6, 2                              --图书单价
    --%>
    <form class="form-horizontal" action="${pageContext.request.contextPath}/add" method="post">
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">图书名</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="name" name="name" placeholder="图书名">
            </div>
        </div>

        <div class="form-group">
            <label for="author" class="col-sm-2 control-label">图书作者</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="author" name="author" placeholder="图书作者">
            </div>
        </div>

        <div class="form-group">
            <label for="press" class="col-sm-2 control-label">图书出版社</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="press" name="press" placeholder="图书出版社">
            </div>
        </div>
         <div class="form-group">
            <label for="price" class="col-sm-2 control-label">图书单价</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="price" name="price" placeholder="图书单价">
            </div>
        </div>


        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-success">确认添加</button>

                <input type="button" onclick="history.go(-1)" class="btn btn-warning" value="取消添加"/>
            </div>
        </div>
    </form>
</div>
</body>
</html>

```

## 修改书籍信息页 update.jsp

```jsp
<%--
  Created by IntelliJ IDEA.
  User: Xiang
  Date: 2021/9/28
  Time: 10:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>update</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>
<div style="width: 1200px;margin: auto">
    <div>
        <h4 align="center">修改书籍信息</h4>
    </div>
    <%--
      bookId int not null primary key auto_increment, --图书id
              name   varchar(50),                             --图书名
              author varchar(50),                             --图书作者
              press  varchar(50),                             --图书出版社
              price  double(6, 2                              --图书单价
      --%>
    <form class="form-horizontal" action="${pageContext.request.contextPath}/update" method="post">
        <input name="bookId" value="${book.bookId}" hidden />
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">图书名</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="name" name="name" placeholder="姓名" value="${book.name}">
            </div>
        </div>

        <div class="form-group">
            <label for="author" class="col-sm-2 control-label">图书作者</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="author" name="author" placeholder="年龄" value="${book.author}">
            </div>
        </div>

        <div class="form-group">
            <label for="press" class="col-sm-2 control-label">图书出版社</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="press" name="press" placeholder="press" value="${book.press}">
            </div>
        </div>
        <div class="form-group">
            <label for="price" class="col-sm-2 control-label">图书单价</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="price" name="price" placeholder="price" value="${book.price}">
            </div>
        </div>


        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">确认修改</button>

                <input type="button" onclick="history.go(-1)" class="btn btn-warning" value="取消修改"/>
            </div>
        </div>
    </form>
</div>
</body>
</html>

```

# 书籍管理系统 项目工程运行截图

## 登录页面

![登录页面](SSM项目整合(书籍管理系统)/登录页面.png)

## 注册页面

![注册页面](SSM项目整合(书籍管理系统)/注册页面.png)

## 书籍列表展示页面

![书籍列表展示页面](SSM项目整合(书籍管理系统)/书籍列表展示页面.png)

## 添加书籍页面

![添加书籍页面](SSM项目整合(书籍管理系统)/添加书籍页面.png)

## 修改书籍页面

![修改书籍页面](SSM项目整合(书籍管理系统)/修改书籍页面.png)

## 项目结构目录

![项目结构目录](SSM项目整合(书籍管理系统)/项目结构目录.png)

# 至此 本项目工程完结