# SSM 项目整合 (用户管理系统)

## 1. 新建项目工程

## 2. 配置Tomcat 

![Tomcat](ssm/Tomcat.png)

## 3. 导入 pom.xml 依赖

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.xiang</groupId>
    <artifactId>ssm</artifactId>
    <version>1.0-SNAPSHOT</version>
    <name>ssm</name>
    <packaging>war</packaging>

    <properties>
        <maven.compiler.target>1.8</maven.compiler.target>
        <maven.compiler.source>1.8</maven.compiler.source>
        <junit.version>5.7.1</junit.version>
    </properties>

    <dependencies>
        <!--Junit-->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
        </dependency>
        <!--数据库驱动-->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>8.0.25</version>
        </dependency>
        <!-- 数据库连接池 -->
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>druid</artifactId>
            <version>1.2.6</version>
        </dependency>

        <!--Servlet - JSP -->
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>servlet-api</artifactId>
            <version>2.5</version>
        </dependency>
        <dependency>
            <groupId>javax.servlet.jsp</groupId>
            <artifactId>jsp-api</artifactId>
            <version>2.2</version>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>jstl</artifactId>
            <version>1.2</version>
        </dependency>

        <!--Mybatis-->
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis</artifactId>
            <version>3.5.7</version>
        </dependency>
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis-spring</artifactId>
            <version>2.0.6</version>
        </dependency>

        <!--Spring-->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
            <version>5.3.10</version>
        </dependency>
<!--        <dependency>-->
<!--            <groupId>org.springframework</groupId>-->
<!--            <artifactId>spring-webmvc</artifactId>-->
<!--            <version>5.2.0.RELEASE</version>-->
<!--        </dependency>-->
<!--        <dependency>-->
<!--            <groupId>org.springframework</groupId>-->
<!--            <artifactId>spring-jdbc</artifactId>-->
<!--            <version>5.1.8.RELEASE</version>-->
<!--        </dependency>-->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-jdbc</artifactId>
            <version>5.3.9</version>
        </dependency>

        <!--lombok-->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.18</version>
            <scope>provided</scope>
        </dependency>

        <!--jackson-->
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>2.13.0-rc2</version>
        </dependency>

        <dependency>
            <groupId>log4j</groupId>
            <artifactId>log4j</artifactId>
            <version>1.2.17</version>
        </dependency>

        <dependency>
<!--            分页-->
            <groupId>com.github.pagehelper</groupId>
            <artifactId>pagehelper</artifactId>
            <version>5.1.2</version>
        </dependency>

    </dependencies>

    <!--导入插件，防止静态资源过滤-->
    <build>
        <resources>
            <resource>
                <directory>src/main/java</directory>
                <includes>
                    <include>**/*.properties</include>
                    <include>**/*.xml</include>
                </includes>
                <filtering>false</filtering>
            </resource>
            <resource>
                <directory>src/main/resources</directory>
                <includes>
                    <include>**/*.properties</include>
                    <include>**/*.xml</include>
                </includes>
                <filtering>false</filtering>
            </resource>
        </resources>

        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                    <encoding>utf-8</encoding>
                </configuration>
            </plugin>
        </plugins>

    </build>
</project>
```

## 4. 编写 data.sql 文件

```sql
-- 创建数据库
create
database webapp2 charset utf8mb4;
-- 创建用户名、密码
create
user'webapp2'@'localhost'identified by'webapp2';
-- 授权
grant all
on webapp2.*to'webapp2'@'localhost';
-- 用用户名、密码登录
mysql
-uwebapp2 -pwebapp2;
-- 创建表

-- `id`,`username`,`password`,`name`,`gender`,`age`,`address`,`qq`,`email`
create table user
(
    id       int primary key auto_increment,
    username varchar(50),
    password varchar(50),
    name     varchar(20) not null,
    gender   varchar(5),
    age      int,
    address  varchar(32),
    qq       varchar(20),
    email    varchar(50)
);

insert into user (`username`, `password`, `name`, `gender`, `age`, `address`, `qq`, `email`)
values ("xiang", "123456", "小向", "男", "19", "四川成都", "2103717751", "xiangdada1@gmail.com"),
       ("xiang", "123456", "小一", "男", "19", "四川成都", "2103717751", "xiangdada1@gmail.com"),
       ("xiang", "123456", "小二", "男", "19", "四川成都", "2103717751", "xiangdada1@gmail.com"),
       ("xiang", "123456", "小三", "男", "19", "四川成都", "2103717751", "xiangdada1@gmail.com"),
       ("xiang", "123456", "小四", "男", "19", "四川成都", "2103717751", "xiangdada1@gmail.com"),
       ("xiang", "123456", "小五", "男", "19", "四川成都", "2103717751", "xiangdada1@gmail.com"),
       ("xiang", "123456", "小六", "男", "19", "四川成都", "2103717751", "xiangdada1@gmail.com"),
       ("xiang", "123456", "小七", "男", "19", "四川成都", "2103717751", "xiangdada1@gmail.com"),
       ("xiang", "123456", "小八", "男", "19", "四川成都", "2103717751", "xiangdada1@gmail.com"),
       ("xiang", "123456", "小九", "男", "19", "四川成都", "2103717751", "xiangdada1@gmail.com");
```

## 5. 编写 druid.properties 文件

```properties
jdbc.driverClassName=com.mysql.cj.jdbc.Driver
jdbc.url=jdbc:mysql://localhost:3307/webapp2
jdbc.username=webapp1
jdbc.password=webapp1

#这里的每一个key的前面加了jdbc的原因是，如果不加有可能会与电脑的其他路径相冲突
```

## 6. 编写 log4j.properties 文件

```properties
#将等级为DEBUG的日志信息输出到console和file这两个目的地，console和file的定义在下面的代码
log4j.rootLogger=DEBUG,console,file

#控制台输出的相关设置
log4j.appender.console = org.apache.log4j.ConsoleAppender 
log4j.appender.console.Target = System.out
log4j.appender.console.Threshold=DEBUG
log4j.appender.console.layout = org.apache.log4j.PatternLayout
log4j.appender.console.layout.ConversionPattern=[%c]-%m%n

#文件输出的相关设置
log4j.appender.file = org.apache.log4j.RollingFileAppender
log4j.appender.file.File=./log/xiang.log
log4j.appender.file.MaxFileSize=10mb
log4j.appender.file.Threshold=DEBUG
log4j.appender.file.layout=org.apache.log4j.PatternLayout
log4j.appender.file.layout.ConversionPattern=[%p][%d{yy-MM-dd}][%c]%m%n
log4j.appender.QCDataWS_LOG.encoding=utf-8

#日志输出级别
log4j.logger.org.mybatis=DEBUG
log4j.logger.java.sql=DEBUG
log4j.logger.java.sql.Statement=DEBUG
log4j.logger.java.sql.ResultSet=DEBUG
log4j.logger.java.sql.PreparedStatement=DEBUG
```

## 7. 编写 spring-mvc.xml 文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
       http://www.springframework.org/schema/context
       https://www.springframework.org/schema/context/spring-context.xsd
       http://www.springframework.org/schema/mvc
       https://www.springframework.org/schema/mvc/spring-mvc.xsd">

    <!--开启注解支持，确定扫描的包-->
<!--    <context:component-scan base-package="com.xiang"/>-->

    <!--开启mvc的注解支持-->
    <!--<mvc:annotation-driven/>-->
    <!--开启mvc的注解支持，同时解决json乱码问题，这是固定的，记住怎么使用就可以-->
    <mvc:annotation-driven>
        <mvc:message-converters register-defaults="true">
            <bean class="org.springframework.http.converter.StringHttpMessageConverter">
                <constructor-arg value="UTF-8"/>
            </bean>
            <bean class="org.springframework.http.converter.json.MappingJackson2HttpMessageConverter">
                <property name="objectMapper">
                    <bean class="org.springframework.http.converter.json.Jackson2ObjectMapperFactoryBean">
                        <property name="failOnEmptyBeans" value="false"/>
                    </bean>
                </property>
            </bean>
        </mvc:message-converters>
    </mvc:annotation-driven>

    <!-- 让Spring MVC不处理静态资源，比如.css .js .mp4 .jpg .html等 -->
    <mvc:default-servlet-handler/>

    <!--配置视图解析器-->
    <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver">
        <property name="prefix" value="/"/>
        <property name="suffix" value=".jsp"/>
    </bean>
</beans>
```

## 8. 编写 spring-mybatis.xml 文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
       http://www.springframework.org/schema/context
       https://www.springframework.org/schema/context/spring-context.xsd">

    <!--导入druid.properties进行关联-->
    <context:property-placeholder location="classpath:druid.properties"/>

    <!--配置数据库连接池-->
    <bean id="dataSource" class="com.alibaba.druid.pool.DruidDataSource">
        <!--注入连接属性-->
        <property name="driverClassName" value="${jdbc.driverClassName}"/>
        <property name="url" value="${jdbc.url}"/>
        <property name="username" value="${jdbc.username}"/>
        <property name="password" value="${jdbc.password}"/>
    </bean>

    <!--配置SqlSessionFactory对象,该对象可以生成SqlSession对象-->
    <bean id="sqlSessionFactory" class="org.mybatis.spring.SqlSessionFactoryBean">
        <!--连接数据源-->
        <property name="dataSource" ref="dataSource"/>
        <!--绑定Mybatis的配置文件-->
        <property name="configLocation" value="classpath:mybatis-config.xml"/>
    </bean>

    <!--配置MapperScannerConfigurer，将创建的接口的都自动注入到了SqlSessionFactory中，
        给每个接口创建了实例-->
    <bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
        <!--确定哪个包下的接口需要创建实体类-->
        <property name="basePackage" value="com.xiang.mapper"/>
        <!--指定需要注入到哪一个SqlSessionFactory中-->
        <property name="sqlSessionFactoryBeanName" value="sqlSessionFactory"/>
    </bean>

</beans>
```

## 9. 编写 spring-service.xml 文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context" xmlns:tx="http://www.springframework.org/schema/tx"
       xmlns:jdbc="http://www.springframework.org/schema/jdbc"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
       http://www.springframework.org/schema/context
       https://www.springframework.org/schema/context/spring-context.xsd http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx.xsd http://www.springframework.org/schema/jdbc http://www.springframework.org/schema/jdbc/spring-jdbc.xsd">


    <!-- 配置事务管理器 -->
    <!--1.创建事务管理器-->
    <bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
        <!--注入数据源-->
        <constructor-arg ref="dataSource" />
    </bean>

    <!--2.开启事务注解，需要导入tx命名空间-->
    <!--transaction-manager="transactionManager" : 绑定事务管理器-->
    <tx:annotation-driven transaction-manager="transactionManager"></tx:annotation-driven>

    <jdbc:embedded-database id="dataSource"/>
</beans>
```

## 10. 编写 mybatis-config.xml 文件

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>
    <!--    设置-->
    <settings>
        <!--        设置日志-->
        <setting name="logImpl" value="LOG4J"/>
    </settings>
    <!--    别名-->
    <typeAliases>
        <package name="com.xiang"/>
    </typeAliases>
    <!--引入pageHelper分页插件，注意整个plugins标签位置,位置不对会报错！-->
    <plugins>
        <plugin interceptor="com.github.pagehelper.PageInterceptor"></plugin>
    </plugins>
    <!--    关联UserMapper.xml 文件-->
    <mappers>
        <!--        <mapper resource="com/xiana/mapper/UserMapper.xml"/>-->
        <mapper class="com.xiang.mapper.UserMapper"/>
    </mappers>


</configuration>

```

## 11. 编写 applicationContext.xml 文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
       http://www.springframework.org/schema/context
       https://www.springframework.org/schema/context/spring-context.xsd">
    <!--扫描包-->
        <context:component-scan base-package="com.xiang"/>
    <import resource="spring-mvc.xml"/>
    <import resource="spring-service.xml"/>
    <import resource="spring-mybatis.xml"/>


</beans>
```

## 12. 编写 pojo.User

```java
package com.xiang.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/26 19:04
 */
@Component
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private Integer id;
    private String username;
    private String password;
    private String name;
    private String gender;
    private Integer age;
    private String address;
    private String qq;
    private String email;
}

```

## 13. 编写 mapper.UserMapper

```java
package com.xiang.mapper;


import com.xiang.pojo.User;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/26 19:05
 */
@Component
public interface UserMapper {

    // 增
    int addUser(User user);

    // 删
    int deleteUser(int id);

    // 改
    int updateUser(User user);

    // 查所有
    List<User> queryAllUser();

    // 根据id查一个
    User queryUserById(int id);

    //条件查询
    List<User> conditionQuery(User user);



}

```

## 14. 编写 mapper.UserMapper.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.xiang.mapper.UserMapper">
    <!--增-->
    <insert id="addUser" parameterType="user">
        insert into user(name, gender, age, address, qq, email)
        values (#{name}, #{gender}, #{age}, #{address}, #{qq}, #{email})
    </insert>

    <!--删-->
    <delete id="deleteUser" parameterType="int">
        delete
        from user
        where id = #{id}
    </delete>

    <!--改-->
    <update id="updateUser" parameterType="user">
        update user
        set name=#{name},
            gender=#{gender},
            age=#{age},
            address=#{address},
            qq=#{qq},
            email=#{email}
        where id = #{id}
    </update>

    <!--查所有-->
    <select id="queryAllUser" resultType="user">
        select *
        from user
    </select>

    <!--根据id查一个-->
    <select id="queryUserById" resultType="user" parameterType="int">
        select *
        from user
        where id = #{id}
    </select>

    <!--根据条件查询-->
    <select id="conditionQuery" resultType="user" parameterType="user">
        select * from user
        <where>
            <if test="name != null and name != ''">
                name = #{name}
            </if>
            <if test="gender != null and gender != ''">
                and gender = #{gender}
            </if>
            <if test="address != null and address != ''">
                and address = #{address}
            </if>
        </where>
    </select>

</mapper>
```

## 15. 编写 service.UserService

```java
package com.xiang.service;

import com.xiang.pojo.User;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/26 19:07
 */

public interface UserService {
    // 增
    int addUser(User user);

    // 删
    int deleteUser(int id);

    // 改
    int updateUser(User user);

    // 查所有
    List<User> queryAllUser();

    // 根据id查一个
    User queryUserById(int id);

    //条件查询
    List<User> conditionQuery(User user);

}

```

## 16. 编写 service.impl.UserServiceImpl

```java
package com.xiang.service.impl;


import com.xiang.mapper.UserMapper;
import com.xiang.pojo.User;
import com.xiang.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/26 19:07
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    public int addUser(User user) {
        return userMapper.addUser(user);
    }

    public int deleteUser(int id) {
        return userMapper.deleteUser(id);
    }

    public int updateUser(User user) {
        return userMapper.updateUser(user);
    }

    public List<User> queryAllUser() {
        return userMapper.queryAllUser();
    }

    public User queryUserById(int id) {
        return userMapper.queryUserById(id);
    }

    @Override
    public List<User> conditionQuery(User user) {
        return userMapper.conditionQuery(user);
    }
    
}

```

## 17. 编写 controller.UserControllerTest

```java
package com.xiang.controller;

import com.xiang.pojo.User;
import com.xiang.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/26 19:06
 */
@Controller
@RequestMapping("/user")
public class UserControllerTest {

    @Autowired
    private UserServiceImpl userServiceImpl;

    @GetMapping("/queryAllUser")
    @ResponseBody
    public List<User> queryAllUser(){
        List<User> users = userServiceImpl.queryAllUser();
        for (User user : users) {
            System.out.println(user);
        }
        return users;
    }
}

```

## 18. 编写 service.UserServiceTest (测试类)

```java
package com.xiang.service;

import com.xiang.pojo.User;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/26 19:49
 */
public class UserServiceTest {
    @Test
    public  void  queryAllUser(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserService service = context.getBean("userServiceImpl", UserService.class);
        List<User> list = service.queryAllUser();
        for (User user : list) {
            System.out.println(user);
        }
    }

}

```

## 19. 编写 controller.UserController

```java
package com.xiang.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xiang.pojo.User;
import com.xiang.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/26 21:00
 */
@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserServiceImpl userServiceImpl;

//    @GetMapping("/list")
//    public String queryAllUser(Model model) {
//        List<User> users = userServiceImpl.queryAllUser();
//        model.addAttribute("users", users);
//        return "list";
//    }

    //查询所有并分页显示
    @GetMapping("/list")
    public String queryAllUser(@RequestParam(value = "page", defaultValue = "1") Integer page, Model model){
        //获取指定页数据，每页显示8条数据
        PageHelper.startPage(page, 6);
        //紧跟的第一个select方法被分页
        List<User> users = userServiceImpl.queryAllUser();
        model.addAttribute("users",users);
        //使用PageInfo包装数据 navigatePages表示导航标签的数量
        PageInfo pageInfo = new PageInfo(users, 5);
        model.addAttribute("pageInfo", pageInfo);
        return "list";
    }



    @PostMapping("/add")
    public String addUser(User user) {
        int i = userServiceImpl.addUser(user);
        return "redirect:/user/list";  // redirect 重定向/跳转
    }

    @GetMapping("/queryUserById/{id}")
    public String queryUserById(@PathVariable("id") Integer id, Model model) {
        User user = userServiceImpl.queryUserById(id);
        model.addAttribute("user",user);
        return "update";  //转发（默认为）
    }

    @PostMapping("/update")
    public String queryUserById(User user) {
        int i = userServiceImpl.updateUser(user);
        return "redirect:/user/list";
    }

    @GetMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") int id) {
        int i = userServiceImpl.deleteUser(id);
        return "redirect:/user/list";
    }

    @GetMapping("/conditionQuery")
    public String deleteUser(User user,Model model) {
        List<User> users = userServiceImpl.conditionQuery(user);
        model.addAttribute("users",users);
        return "list";
    }
    
}

```

## 20. 配置 web.xml 文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">

    <!--配置DispatcherServlet，是SpringMVC的核心，又名前端控制器；用于拦截符合配置的 url 请求、分发请求-->
    <servlet>
        <servlet-name>springmvc</servlet-name>
        <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
        <!--DispatcherServlet需要绑定Spring的配置文件-->
        <init-param>
            <param-name>contextConfigLocation</param-name>
            <param-value>classpath:applicationContext.xml</param-value>
        </init-param>
        <!--设置启动级别1：随着服务器的启动而启动-->
        <load-on-startup>1</load-on-startup>
    </servlet>
    <servlet-mapping>
        <servlet-name>springmvc</servlet-name>
        <url-pattern>/</url-pattern>
    </servlet-mapping>

    <!--配置过滤器，防止乱码-->
    <filter>
        <filter-name>encoding</filter-name>
        <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
        <init-param>
            <param-name>encoding</param-name>
            <param-value>utf-8</param-value>
        </init-param>
    </filter>
    <filter-mapping>
        <filter-name>encoding</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
</web-app>
```

## 21. 编写 index.jsp

```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="zh-CN">
<head>
    <title>index</title>
</head>
<body>
<div >${user.name}欢迎您</div>
<div align="center">
    <a href="${pageContext.request.contextPath}/user/list"
       style="text-decoration:none;font-size:33px;color: bisque">查询所有用户信息
    </a>
</div>
</body>
</html>
```

## 22. 编写 list.jsp

```jsp
<%--
  Created by IntelliJ IDEA.
  User: Xiang
  Date: 2021/9/26
  Time: 21:02
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>list</title>

    <!-- 新 Bootstrap 核心 CSS 文件 -->
    <link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.0/css/bootstrap.min.css">
    <!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
    <script src="http://cdn.bootcss.com/jquery/1.11.1/jquery.min.js"></script>
    <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
    <script src="http://cdn.bootcss.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

</head>
<body>

<div style="width: 1200px;margin: auto">
    <div>
        <h4 align="center">用户展示</h4>
    </div>
    <div>
        <form style="float: left;margin: 5px;" class="form-inline"
              action="${pageContext.request.contextPath}/user/conditionQuery" method="get">
            <div class="form-group">
                <label for="name">姓名</label>
                <input type="text" name="name" class="form-control" id="name">
            </div>
            <div class="form-group">
                <label for="gender">性别</label>
                <input type="text" name="gender" class="form-control" id="gender">
            </div>
            <div class="form-group">
                <label for="address">城市</label>
                <input type="text" name="address" class="form-control" id="address">
            </div>

            <button type="submit" class="btn btn-info">查询</button>
        </form>

        <a style="float: right;margin: 5px;" class="btn btn-primary"
           href="${pageContext.request.contextPath}/index.jsp">欢迎页面</a>
        <a style="float: right;margin: 5px;" class="btn btn-primary"
           href="${pageContext.request.contextPath}/user/list">用户列表页</a>
        <a style="float: right;margin: 5px;" class="btn btn-primary" href="${pageContext.request.contextPath}/add.jsp">添加联系人</a>
    </div>

    <%--    <table class="table table-hover">--%>
    <table class="table table-striped">
        <tr>
            <th>序号</th>
            <th>ID</th>
            <th>账号</th>
            <th>密码</th>
            <th>姓名</th>
            <th>性别</th>
            <th>年龄</th>
            <th>城市</th>
            <th>qq</th>
            <th>邮箱</th>
            <th>操作</th>
        </tr>
        <c:forEach items="${users}" var="user" varStatus="s">
            <tr>
                <td>${s.count}</td>
                <td>${user.id}</td>
                <td>${user.username}</td>
                <td>***</td>
                <td>${user.name}</td>
                <td>${user.gender}</td>
                <td>${user.age}</td>
                <td>${user.address}</td>
                <td>${user.qq}</td>
                <td>${user.email}</td>
                <td>
                    <a href="${pageContext.request.contextPath}/user/delete/${user.id}" class="btn btn-danger">删除</a>
                    <a href="${pageContext.request.contextPath}/user/queryUserById/${user.id}" class="btn btn-success">修改</a>
                </td>
            </tr>
        </c:forEach>

    </table>

    <%--分页导航标签--%>
    <div class="row">
        <div class="col-md-6">
            第${pageInfo.pageNum}页，共${pageInfo.pages}页，共${pageInfo.total}条记录
        </div>
        <div class="col-md-6 offset-md-4">
            <nav aria-label="Page navigation example">
                <ul class="pagination pagination-sm">
                    <li class="page-item"><a class="page-link"
                                             href="${pageContext.request.contextPath}/user/list?page=1">首页</a></li>
                    <c:if test="${pageInfo.hasPreviousPage}">
                        <li class="page-item"><a class="page-link"
                                                 href="${pageContext.request.contextPath}/user/list?page=${pageInfo.pageNum-1}">上一页</a>
                        </li>
                    </c:if>
                    <c:forEach items="${pageInfo.navigatepageNums}" var="page">
                        <c:if test="${page==pageInfo.pageNum}">
                            <li class="page-item active"><a class="page-link" href="#">${page}</a></li>
                        </c:if>
                        <c:if test="${page!=pageInfo.pageNum}">
                            <li class="page-item"><a class="page-link"
                                                     href="${pageContext.request.contextPath}/user/list?page=${page}">${page}</a>
                            </li>
                        </c:if>
                    </c:forEach>
                    <c:if test="${pageInfo.hasNextPage}">
                        <li class="page-item"><a class="page-link"
                                                 href="${pageContext.request.contextPath}/user/list?page=${pageInfo.pageNum+1}">下一页</a>
                        </li>
                    </c:if>
                    <li class="page-item"><a class="page-link"
                                             href="${pageContext.request.contextPath}/user/list?page=${pageInfo.pages}">末页</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>


</div>

</body>
</html>


```

## 23. 编写 add.jsp

```jsp
<%--
  Created by IntelliJ IDEA.
  User: Xiang
  Date: 2021/9/26
  Time: 21:03
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>add</title>

    <!-- 新 Bootstrap 核心 CSS 文件 -->
    <link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.0/css/bootstrap.min.css">
    <!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
    <script src="http://cdn.bootcss.com/jquery/1.11.1/jquery.min.js"></script>
    <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
    <script src="http://cdn.bootcss.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

</head>
<body>
<div style="width: 1200px;margin: auto" >
    <div>
        <h4 align="center">添加用户</h4>
    </div>
    <form class="form-horizontal" action="${pageContext.request.contextPath}/user/add" method="post">
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">姓名</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="name" name="name" placeholder="姓名">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">性别</label>
            <div class="col-sm-10" style="margin-top: 3px;">
                男：<input type="radio" name="gender" value="男" checked="checked" >
                女：<input type="radio" name="gender" value="女">
            </div>
        </div>
        <div class="form-group">
            <label for="age" class="col-sm-2 control-label">年龄</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="age" name="age" placeholder="年龄">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">城市</label>
            <div class="col-sm-1">
                <select class="form-control input-sm" name="address">
                    <option value="成都">成都</option>
                    <option value="重庆">重庆</option>
                    <option value="北京">北京</option>
                    <option value="上海">上海</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="qq" class="col-sm-2 control-label">QQ</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="qq" name="qq" placeholder="QQ">
            </div>
        </div>
        <div class="form-group">
            <label for="email" class="col-sm-2 control-label">邮箱</label>
            <div class="col-sm-10">
                <input type="email" class="form-control" id="email" name="email" placeholder="邮箱">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-success">确认添加</button>

                <input type="button" onclick="history.go(-1)"  class="btn btn-warning"  value="取消添加"/>
            </div>
        </div>
    </form>
</div>

</body>
</html>


```

## 24. 编写 update.jsp

```jsp
<%--
  Created by IntelliJ IDEA.
  User: Xiang
  Date: 2021/9/26
  Time: 21:03
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>update</title>

    <!-- 新 Bootstrap 核心 CSS 文件 -->
    <link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.0/css/bootstrap.min.css">
    <!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
    <script src="http://cdn.bootcss.com/jquery/1.11.1/jquery.min.js"></script>
    <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
    <script src="http://cdn.bootcss.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

</head>
<body>
<div style="width: 1200px;margin: auto" >
    <div>
        <h4 align="center">修改用户</h4>
    </div>
    <form class="form-horizontal" action="${pageContext.request.contextPath}/user/update" method="post">
        <input name="id" value="${user.id}" hidden>
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">姓名</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="name" name="name" placeholder="姓名" value="${user.name}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">性别</label>
            <div class="col-sm-10" style="margin-top: 3px;">
                <c:if test="${user.gender == '男'}">
                    男：<input type="radio" name="gender" value="男" checked="checked" >
                    女：<input type="radio" name="gender" value="女">
                </c:if>
                <c:if test="${user.gender == '女'}">
                    男：<input type="radio" name="gender" value="男" >
                    女：<input type="radio" name="gender" value="女" checked="checked">
                </c:if>
            </div>
        </div>
        <div class="form-group">
            <label for="age" class="col-sm-2 control-label">年龄</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="age" name="age" placeholder="年龄" value="${user.age}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">城市</label>
            <div class="col-sm-1">
                <select class="form-control input-sm" name="address">
                    <c:if test="${user.address == '成都'}">
                        <option value="成都" selected>成都</option>
                        <option value="重庆">重庆</option>
                        <option value="北京">北京</option>
                        <option value="上海">上海</option>
                    </c:if>
                    <c:if test="${user.address == '重庆'}">
                        <option value="成都">成都</option>
                        <option value="重庆" selected>重庆</option>
                        <option value="北京">北京</option>
                        <option value="上海">上海</option>
                    </c:if>
                    <c:if test="${user.address == '北京'}">
                        <option value="成都">成都</option>
                        <option value="重庆">重庆</option>
                        <option value="北京" selected>北京</option>
                        <option value="上海">上海</option>
                    </c:if>
                    <c:if test="${user.address == '上海'}">
                        <option value="成都">成都</option>
                        <option value="重庆">重庆</option>
                        <option value="北京">北京</option>
                        <option value="上海" selected>上海</option>
                    </c:if>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="qq" class="col-sm-2 control-label">QQ</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="qq" name="qq" placeholder="QQ" value="${user.qq}">
            </div>
        </div>
        <div class="form-group">
            <label for="email" class="col-sm-2 control-label">邮箱</label>
            <div class="col-sm-10">
                <input type="email" class="form-control" id="email" name="email" placeholder="邮箱" value="${user.email}">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">确认修改</button>

                <input type="button" onclick="history.go(-1)"  class="btn btn-warning"  value="取消修改"/>
            </div>
        </div>
    </form>
</div>

</body>
</html>

```



# 运行 测试 类 

## controller 代码

```java
@Controller
@RequestMapping("/user")
public class UserControllerTest {

    @Autowired
    private UserServiceImpl userServiceImpl;

    @GetMapping("/queryAllUser")
    @ResponseBody
    public List<User> queryAllUser(){
        List<User> users = userServiceImpl.queryAllUser();
        for (User user : users) {
            System.out.println(user);
        }
        return users;
    }
}
```

## 前端 运行 结果 

![测试类前端运行](ssm/测试类前端运行.png)

![测试类控制台运行](ssm/测试类控制台运行.png)



## 测试类 代码

```java
@Test
    public  void  queryAllUser(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserService service = context.getBean("userServiceImpl", UserService.class);
        List<User> list = service.queryAllUser();
        for (User user : list) {
            System.out.println(user);
        }
    }
```

## 运行结果

![测试类控制台运行2](ssm/测试类控制台运行2.png)

## SSM 项目整合结构目录

![ssm项目整合结构目录.png](ssm/ssm项目整合结构目录.png)

# 项目运行截图

## 欢迎页

![欢迎页](ssm/欢迎页.png)

## 用户列表展示页

![用户列表展示页](ssm/用户列表展示页.png)

## 修改用户页

![修改用户页](ssm/修改用户页.png)

## 添加用户页

![添加用户页](ssm/添加用户页.png)