# spring

## set注入与构造器注入

### 1、配置依赖

```xml
    <dependencies>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
            <version>5.3.9</version>
        </dependency>
    </dependencies>
```

### 2、编写dao.UserDao类

```java
package com.xiang.dao;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/22 10:55
 */
public interface UserDao {
    String show();
}
```

### 3、编写dao.impl.UserDaoimpl1/UserDaoimpl2类

```java
package com.xiang.dao.impl;

import com.xiang.dao.UserDao;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/22 10:56
 */
public class UserDaoImpl1 implements UserDao {
    @Override
    public String show() {
        return "查询用户信息1";
    }
}
```

```java
package com.xiang.dao.impl;

import com.xiang.dao.UserDao;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/22 10:56
 */
public class UserDaoImpl2 implements UserDao {
    @Override
    public String show() {
        return "查询用户信息2";
    }
}
```

### 4、编写service.UserService类

```java
package com.xiang.service;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/22 10:57
 */
public interface UserService {
String show();
}
```

### 5、编写service.UserServiceImpl类

```java
package com.xiang.service;

import com.xiang.dao.UserDao;
import com.xiang.dao.impl.UserDaoImpl1;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/22 10:57
 */
public class UserServiceImpl implements  UserService{
//    private UserDaoImpl1 userDaoImpl1 = new UserDaoImpl1();
    private UserDao userDao;

    /**
     * set 注入
     * @param userDao
     */
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    /**
     * 构造方法
     * 构造器 注入
     * @param userDao
     */
   public  UserServiceImpl(UserDao userDao){
        this.userDao = userDao;
   }

    /**
     * 调方法
     * @return
     */
    @Override
    public String show() {
        return userDao.show();
    }
}
```

### 6、编写测试类

```java
package com.xiang.service;

import com.xiang.dao.impl.UserDaoImpl1;
import com.xiang.dao.impl.UserDaoImpl2;
import org.junit.Test;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/22 11:00
 */
public class UserServiceImplTest {

    /**
     * 普通方式查询
     */
//    @Test
//    public void Test01() {
//        UserServiceImpl service = new UserServiceImpl();
//        System.out.println(service.show());
//    }

    /**
     * set 注入
     */
//    @Test
//    public void Test02() {
//        UserServiceImpl service = new UserServiceImpl();
//        service.setUserDao(new UserDaoImpl2());
//        System.out.println(service);
//        System.out.println(service.show());
//    }

    /**
     * set 注入
     */
//    @Test
//    public void Test03() {
//        UserServiceImpl service = new UserServiceImpl();
//        service.setUserDao(new UserDaoImpl1());
//        System.out.println(service);
//        System.out.println(service.show());
//    }

    /**
     * 构造器 注入
     */
    @Test
    public void Test04() {
        UserServiceImpl service = new UserServiceImpl(new UserDaoImpl2());
        System.out.println(service);
        System.out.println(service.show());
    }
}
```

### 7、运行结果 

![两种注入.png](img/两种注入.png)

