# AJAX

ajax（Web数据交互方式）

+ Ajax即**A**synchronous **J**avascript **A**nd **X**ML（异步JavaScript和[XML](https://baike.baidu.com/item/XML/86251)）
+ 在 2005年被Jesse James Garrett提出的新术语，用来描述一种使用现有技术集合的‘新’方法，
+ 包括: [HTML](https://baike.baidu.com/item/HTML/97049) 或 [XHTML](https://baike.baidu.com/item/XHTML/316621), CSS, [JavaScript](https://baike.baidu.com/item/JavaScript/321142), [DOM](https://baike.baidu.com/item/DOM/50288), XML, [XSLT](https://baike.baidu.com/item/XSLT/1330564), 以及最重要的[XMLHttpRequest](https://baike.baidu.com/item/XMLHttpRequest/6788735)。
+ 使用Ajax技术网页应用能够快速地将增量更新呈现在[用户界面](https://baike.baidu.com/item/用户界面/6582461)上，而不需要重载（刷新）整个页面，这使得程序能够更快地回应用户的操作。

## **ajax所包含的技术**

  大家都知道ajax并非一种新的技术，而是几种原有技术的结合体。它由下列技术组合而成。

+ 使用CSS和XHTML来表示。

+ 使用DOM模型来交互和动态显示。

+ 使用XMLHttpRequest来和服务器进行异步通信。

+ 使用javascript来绑定和调用。

在上面几中技术中，除了XmlHttpRequest对象以外，其它所有的技术都是基于web标准并且已经得到了广泛使用的，XMLHttpRequest虽然目前还没有被W3C所采纳，但是它已经是一个事实的标准，因为目前几乎所有的主流浏览器都支持它。



## ajax 工作原理

https://img2020.cnblogs.com/blog/1868218/202109/1868218-20210917223234409-268663252.png

![image-20210915090703524](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210915090703524.png)

+ Ajax的原理简单来说通过XmlHttpRequest对象来向服务器发异步请求，从服务器获得数据，然后用javascript来操作DOM而更新页面。

+ 这其中最关键的一步就是从服务器获得请求数据。

+ XMLHttpRequest是ajax的核心机制，它是在IE5中首先引入的，是一种支持异步请求的技术。

+ 简单的说，也就是javascript可以及时向服务器提出请求和处理响应，而不阻塞用户。达到无刷新的效果。

+ XMLHttpRequest这个对象的属性。

  ​	它的属性有：

  + onreadystatechange 每次状态改变所触发事件的事件处理程序。
  + responseText   从服务器进程返回数据的字符串形式。
  + responseXML   从服务器进程返回的DOM兼容的文档数据对象。
  + status      从服务器返回的数字代码，比如常见的404（未找到）和200（已就绪）
  + status Text    伴随状态码的字符串信息
  + readyState    对象状态值

## **ajax的优点**

1. 最大的一点是**页面无刷新**，在**页面内与服务器通信，给用户的体验非常好。**

2. 使用**异步方式与服务器通信，不需要打断用户的操作**，具有更加迅速的响应能力。

3. 可以**把以前一些服务器负担的工作转嫁到客户端**，利用客户端闲置的能力来处理，减轻服务器和带宽的负担，节约空间和宽带租用成本。并且减轻服务器的负担，ajax的原则是“按需取数据”，可以最大程度的减少冗余请求，和响应对服务器造成的负担。

4. **基于标准化的并被广泛支持的技术，**不需要下载插件或者小程序。

## **ajax的缺点**

ajax的缺陷都是它先天所产生的

1. ajax干掉了back按钮，即对浏览器后退机制的破坏。
   	+ 后退按钮是一个标准的web站点的重要功能，但是它没法和js进行很好的合作。
   	+ 这是ajax所带来的一个比较严重的问题，因为用户往往是希望能够通过后退来取消前一次操作的。
2. 安全问题
   + 对IT企业带来了新的安全威胁，ajax技术就如同对企业数据建立了一个直接通道。
   + 这使得开发者在不经意间会暴露比以前更多的数据和服务器逻辑。
   + ajax的逻辑可以对客户端的安全扫描技术隐藏起来，允许黑客从远端服务器上建立新的攻击。
   + 还有ajax也难以避免一些已知的安全弱点，诸如跨站点脚步攻击、SQL注入攻击和基于credentials的安全漏洞等。
3. 对搜索引擎的支持比较弱。
4. 破坏了程序的异常机制。





## **$.ajax()方法详解**　　　

### **1.url**: 

要求为String类型的参数，（默认为当前页地址）发送请求的地址。

### **2.type**: 

要求为String类型的参数，请求方式（post或get）默认为get。

	+  注意其他http请求方法，例如put和delete也可以使用，但仅部分浏览器支持。

### **3.timeout**: 

要求为Number类型的参数，设置请求超时时间（毫秒）。此设置将覆盖$.ajaxSetup()方法的全局设置。

### **4.async**: 

要求为Boolean类型的参数，默认设置为true，所有请求均为异步请求。

如果需要发送同步请求，请将此选项设置为false。

	+ 注意，同步请求将锁住浏览器，用户其他操作必须等待请求完成才可以执行。

### **5.cache**: 

要求为Boolean类型的参数，默认为true（当dataType为script时，默认为false），设置为false将不会从浏览器缓存中加载请求信息。

### **6.data**: 

要求为Object或String类型的参数，发送到服务器的数据。

如果已经不是字符串，将自动转换为字符串格式。

get请求中将附加在url后。防止这种自动转换，可以查看processData选项。

+ 对象必须为key/value格式，例如{foo1:"bar1",foo2:"bar2"}转换为&foo1=bar1&foo2=bar2。
+ 如果是数组，JQuery将自动为不同值对应同一个名称。例如{foo:["bar1","bar2"]}转换为&foo=bar1&foo=bar2。

### **7.dataType**: 

要求为String类型的参数，预期服务器返回的数据类型。

如果不指定，JQuery将自动根据http包mime信息返回responseXML或responseText，并作为回调函数参数传递。可用的类型如下：

+ xml：返回XML文档，可用JQuery处理。
+ html：返回纯文本HTML信息；包含的script标签会在插入DOM时执行。
+ script：返回纯文本JavaScript代码。不会自动缓存结果。除非设置了cache参数。
  + 注意在远程请求时（不在同一个域下），所有post请求都将转为get请求。
+ json：返回JSON数据。
+ jsonp：JSONP格式。使用SONP形式调用函数时，
  + 例如myurl?callback=?，JQuery将自动替换后一个“?”为正确的函数名，以执行回调函数。
+ text：返回纯文本字符串。

### **8.beforeSend**：

要求为Function类型的参数，发送请求前可以修改XMLHttpRequest对象的函数，例如添加自定义HTTP头。

在beforeSend中如果返回false可以取消本次ajax请求。

XMLHttpRequest对象是惟一的参数。

```java
function(XMLHttpRequest){
        this;  //调用本次ajax请求时传递的options参数
      }
```

### **9.complete**：

要求为Function类型的参数，请求完成后调用的回调函数（请求成功或失败时均调用）。

+ 参数：XMLHttpRequest对象和一个描述成功请求类型的字符串。

```java
function(XMLHttpRequest, textStatus){
       this;  //调用本次ajax请求时传递的options参数
      }
```

### **10.success**：要求为Function类型的参数，请求成功后调用的回调函数，有两个参数。

+ 由服务器返回，并根据dataType参数进行处理后的数据。
+ 描述状态的字符串。

     ```java
     function(data, textStatus){
           //data可能是xmlDoc、jsonObj、html、text等等
           this; //调用本次ajax请求时传递的options参数
          }
     ```

### **11.error**:

要求为Function类型的参数，请求失败时被调用的函数。

+ 该函数有3个参数
  + 即XMLHttpRequest对象、
  + 错误信息、
  + 捕获的错误对象(可选)。
+ ajax事件函数如下：

```java
function(XMLHttpRequest, textStatus, errorThrown){
     //通常情况下textStatus和errorThrown只有其中一个包含信息
     this;  //调用本次ajax请求时传递的options参数
    }
```

### **12.cotentType**：

要求为String类型的参数，当发送信息至服务器时，内容编码类型默认为"application/x-www-form-urlencoded"。该默认值适合大多数应用场合。

### **13.dataFilter**：

要求为Function类型的参数，给Ajax返回的原始数据进行预处理的函数。

+ 提供data和type两个参数。
+ data是Ajax返回的原始数据，type是调用jQuery.ajax时提供的dataType参数。
+ 函数返回的值将由jQuery进一步处理。

```java
function(data, type){
        //返回处理后的数据
        return data;
     }
```

### **14.global**：

要求为Boolean类型的参数，默认为true。

+ 表示是否触发全局ajax事件。
+ 设置为false将不会触发全局ajax事件，ajaxStart或ajaxStop可用于控制各种ajax事件。

### **15.ifModified**：

要求为Boolean类型的参数，默认为false。

+ 仅在服务器数据改变时获取新数据。
+ 服务器数据改变判断的依据是Last-Modified头信息。默认值是false，即忽略头信息。

### **16.jsonp**：

要求为String类型的参数，在一个jsonp请求中重写回调函数的名字。

+ 该值用来替代在"callback=?"这种GET或POST请求中URL参数里的"callback"部分，
+ 例如{jsonp:'onJsonPLoad'}会导致将"onJsonPLoad=?"传给服务器。

### **17.username**：

要求为String类型的参数，用于响应HTTP访问认证请求的用户名。

### **18.password**：

要求为String类型的参数，用于响应HTTP访问认证请求的密码。

### **19.processData**：

要求为Boolean类型的参数，默认为true。

+ 默认情况下，发送的数据将被转换为对象（从技术角度来讲并非字符串）
+ 以配合默认内容类型"application/x-www-form-urlencoded"。
+ 如果要发送DOM树信息或者其他不希望转换的信息，请设置为false。

### **20.scriptCharset**：

要求为String类型的参数，只有当请求时dataType为"jsonp"或者"script"，并且type是GET时才会用于强制修改字符集(charset)。

+ 通常在本地和远程的内容编码不同时使用。
