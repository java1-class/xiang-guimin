# springMVC

## maven常用命令

1. mvn archetype:generate 创建Maven项目

2. mvn compile 编译源代码

3. mvn deploy 发布项目

4. mvn test-compile 编译测试源代码

5. mvn test 运行应用程序中的单元测试

6. mvn site 生成项目相关信息的网站

7. mvn clean 清除项目目录中的生成结果

8. mvn package 根据项目生成的jar

9. mvn install 在本地Repository中安装jar

10. mvn eclipse:eclipse 生成eclipse项目文件

11. mvn[jetty](https://baike.baidu.com/item/jetty):run 启动jetty服务

12. mvn[tomcat](https://baike.baidu.com/item/tomcat):run 启动tomcat服务

13. mvn clean package -Dmaven.test.skip=true:清除以前的包后重新打包，跳过测试类

```pseudocode
C:\Users\Xiang>echo %M2_HOME%
D:\Apache-Tomcat\apache-maven-3.6.3

C:\Users\Xiang>mvn -v
Apache Maven 3.6.3 (cecedd343002696d0abb50b32b541b8a6ba2883f)
Maven home: D:\Apache-Tomcat\apache-maven-3.6.3\bin\..
Java version: 1.8.0_281, vendor: Oracle Corporation, runtime: D:\JDK\jdk1.8.0\jre
Default locale: zh_CN, platform encoding: GBK
OS name: "windows 10", version: "10.0", arch: "amd64", family: "windows"
```

## maven 测试连接数据库

```java
package com.xiang.lesson01;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBTest {

    private static String URL = "jdbc:mysql://localhost:3307/webapp1?serverTimezone=UTC&useUnicode=true&characterEncoding=utf-8&useSSL=false";
    private static String DriverClass = "com.mysql.cj.jdbc.Driver";
    private static String UserName = "webapp1";
    private static String PassWord = "webapp1";
    private static Connection connection = null;

    public static Connection getConnection() {
        try {
            Class.forName(DriverClass);
            connection = DriverManager.getConnection(URL, UserName, PassWord);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return connection;
    }

//    public  void  closeRerource(Connection connection){
//        try {
//            connection.close();
//        } catch (SQLException throwables) {
//            throwables.printStackTrace();
//        }
//    }

    public static void closeRerource() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        connection = DBTest.getConnection();
        if (connection != null) {
            System.out.println("连接成功");
        } else {
            System.out.println("连接失败");
        }
        closeRerource();
    }

}



```

## 配置依赖

- 在pom.xml 文件下配置

```java
<dependency>
     <groupId>mysql</groupId>
     <artifactId>mysql-connector-java</artifactId>
     <version>8.0.25</version>
</dependency>
```

## 运行结果

https://img2020.cnblogs.com/blog/1868218/202109/1868218-20210909135612133-667526075.png

![](https://img2020.cnblogs.com/blog/1868218/202109/1868218-20210909135612133-667526075.png)

## MVC 设计模式

https://img2020.cnblogs.com/blog/1868218/202109/1868218-20210909141940833-2082234107.png

![image-20210909101150846](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210909101150846.png)

###  什么是 MVC

![image-20210911163619596](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210911163619596.png)

 +   **Model（模型）**

     + 是应用程序中用于处理应用程序数据逻辑的部分。　　

     + 通常模型对象负责在数据库中存取数据。

 +   **View（视图）**

     + 是应用程序中处理数据显示的部分。

     + 通常视图是依据模型数据创建的。

 +   **Controller（控制器）**

     + 是应用程序中处理用户交互的部分。

     + 通常控制器负责从视图读取数据，控制用户输入，并向模型发送数据。

       

MVC是一个框架模式，它强制性的使应用程序的输入、处理和输出分开。

**使用MVC应用程序被分成三个核心部件：模型、视图、控制器。**

它们各自处理自己的任务。**最典型的MVC就是JSP + servlet + javabean的模式**

**Model:常用javabean去实现，通过各种类来对数据库的数据进行获取，并封装在对象当中。**

**View:常用JSP来实现，通过可直接观察的JSP页面来展示我们从数据库中获取的数据。**

**Controller:常用servlet来实现，通过servlet来获取经过javabean包装过的对象（已存入数据库中的数据），然后再发送数据传输到JSP界面。**



### 什么是 JavaBean

JavaBean 是一种JAVA语言写成的可重用组件。

为写成JavaBean，类必须是具体的和公共的，并且具有无参数的构造器。

JavaBean 通过提供符合一致性设计模式的公共方法将内部域暴露成员属性，long和class方法获取。

众所周知，属性名称符合这种模式，其他Java 类可以通过自省机制发现和操作这些JavaBean 的属性。



**JavaBean一般由三部分组成：**

1. 属性

2. 方法

3. 事件

实际上JavaBean是一种Java类，通过封装属性和方法成为具有某种功能或者处理某个业务的对象，简称bean。



由于javabean是基于 java语言的，因此javabean不依赖平台，具有以下特点：

1. 可以实现代码的重复利用
2. 易编写、易维护、易使用
3. 可以在任何安装了Java运行环境的平台上的使用，而不需要重新编译

### 总结：

粗俗的讲，javabean就是一个简单的类，但是这个类要有set()、get()方法，还有类中的属性都要是私有化的（private），方法是公有化的（public），还有就是要有一个无参的构造方法，如果你要设置了带参数的构造方法，那么请把无参的构造方法重新写一次，因为默认的构造方法会被带参数的构造方法覆盖掉。

​	



## 搭建springMVC 框架

1. 编写 mvc-dispatcher-servlet.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xsi:schemaLocation="
        http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context 
        http://www.springframework.org/schema/context/spring-context.xsd
        http://www.springframework.org/schema/mvc
        http://www.springframework.org/schema/mvc/spring-mvc.xsd">

    <!-- 扫描控制器 -->
    <context:component-scan base-package="com.xiang">
        <context:include-filter type="annotation"
                                expression="org.springframework.stereotype.Controller" />
    </context:component-scan>
    <!-- 扩充了注解驱动，可以将请求参数绑定到控制器参数 -->
    <mvc:annotation-driven />

    <!-- 静态资源处理， css， js， imgs -->
    <mvc:resources mapping="/resource/css/**" location="/resource/css/" />
    <mvc:resources mapping="/resource/js/**" location="/resource/js/" />
    <mvc:resources mapping="/resource/image/**" location="/resource/image/" />

    <!-- 配置试图解析器 -->
    <bean
            class="org.springframework.web.servlet.view.InternalResourceViewResolver">
        <property name="viewClass"
                  value="org.springframework.web.servlet.view.JstlView"></property>
        <!-- 配置jsp文件前缀及后缀 -->
        <property name="prefix" value="/"></property>
        <property name="suffix" value=".jsp"></property>
    </bean>
</beans>
```

2. 编写springApplication.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
		http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
		">
</beans>
```

3. 编写web.xml

```xml
<!DOCTYPE web-app>

<web-app>
  <display-name>Archetype Created Web Application</display-name>
  <!-- 欢迎页面 -->
  <welcome-file-list>
    <welcome-file>login.jsp</welcome-file>
  </welcome-file-list>

  <!-- 配置spring 上下文 -->
  <context-param>
    <param-name>contextConfigLocation</param-name>
    <param-value>classpath:springApplication.xml</param-value>
  </context-param>

  <!-- 配置监听器 -->
  <listener>
    <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
  </listener>

  <!-- spring mvc的配置上下文 -->
  <servlet>
    <servlet-name>mvc-dispatcher</servlet-name>
    <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>

    <init-param>
      <param-name>contextConfigLocation</param-name>
      <param-value>classpath:mvc-dispatcher-servlet.xml</param-value>
    </init-param>

    <load-on-startup>1</load-on-startup>
  </servlet>

  <servlet-mapping>
    <servlet-name>mvc-dispatcher</servlet-name>
    <!-- 拦截所有请求 -->
    <url-pattern>/</url-pattern>
  </servlet-mapping>

  <!-- 编码过滤器 -->
  <filter>
    <filter-name>characterEncodingFilter</filter-name>
    <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
    <init-param>
      <param-name>encoding</param-name>
      <param-value>UTF-8</param-value>
    </init-param>
    <init-param>
      <param-name>forceEncoding</param-name>
      <param-value>true</param-value>
    </init-param>
  </filter>
  <filter-mapping>
    <filter-name>characterEncodingFilter</filter-name>
    <url-pattern>/*</url-pattern>
  </filter-mapping>
</web-app>

```

4. 运行 **mvn clean**

   https://img2020.cnblogs.com/blog/1868218/202109/1868218-20210909142112948-1739075289.png

   ![image-20210909112433375](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210909112433375.png)

5. 运行**mvn install**

   https://img2020.cnblogs.com/blog/1868218/202109/1868218-20210909142125647-1722495352.png

![image-20210909112504085](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210909112504085.png)

6. 运行命令截图

   https://img2020.cnblogs.com/blog/1868218/202109/1868218-20210909142351457-344115485.png

   ![image-20210909142337150](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210909142337150.png)

## 测试apache-tomcat 项目

1. 新建项目工程
2. 配置tomcat

https://img2020.cnblogs.com/blog/1868218/202109/1868218-20210909145639649-518942706.png

https://img2020.cnblogs.com/blog/1868218/202109/1868218-20210909145705886-374474224.png

![image-20210909144845457](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210909144845457.png)

![image-20210909144916710](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210909144916710.png)

1. 配置依赖 （pom.xml）
2. 编写代码
3. 项目工程目录截图

https://img2020.cnblogs.com/blog/1868218/202109/1868218-20210909145738036-1066701404.png

![image-20210909135314826](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210909135314826.png)

4. 代码编写

### 编写 pom.xml 文件 （配置依赖）

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.xiang</groupId>
    <artifactId>mavenTest</artifactId>
    <version>1.0-SNAPSHOT</version>
    <name>mavenTest</name>
    <packaging>war</packaging>

    <!-- 依赖软件包版本 -->
    <properties>
        <commons-lang.version>2.6</commons-lang.version>
        <spring.version>5.3.9</spring.version>
        <slf4j.version>2.0.0-alpha5</slf4j.version>
        <maven.compiler.target>1.8</maven.compiler.target>
        <maven.compiler.source>1.8</maven.compiler.source>
        <junit.version>5.7.1</junit.version>
    </properties>

    <!-- 配置依赖管理包 -->
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-framework-bom</artifactId>
                <version>${spring.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <!--  依赖配置节点-->
    <dependencies>
        <!--springMVC依赖包-->
        <!-- https://mvnrepository.com/artifact/org.springframework/spring-webmvc -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
            <version>5.3.9</version>
        </dependency>


        <!-- 通用依赖包  -->
        <dependency>
            <groupId>commons-lang</groupId>
            <artifactId>commons-lang</artifactId>
            <version>${commons-lang.version}</version>
        </dependency>

        <!--日志依赖包-->
        <!-- https://mvnrepository.com/artifact/org.slf4j/slf4j-log4j12 -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-log4j12</artifactId>
            <version>2.0.0-alpha5</version>
            <scope>test</scope>
        </dependency>
        <!-- https://mvnrepository.com/artifact/org.slf4j/slf4j-api -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>2.0.0-alpha5</version>
        </dependency>

        <!-- json依赖包 -->
        <!-- https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-core -->
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-core</artifactId>
            <version>2.13.0-rc2</version>
        </dependency>

        <!-- https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-annotations -->
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-annotations</artifactId>
            <version>2.13.0-rc2</version>
        </dependency>

        <dependency>
            <groupId>org.codehaus.jackson</groupId>
            <artifactId>jackson-mapper-asl</artifactId>
            <version>1.9.13</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-databind -->
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>2.13.0-rc2</version>
        </dependency>


        <!--    MySQL驱动-->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>8.0.25</version>
        </dependency>


        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <version>4.0.1</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-api</artifactId>
            <version>${junit.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-engine</artifactId>
            <version>${junit.version}</version>
            <scope>test</scope>
        </dependency>

    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-war-plugin</artifactId>
                <version>3.3.1</version>
            </plugin>
        </plugins>
    </build>
</project>
```

### **DBTest 类；测试连接数据库**

```java
package com.xiang.lesson01;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBTest {

    private static String URL = "jdbc:mysql://localhost:3307/webapp2?serverTimezone=UTC&useUnicode=true&characterEncoding=utf-8&useSSL=false";
    private static String DriverClass = "com.mysql.cj.jdbc.Driver";
    private static String UserName = "webapp1";
    private static String PassWord = "webapp1";
    private static Connection connection = null;

    public static Connection getConnection() {
        try {
            Class.forName(DriverClass);
            connection = DriverManager.getConnection(URL, UserName, PassWord);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return connection;
    }

//    public  void  closeRerource(Connection connection){
//        try {
//            connection.close();
//        } catch (SQLException throwables) {
//            throwables.printStackTrace();
//        }
//    }

    public static void closeRerource() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        connection = DBTest.getConnection();
        if (connection != null) {
            System.out.println("连接成功");
        } else {
            System.out.println("连接失败");
        }
        closeRerource();
    }

}

```

### **DBMaven类；查询数据库**

```java
package com.xiang.lesson01;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet(urlPatterns = "/index")
public class DBMaven extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Connection connection = DBTest.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select  * from student");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                System.out.println(
                        resultSet.getInt("s_id") + "\t" +
                                resultSet.getString("s_name") + "\t" +
                                resultSet.getString("s_sex") + "\t" +
                                resultSet.getDate("s_birthday") + "\t" +
                                resultSet.getInt("s_professional"));
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        req.getRequestDispatcher("/index.jsp").forward(req,resp);
    }

}
```

### 编写 mvc-dispatcher-servlet.xml 文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xsi:schemaLocation="
        http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context 
        http://www.springframework.org/schema/context/spring-context.xsd
        http://www.springframework.org/schema/mvc
        http://www.springframework.org/schema/mvc/spring-mvc.xsd">

    <!-- 扫描控制器 -->
    <context:component-scan base-package="com.xiang">
        <context:include-filter type="annotation"
                                expression="org.springframework.stereotype.Controller" />
    </context:component-scan>
    <!-- 扩充了注解驱动，可以将请求参数绑定到控制器参数 -->
    <mvc:annotation-driven />

    <!-- 静态资源处理， css， js， imgs -->
    <mvc:resources mapping="/resource/css/**" location="/resource/css/" />
    <mvc:resources mapping="/resource/js/**" location="/resource/js/" />
    <mvc:resources mapping="/resource/image/**" location="/resource/image/" />

    <!-- 配置试图解析器 -->
    <bean
            class="org.springframework.web.servlet.view.InternalResourceViewResolver">
        <property name="viewClass"
                  value="org.springframework.web.servlet.view.JstlView"></property>
        <!-- 配置jsp文件前缀及后缀 -->
        <property name="prefix" value="/"></property>
        <property name="suffix" value=".jsp"></property>
    </bean>
</beans>
```



### 编写 springApplication.xml 文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
		http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
		">
</beans>
```

### 编写 web.xml 文件

```xml
<!DOCTYPE web-app>

<web-app>
  <display-name>Archetype Created Web Application</display-name>
  <!-- 欢迎页面 -->
  <welcome-file-list>
    <welcome-file>login.jsp</welcome-file>
  </welcome-file-list>

  <!-- 配置spring 上下文 -->
  <context-param>
    <param-name>contextConfigLocation</param-name>
    <param-value>classpath:springApplication.xml</param-value>
  </context-param>

  <!-- 配置监听器 -->
  <listener>
    <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
  </listener>

  <!-- spring mvc的配置上下文 -->
  <servlet>
    <servlet-name>mvc-dispatcher</servlet-name>
    <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>

    <init-param>
      <param-name>contextConfigLocation</param-name>
      <param-value>classpath:mvc-dispatcher-servlet.xml</param-value>
    </init-param>

    <load-on-startup>1</load-on-startup>
  </servlet>

  <servlet-mapping>
    <servlet-name>mvc-dispatcher</servlet-name>
    <!-- 拦截所有请求 -->
    <url-pattern>/</url-pattern>
  </servlet-mapping>

  <!-- 编码过滤器 -->
  <filter>
    <filter-name>characterEncodingFilter</filter-name>
    <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
    <init-param>
      <param-name>encoding</param-name>
      <param-value>UTF-8</param-value>
    </init-param>
    <init-param>
      <param-name>forceEncoding</param-name>
      <param-value>true</param-value>
    </init-param>
  </filter>
  <filter-mapping>
    <filter-name>characterEncodingFilter</filter-name>
    <url-pattern>/*</url-pattern>
  </filter-mapping>
</web-app>

```

### 编写 index.jsp 文件

```jsp
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>
</head>
<body>
<h1><%= "Hello World!" %>
</h1>
<br/>
<a href="/mavenTest">Hello Servlet</a>
</body>
</html>
```

### 编写 login.jsp 文件

```jsp
<%--
  Created by IntelliJ IDEA.
  User: Xiang
  Date: 2021/9/9
  Time: 11:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>

<body>
<h1><%= "login" %>
</h1>
<br/>
<a href="/mavenTest/index">login</a>
</body>
</html>
```

### 运行截图

https://img2020.cnblogs.com/blog/1868218/202109/1868218-20210909145910079-1473870015.png
https://img2020.cnblogs.com/blog/1868218/202109/1868218-20210909145923693-668176327.png

![image-20210909145033912](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210909145033912.png)

![image-20210909145128338](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210909145128338.png)

![image-20210909145105980](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210909145105980.png)



## Spring MVC 框架

JavaEE体系结构包括四层，从上到下分别是应用层、Web层、业务层、持久层。

Struts和SpringMVC是Web层的框架，Spring是业务层的框架，Hibernate和MyBatis是持久层的框架。

### 为什么要使用SpringMVC

很多应用程序的问题在于处理业务数据的对象和显示业务数据的视图之间存在紧密耦合，通常，更新业务对象的命令都是从视图本身发起的，使视图对任何业务对象更改都有高度敏感性。而且，当多个视图依赖于同一个业务对象时是没有灵活性的。

SpringMVC是一种基于Java，实现了Web MVC设计模式，请求驱动类型的轻量级Web框架，即使用了MVC架构模式的思想，将Web层进行职责解耦。基于请求驱动指的就是使用请求-响应模型，框架的目的就是帮助我们简化开发，SpringMVC也是要简化我们日常Web开发。

### **SpringMVC的核心架构**

https://img-blog.csdnimg.cn/20190630145911981.png

![image-20210909151210706](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210909151210706.png)



### 具体流程：

（1）首先浏览器发送请求——>DispatcherServlet，前端控制器收到请求后自己不进行处理，而是委托给其他的解析器进行处理，作为统一访问点，进行全局的流程控制；

（2）DispatcherServlet——>HandlerMapping，处理器映射器将会把请求映射为HandlerExecutionChain对象（包含一个Handler处理器对象、多个HandlerInterceptor拦截器）对象；

（3）DispatcherServlet——>HandlerAdapter，处理器适配器将会把处理器包装为适配器，从而支持多种类型的处理器，即适配器设计模式的应用，从而很容易支持很多类型的处理器；

（4）HandlerAdapter——>调用处理器相应功能处理方法，并返回一个ModelAndView对象（包含模型数据、逻辑视图名）；

（5）ModelAndView对象（Model部分是业务对象返回的模型数据，View部分为逻辑视图名）——> ViewResolver， 视图解析器将把逻辑视图名解析为具体的View；

（6）View——>渲染，View会根据传进来的Model模型数据进行渲染，此处的Model实际是一个Map数据结构；

（7）返回控制权给DispatcherServlet，由DispatcherServlet返回响应给用户，到此一个流程结束。



## HTTP请求地址映射

### 1编写UserController 类

```java
package com.xiang.lesson01.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/user")
public class UserController {

    @RequestMapping("/login")
    public String register(){
        return "user/login";
    }


    @RequestMapping(value = "/dologin/{userId}",method = RequestMethod.POST)
    public String  login(){
        return "/user/success";
    }

}
```

### 2编写 login.jsp 文件

```jsp
<%--
  Created by IntelliJ IDEA.
  User: Xiang
  Date: 2021/9/9
  Time: 11:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
</head>

<body>
<div align="center">
    <form action="<%=request.getContextPath()%>/user/dologin/123" method="post">
        <table border="1">
            <tr>
                <td>user</td>
                <td>
                    <input type="text" name="user">
                </td>
            </tr>
            <tr>
                <td>pwd</td>
                <input type="password" name="pwd">
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="submit">
                </td>
            </tr>
        </table>
    </form>
</div>

</body>
</html>

```

### 3编写success.jsp 文件

```jsp
<%--
  Created by IntelliJ IDEA.
  User: Xiang
  Date: 2021/9/9
  Time: 15:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h3>
  success
</h3>
</body>
</html>
```

### 4运行结果

![image-20210909161714303](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210909161714303.png)



## Get、Post、Put与Delete的区别

**GET**

 + GET请求是用来获取数据的，不对服务器的数据做任何的修改，新增，删除等操作。
 + GET请求就像数据库的SELECT操作一样，只是用来查询一下数据，不会修改、增加数据，不会影响资源的内容。
 + GET请求会把请求的参数附加在URL后面，这样会产生安全问题，如果是系统的登陆接口采用的GET请求，需要对请求的参数做一个加密。



**PUT**

 + PUT请求是向服务器端发送数据的，
 + PUT请求就像数据库的UPDATE操作一样，用来修改数据的内容，
 + PUT的侧重点在于对于数据的修改操作。



**POST**

 + POST请求同PUT请求类似，都是向服务器端发送数据的，
 + 但是POST请求会改变数据的种类等资源，就像数据库的INSERT操作一样，会创建新的内容。
 + 常用来数据的提交，新增操作。



**DELETE**

 + DELETE请求用来删除某一个资源的，
 + DELETE请求就像数据库的DELETE操作一样。



根据上边四个描述，可以理解为：
	**1. POST 创建**
	**2. DELETE 删除**
	**3. PUT 更新**
	**4. GET 查看**



## get和post的区别主要有以下几方面

1. **url可见性：**
   	+ get，参数url可见；
   	+ post，url参数不可见

2. **数据传输上：**
   	+ get，通过拼接url进行传递参数；
   	+ post，通过body体传输参数

3. **缓存性：**
   	+ get请求是可以缓存的
   	+ post请求不可以缓存

4. **后退页面的反应**
   	+ get请求页面后退时，不产生影响
   	+ post请求页面后退时，会重新提交请求

5. **传输数据的大小**
   	+ get一般传输数据大小不超过2k-4k（根据浏览器不同，限制不一样，但相差不大）
   	+ post请求传输数据的大小根据php.ini 配置文件设定，也可以无限大。

6. **安全性**
   	+ 这个也是最不好分析的，原则上post肯定要比get安全，毕竟传输参数时url不可见，但也挡不住部分人闲的没事在那抓包玩。
   	+ 安全性个人觉得是没多大区别的，防君子不防小人就是这个道理。对传递的参数进行加密，其实都一样。
7. **数据包**
   	+ GET产生一个TCP数据包；
   	+ POST产生两个TCP数据包。
   	+ 对于GET方式的请求，浏览器会把http header和data一并发送出去，服务器响应200（返回数据）；
   	+ 而对于POST，浏览器先发送header，服务器响应100 continue，浏览器再发送data，服务器响应200 ok（返回数据）。
   	+ 在网络环境好的情况下，发一次包的时间和发两次包的时间差别基本可以无视。
   	+ 而在网络环境差的情况下，两次包的TCP在验证数据包完整性上，有非常大的优点。
   	+ 并不是所有浏览器都会在POST中发送两次包，Firefox就只发送一次。



## springMVC 参数绑定  

### 1编写UserController 类

```java
package com.xiang.lesson01.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value = "/user")
public class UserController {

    @RequestMapping("/login")
    public String register() {
        return "user/login";
    }


    @RequestMapping(value = "/dologin/{userId}", method = RequestMethod.POST)
    public String login(@PathVariable("userId") int userId) {
//       PathVariable 前台 传入后台
        System.out.println("userid====>" + userId);
        return "/user/success";
    }

    @RequestMapping(value = "/logintwo", method = RequestMethod.POST)
    public String login2(@RequestParam("user") String user, @RequestParam("pwd") String pwd) {
        if (user.equals("xiang") && pwd.equals("123")) {
            return "/user/success";
        } else {
            return "/user/fail";
        }
    }
}
```

### 2编写login.jsp 文件

```xml
<%--
  Created by IntelliJ IDEA.
  User: Xiang
  Date: 2021/9/9
  Time: 11:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
</head>

<body>
<div align="center">
    <form action="<%=request.getContextPath()%>/user/logintwo" method="post">
        <table border="1">
            <tr>
                <td>user</td>
                <td>
                    <input type="text" name="user">
                </td>
            </tr>
            <tr>
                <td>pwd</td>
                <input type="password" name="pwd">
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="submit">
                </td>
            </tr>
        </table>
    </form>
</div>


</body>
</html>
```

### 3编写success.jsp 文件

```xml
<%--
  Created by IntelliJ IDEA.
  User: Xiang
  Date: 2021/9/9
  Time: 15:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h3>
  success
</h3>
</body>
</html>
```

### 4编写login.jsp 文件

```xml
<%--
  Created by IntelliJ IDEA.
  User: Xiang
  Date: 2021/9/9
  Time: 16:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<div align="center">
    <form action="<%=request.getContextPath()%>/user/logintwo" method="post">
        <table border="1">
            <tr>
                <td>user</td>
                <td>
                    <input type="text" name="user">
                </td>
            </tr>
            <tr>
                <td>pwd</td>
                <input type="password" name="pwd">
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="submit">
                </td>
            </tr>
        </table>
    </form>
</div>


</body>
</html>
```

### 5运行截图

https://img2020.cnblogs.com/blog/1868218/202109/1868218-20210910100637658-1430564011.png
https://img2020.cnblogs.com/blog/1868218/202109/1868218-20210910100648408-2117007375.png
https://img2020.cnblogs.com/blog/1868218/202109/1868218-20210910100703042-88222426.png

![image-20210910100128589](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210910100128589.png)

![image-20210910100209246](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210910100209246.png)

![image-20210910100238720](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210910100238720.png)

![image-20210910100302144](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210910100302144.png)

![image-20210910100329847](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210910100329847.png)



## 数据模型控制 

### 1数据模型访问结构

![image-20210910143320914](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210910143320914.png)

### 通过





## SpringMVC框架访问数据库的用户表

**题目：**

利用SpringMVC框架访问数据库的用户表(id,username,sex,age,birthday)， 并实现用户数据的展示；
说明：后台采用JDBC方式，展示采用model、modelMap、modelAndMap三种方式 

### 1.新建项目工程

### 2.配置Tomcat 

https://img2020.cnblogs.com/blog/1868218/202109/1868218-20210911122548022-1411028163.png

![image-20210911121827393](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210911121827393.png)

### 3.编写data.sql文件

```sql
-- 创建数据库
create database webapp1 charset utf8mb4;
-- 创建用户名、密码
create user'webapp1'@'localhost'identified by'webapp1';
-- 授权
grant all on webapp1.*to'webapp1'@'localhost';
-- 用用户名、密码登录
mysql -uwebapp1 -pwebapp1;
-- 创建表
-- id,username,sex,age,birthday
create table user
(
    id int not null primary key auto_increment,
    username varchar(12),
    sex varchar(2),
    age int,
    birthday date
);

-- 查看表结构
describe user;
-- 添加数据
insert into user
(`username`,`sex`,`age`,`birthday`)
values
("小向","男",19,"2000-1-1");
-- 添加多条数据
insert into user
(`username`,`sex`,`age`,`birthday`)
values
("小向","男",19,"2000-1-1"),
("小李","男",19,"2000-1-1"),
("小张","男",19,"2000-1-1"),
("小五","男",19,"2000-1-1"),
("小三","男",19,"2000-1-1"),
("小二","男",19,"2000-1-1"),
("小周","男",19,"2000-1-1");

```

### 4.配置pom.xml文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.xiang</groupId>
    <artifactId>mvcText</artifactId>
    <version>1.0-SNAPSHOT</version>
    <name>mvcText</name>
    <packaging>war</packaging>
    <!-- 依赖软件包版本 -->
    <properties>
        <commons-lang.version>2.6</commons-lang.version>
        <spring.version>5.3.9</spring.version>
        <slf4j.version>2.0.0-alpha5</slf4j.version>
        <maven.compiler.target>1.8</maven.compiler.target>
        <maven.compiler.source>1.8</maven.compiler.source>
        <junit.version>5.7.1</junit.version>
    </properties>
    <!-- 配置依赖管理包 -->
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-framework-bom</artifactId>
                <version>${spring.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <!--  依赖配置节点-->
    <dependencies>
        <!--springMVC依赖包-->
        <!-- https://mvnrepository.com/artifact/org.springframework/spring-webmvc -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
            <version>5.3.9</version>
        </dependency>


        <!-- 通用依赖包  -->
        <dependency>
            <groupId>commons-lang</groupId>
            <artifactId>commons-lang</artifactId>
            <version>${commons-lang.version}</version>
        </dependency>

        <!--日志依赖包-->
        <!-- https://mvnrepository.com/artifact/org.slf4j/slf4j-log4j12 -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-log4j12</artifactId>
            <version>2.0.0-alpha5</version>
            <scope>test</scope>
        </dependency>
        <!-- https://mvnrepository.com/artifact/org.slf4j/slf4j-api -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>2.0.0-alpha5</version>
        </dependency>

        <!-- json依赖包 -->
        <!-- https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-core -->
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-core</artifactId>
            <version>2.13.0-rc2</version>
        </dependency>

        <!-- https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-annotations -->
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-annotations</artifactId>
            <version>2.13.0-rc2</version>
        </dependency>

        <dependency>
            <groupId>org.codehaus.jackson</groupId>
            <artifactId>jackson-mapper-asl</artifactId>
            <version>1.9.13</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-databind -->
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>2.13.0-rc2</version>
        </dependency>


        <!--    MySQL驱动-->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>8.0.25</version>
        </dependency>
        <!--        jstl-->
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>jstl</artifactId>
            <version>1.2</version>
        </dependency>

        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <version>4.0.1</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-api</artifactId>
            <version>${junit.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-engine</artifactId>
            <version>${junit.version}</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-war-plugin</artifactId>
                <version>3.3.1</version>
            </plugin>
        </plugins>
    </build>
</project>
```

### 5.配置mvc-dispatcher-servlet.xml文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xsi:schemaLocation="
        http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context 
        http://www.springframework.org/schema/context/spring-context.xsd
        http://www.springframework.org/schema/mvc
        http://www.springframework.org/schema/mvc/spring-mvc.xsd">

    <!-- 扫描控制器 -->
    <context:component-scan base-package="com.xiang">
        <context:include-filter type="annotation"
                                expression="org.springframework.stereotype.Controller" />
    </context:component-scan>
    <!-- 扩充了注解驱动，可以将请求参数绑定到控制器参数 -->
    <mvc:annotation-driven />

    <!-- 静态资源处理， css， js， imgs -->
    <mvc:resources mapping="/resource/css/**" location="/resource/css/" />
    <mvc:resources mapping="/resource/js/**" location="/resource/js/" />
    <mvc:resources mapping="/resource/image/**" location="/resource/image/" />

    <!-- 配置试图解析器 -->
    <bean
            class="org.springframework.web.servlet.view.InternalResourceViewResolver">
        <property name="viewClass"
                  value="org.springframework.web.servlet.view.JstlView"></property>
        <!-- 配置jsp文件前缀及后缀 -->
        <property name="prefix" value="/"></property>
        <property name="suffix" value=".jsp"></property>
    </bean>
</beans>
```

### 6. 配置springApplication.xml文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
		http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
		">
</beans>
```

### 7.编写DBTest类，测试连接数据库

```java
package com.xiang;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class DBTest {
    private static String URL = "jdbc:mysql://localhost:3307/webapp1?serverTimezone=UTC&useUnicode=true&characterEncoding=utf-8&useSSL=false";
    private static String DriverClass = "com.mysql.cj.jdbc.Driver";
    private static String UserName = "webapp1";
    private static String PassWord = "webapp1";
    private static Connection connection = null;

    public static Connection getConnection() {
        try {
            Class.forName(DriverClass);
            connection = DriverManager.getConnection(URL, UserName, PassWord);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return connection;
    }

    public static void closeResource() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        DBTest.getConnection();
        if (connection != null) {
            System.out.println("数据库连接成功");
        } else {
            System.out.println("数据库连接失败");
        }
        closeResource();
    }

}

```

### 8.编写pojo.User类

```java
package com.xiang.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * id int not null primary key auto_increment,
 * username varchar(12),
 * sex varchar(2),
 * age int,
 * birthday date
 */
public class User {
    private int id;
    private String username;
    private String sex;
    private int age;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthday;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", sex='" + sex + '\'' +
                ", age=" + age +
                ", birthday=" + birthday +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
}

```

### 9.编写controller.UserController类

```java
package com.xiang.controller;

import com.xiang.DBTest;
import com.xiang.pojo.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;

@Controller
@RequestMapping("/user")
public class UserController {
    private static Connection connection = null;
    private static PreparedStatement statement = null;
    private static ResultSet query = null;
    private static ArrayList<Object> list = null;

    public static void init() throws Exception {
        connection = DBTest.getConnection();
        statement = connection.prepareStatement("select  * from  user ");
        query = statement.executeQuery();
        list = new ArrayList<>();

        while (query.next()) {
            User user = new User();
            int anInt = query.getInt(1);
            user.setId(anInt);
            String username = query.getString("username");
            user.setUsername(username);
            String sex = query.getString("sex");
            user.setSex(sex);
            int age = query.getInt(4);
            user.setAge(age);
            Date birthday = query.getDate("birthday");
            user.setBirthday(birthday);
            list.add(user);
        }
        System.out.println("init()");
    }

    @RequestMapping("/view1")
    public String getVive1(ModelMap modelMap) throws Exception {
        init();
        modelMap.addAttribute("list", list);
        DBTest.closeResource();
        return "/user/view1";
    }

    @RequestMapping("/view2")
    public String getVive2(Model model) throws Exception {
        connection = DBTest.getConnection();
        init();
        model.addAttribute("list", list);
        DBTest.closeResource();
        return "/user/view2";
    }

    @RequestMapping("/view3")
    public ModelAndView getVive3() throws Exception {
        init();
        ModelAndView andView = new ModelAndView();
        andView.setViewName("/user/view3");
        andView.addObject("list", list);
        return andView;
    }

}

```

### 10.编写user.view1.jsp文件

```jsp
<%--
  Created by IntelliJ IDEA.
  User: Xiang
  Date: 2021/9/10
  Time: 23:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>view1</title>
</head>
<body>
<h4>
    view1
</h4>
<form action="<%=request.getContextPath()%>/view1">
    <table border="1">

        <tr>
            <th scope="col">id</th>
            <th scope="col">username</th>
            <th scope="col">sex</th>
            <th scope="col">age</th>
            <th scope="col">birthday</th>
        </tr>

        <c:forEach items="${list}" var="model">
            <tr>
               <td>${model.id}</td>
               <td>${model.username}</td>
               <td>${model.sex}</td>
               <td>${model.age}</td>
               <td>${model.birthday}</td>

            </tr>
        </c:forEach>

<%--        <c:forEach items="${list}" var="model">--%>
<%--            <tr>--%>
<%--                <td>${model.byteValue()}</td>--%>
<%--            </tr>--%>
<%--        </c:forEach>--%>
        <tr></tr>
        <tr></tr>
        <tr></tr>
        <tr></tr>

        <%--<c:forEach items="${id}" var="model">--%>
        <%--    <tr>--%>
        <%--        --%>
        <%--        --%>
        <%--        <td><c:out value="${model.username}"/></td>--%>
        <%--        <td><c:out value="${model.sex}"/></td>--%>
        <%--        <td><c:out value="${model.age}"/></td>--%>
        <%--        <td><c:out value="${model.birthday}"/></td>--%>
        <%--&lt;%&ndash;    <td><fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${model.birthday}"/></td>&ndash;%&gt;--%>

        <%--</c:forEach>--%>

    </table>
</form>
</body>
</html>

```

### 11.编写user.view2.jsp文件

```jsp
<%--
  Created by IntelliJ IDEA.
  User: Xiang
  Date: 2021/9/10
  Time: 23:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>view2</title>
</head>
<body>
<h4>
    view2
</h4>
<form action="<%=request.getContextPath()%>/view2">
    <table border="1">

        <tr>
            <th scope="col">id</th>
            <th scope="col">username</th>
            <th scope="col">sex</th>
            <th scope="col">age</th>
            <th scope="col">birthday</th>
        </tr>

        <c:forEach items="${list}" var="model">
            <tr>
               <td>${model.id}</td>
               <td>${model.username}</td>
               <td>${model.sex}</td>
               <td>${model.age}</td>
               <td>${model.birthday}</td>

            </tr>
        </c:forEach>

<%--        <c:forEach items="${list}" var="model">--%>
<%--            <tr>--%>
<%--                <td>${model.byteValue()}</td>--%>
<%--            </tr>--%>
<%--        </c:forEach>--%>
        <tr></tr>
        <tr></tr>
        <tr></tr>
        <tr></tr>

        <%--<c:forEach items="${id}" var="model">--%>
        <%--    <tr>--%>
        <%--        --%>
        <%--        --%>
        <%--        <td><c:out value="${model.username}"/></td>--%>
        <%--        <td><c:out value="${model.sex}"/></td>--%>
        <%--        <td><c:out value="${model.age}"/></td>--%>
        <%--        <td><c:out value="${model.birthday}"/></td>--%>
        <%--&lt;%&ndash;    <td><fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${model.birthday}"/></td>&ndash;%&gt;--%>

        <%--</c:forEach>--%>

    </table>
</form>
</body>
</html>

```

### 12.编写user.view3.jsp文件

```jsp
<%--
  Created by IntelliJ IDEA.
  User: Xiang
  Date: 2021/9/10
  Time: 23:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>view3</title>
</head>
<body>
<h4>
    view3
</h4>
<form action="<%=request.getContextPath()%>/view2">
    <table border="1">

        <tr>
            <th scope="col">id</th>
            <th scope="col">username</th>
            <th scope="col">sex</th>
            <th scope="col">age</th>
            <th scope="col">birthday</th>
        </tr>

        <c:forEach items="${list}" var="model">
            <tr>
               <td>${model.id}</td>
               <td>${model.username}</td>
               <td>${model.sex}</td>
               <td>${model.age}</td>
               <td>${model.birthday}</td>

            </tr>
        </c:forEach>

<%--        <c:forEach items="${list}" var="model">--%>
<%--            <tr>--%>
<%--                <td>${model.byteValue()}</td>--%>
<%--            </tr>--%>
<%--        </c:forEach>--%>
        <tr></tr>
        <tr></tr>
        <tr></tr>
        <tr></tr>

        <%--<c:forEach items="${id}" var="model">--%>
        <%--    <tr>--%>
        <%--        --%>
        <%--        --%>
        <%--        <td><c:out value="${model.username}"/></td>--%>
        <%--        <td><c:out value="${model.sex}"/></td>--%>
        <%--        <td><c:out value="${model.age}"/></td>--%>
        <%--        <td><c:out value="${model.birthday}"/></td>--%>
        <%--&lt;%&ndash;    <td><fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${model.birthday}"/></td>&ndash;%&gt;--%>

        <%--</c:forEach>--%>

    </table>
</form>
</body>
</html>

```

### 13.项目工程结构目录图

https://img2020.cnblogs.com/blog/1868218/202109/1868218-20210911122651915-647828313.png

![image-20210911121442162](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210911121442162.png)



### 14.项目工程运行结果

https://img2020.cnblogs.com/blog/1868218/202109/1868218-20210911122711986-963307129.png

![image-20210911121730721](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210911121730721.png)

### 15.项目工程打包

```java
mvn package -Dmaven.test.skip=true
```

https://img2020.cnblogs.com/blog/1868218/202109/1868218-20210911122730604-282805916.png

![image-20210911122124361](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210911122124361.png)

![image-20210911122308612](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210911122308612.png)

### 16.忘记配置web.xml了 不好意思；

```xml
<!DOCTYPE web-app>

<web-app>
  <display-name>Archetype Created Web Application</display-name>
  <!-- 欢迎页面 -->
  <welcome-file-list>
    <welcome-file>index.jsp</welcome-file>
  </welcome-file-list>

  <!-- 配置spring 上下文 -->
  <context-param>
    <param-name>contextConfigLocation</param-name>
    <param-value>classpath:springApplication.xml</param-value>
  </context-param>

  <!-- 配置监听器 -->
  <listener>
    <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
  </listener>

  <!-- spring mvc的配置上下文 -->
  <servlet>
    <servlet-name>mvc-dispatcher</servlet-name>
    <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>

    <init-param>
      <param-name>contextConfigLocation</param-name>
      <param-value>classpath:mvc-dispatcher-servlet.xml</param-value>
    </init-param>

    <load-on-startup>1</load-on-startup>
  </servlet>

  <servlet-mapping>
    <servlet-name>mvc-dispatcher</servlet-name>
    <!-- 拦截所有请求 -->
    <url-pattern>/</url-pattern>
  </servlet-mapping>

  <!-- 编码过滤器 -->
  <filter>
    <filter-name>characterEncodingFilter</filter-name>
    <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
    <init-param>
      <param-name>encoding</param-name>
      <param-value>UTF-8</param-value>
    </init-param>
    <init-param>
      <param-name>forceEncoding</param-name>
      <param-value>true</param-value>
    </init-param>
  </filter>
  <filter-mapping>
    <filter-name>characterEncodingFilter</filter-name>
    <url-pattern>/*</url-pattern>
  </filter-mapping>
</web-app>

```

 

## SpringMVC获取服务器时间

**题目：**

前端页面显示后台服务器时间

### 1、新建项目工程

### 2、配置Tomcat 

https://img2020.cnblogs.com/blog/1868218/202109/1868218-20210912000412993-265843071.png

![image-20210911234326312](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210911234326312.png)

### 3、配置pom.xml文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.xiang</groupId>
    <artifactId>mvcText</artifactId>
    <version>1.0-SNAPSHOT</version>
    <name>dateDemo</name>
    <packaging>war</packaging>
    <!-- 依赖软件包版本 -->
    <properties>
        <commons-lang.version>2.6</commons-lang.version>
        <spring.version>5.3.9</spring.version>
        <slf4j.version>2.0.0-alpha5</slf4j.version>
        <maven.compiler.target>1.8</maven.compiler.target>
        <maven.compiler.source>1.8</maven.compiler.source>
        <junit.version>5.7.1</junit.version>
    </properties>
    <!-- 配置依赖管理包 -->
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-framework-bom</artifactId>
                <version>${spring.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <!--  依赖配置节点-->
    <dependencies>
        <!--springMVC依赖包-->
        <!-- https://mvnrepository.com/artifact/org.springframework/spring-webmvc -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
            <version>5.3.9</version>
        </dependency>

        <!-- 通用依赖包  -->
        <dependency>
            <groupId>commons-lang</groupId>
            <artifactId>commons-lang</artifactId>
            <version>${commons-lang.version}</version>
        </dependency>

        <!--jstl-->
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>jstl</artifactId>
            <version>1.2</version>
        </dependency>

    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-war-plugin</artifactId>
                <version>3.3.1</version>
            </plugin>
        </plugins>
    </build>
</project>
```

### 4、配置mvc-dispatcher-servlet.xml文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xsi:schemaLocation="
        http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context
        http://www.springframework.org/schema/context/spring-context.xsd
        http://www.springframework.org/schema/mvc
        http://www.springframework.org/schema/mvc/spring-mvc.xsd">

    <!-- 扫描控制器 -->
    <context:component-scan base-package="com.xiang">
        <context:include-filter type="annotation"
                                expression="org.springframework.stereotype.Controller"/>
    </context:component-scan>
    <!-- 扩充了注解驱动，可以将请求参数绑定到控制器参数 -->
    <mvc:annotation-driven/>

    <!-- 静态资源处理， css， js， imgs -->
    <!--    <mvc:resources mapping="/resource/css/**" location="/resource/css/" />-->
    <!--    <mvc:resources mapping="/resource/js/**" location="/resource/js/" />-->
    <!--    <mvc:resources mapping="/resource/image/**" location="/resource/image/" />-->

    <!-- 配置试图解析器 -->
    <bean
            class="org.springframework.web.servlet.view.InternalResourceViewResolver">
        <property name="viewClass"
                  value="org.springframework.web.servlet.view.JstlView"></property>
        <!-- 配置jsp文件前缀及后缀 -->
        <property name="prefix" value="/"></property>
        <property name="suffix" value=".jsp"></property>
    </bean>

    <!--    自动注册最合适的处理器映射器、处理器适配器-->
    <mvc:annotation-driven/>
</beans>
```

### 5、配置springApplication.xml文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
		http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
		">
</beans>
```

### 6、编写controller.HomeController类

```java
package com.xiang.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/11 17:30
 */

@Controller

public class HomeController {
    private  Date date = new Date();

    @RequestMapping("/date")
    public ModelAndView test01() {
        date = new Date(); //服务器时间
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:ss:mm");
        String format = dateFormat.format(date);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
        String s = df.format(date);

        new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        date = new Date(System.currentTimeMillis());

        LocalDateTime.now(); // get the current date and time
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

        Calendar calendar = Calendar.getInstance(); // get current instance of the calendar
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        formatter.format(calendar.getTime());


//        返回服务器时间到前端
//        封装了数据和页面信息 ModelAndView
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("format", format);
        modelAndView.addObject("date", date);
        modelAndView.addObject("df", df);
        modelAndView.addObject("timeFormatter", timeFormatter);
        modelAndView.addObject("formatter", formatter);
        modelAndView.addObject("s", s);

//        视图解析
        modelAndView.setViewName("date/date");
        return modelAndView;
    }
}
```

### 7、编写date.date.jsp文件

```jsp
<%--
  Created by IntelliJ IDEA.
  User: Xiang
  Date: 2021/9/11
  Time: 17:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>date</title>
</head>
<body>
<h3>
    跳转成功，当前服务器时间: ${format}
</h3>
<h3>
    跳转成功，当前服务器时间: ${date}
</h3>
<h3>
    跳转成功，当前服务器时间: ${df}
</h3>
<h3>
    跳转成功，当前服务器时间: ${timeFormatter}
</h3>
<h3>
    跳转成功，当前服务器时间: ${formatter}
</h3>
<h3 style="color: red">
    跳转成功，当前服务器时间: ${s}
</h3>

</body>
</html>

```

### 8、项目工程结构目录

https://img2020.cnblogs.com/blog/1868218/202109/1868218-20210912000430864-1091701210.png

![image-20210911235811318](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210911235811318.png)

### 9、项目工程运行结果 

https://img2020.cnblogs.com/blog/1868218/202109/1868218-20210912000438224-5167184.png

![image-20210912000137338](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210912000137338.png)



## SpringMVC的九大组件

### SpringMVC是Spring框架中的一个表现层框架，也称为控制层，SpringMVC中有很重要的九大组件

```xml
<!-- spring mvc的配置上下文 -->
    <servlet>
        <servlet-name>mvc-dispatcher</servlet-name>
        <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>

        <init-param>
            <param-name>contextConfigLocation</param-name>
            <param-value>classpath:mvc-dispatcher-servlet.xml</param-value>
        </init-param>

        <load-on-startup>1</load-on-startup>
    </servlet>
```

### 从上面的配置可以看出，SpringMVC与WEB项目集成的入口就是这个**DispatcherServlet**类，这个类会加载我们配置的spring-mvc.xml文件，源码如下：

```java
public class DispatcherServlet extends FrameworkServlet {

	/** Well-known name for the MultipartResolver object in the bean factory for this namespace. */
	public static final String MULTIPART_RESOLVER_BEAN_NAME = "multipartResolver";

	/** Well-known name for the LocaleResolver object in the bean factory for this namespace. */
	public static final String LOCALE_RESOLVER_BEAN_NAME = "localeResolver";

	/** Well-known name for the ThemeResolver object in the bean factory for this namespace. */
	public static final String THEME_RESOLVER_BEAN_NAME = "themeResolver";

	/**
	 * Well-known name for the HandlerMapping object in the bean factory for this namespace.
	 * Only used when "detectAllHandlerMappings" is turned off.
	 * @see #setDetectAllHandlerMappings
	 */
	public static final String HANDLER_MAPPING_BEAN_NAME = "handlerMapping";

	/**
	 * Well-known name for the HandlerAdapter object in the bean factory for this namespace.
	 * Only used when "detectAllHandlerAdapters" is turned off.
	 * @see #setDetectAllHandlerAdapters
	 */
	public static final String HANDLER_ADAPTER_BEAN_NAME = "handlerAdapter";

	/**
	 * Well-known name for the HandlerExceptionResolver object in the bean factory for this namespace.
	 * Only used when "detectAllHandlerExceptionResolvers" is turned off.
	 * @see #setDetectAllHandlerExceptionResolvers
	 */
	public static final String HANDLER_EXCEPTION_RESOLVER_BEAN_NAME = "handlerExceptionResolver";

	/**
	 * Well-known name for the RequestToViewNameTranslator object in the bean factory for this namespace.
	 */
	public static final String REQUEST_TO_VIEW_NAME_TRANSLATOR_BEAN_NAME = "viewNameTranslator";

	/**
	 * Well-known name for the ViewResolver object in the bean factory for this namespace.
	 * Only used when "detectAllViewResolvers" is turned off.
	 * @see #setDetectAllViewResolvers
	 */
	public static final String VIEW_RESOLVER_BEAN_NAME = "viewResolver";

	/**
	 * Well-known name for the FlashMapManager object in the bean factory for this namespace.
	 */
	public static final String FLASH_MAP_MANAGER_BEAN_NAME = "flashMapManager";
```

### 一共是十个方法，其中有一个初始化策略的方法initStrategies

```java
/**
	 * This implementation calls {@link #initStrategies}.
	 */
	@Override
	protected void onRefresh(ApplicationContext context) {
		initStrategies(context);
	}

	/**
	 * Initialize the strategy objects that this servlet uses.
	 * <p>May be overridden in subclasses in order to initialize further strategy objects.
	 */
	protected void initStrategies(ApplicationContext context) {
		initMultipartResolver(context);
		initLocaleResolver(context);
		initThemeResolver(context);
		initHandlerMappings(context);
		initHandlerAdapters(context);
		initHandlerExceptionResolvers(context);
		initRequestToViewNameTranslator(context);
		initViewResolvers(context);
		initFlashMapManager(context);
	}
```

### 可看出，初始化策略是其他九个初始化方法的概括，而其他九个初始化方法就是我们要介绍的九大组件

 + 1、MultipartResolver（文件处理器）
    + 对应的初始化方法是initMultipartResolver(context)，用于处理上传请求。
    + 处理方法是将普通的request包装成MultipartHttpServletRequest，后者可以直接调用getFile方法获取File。
 + 2、LocaleResolver（当前环境处理器）
    + 对应的初始化方法是initLocaleResolver(context)，这就相当于配置数据库的方言一样，有了这个就可以对不同区域的用户显示不同的结果。
    + SpringMVC主要有两个地方用到了Locale：
       + 一是ViewResolver视图解析的时候；
       + 二是用到国际化资源或者主题的时候。
 + 3、ThemeResolver（主题处理器）
    + 对应的初始化方法是initThemeResolver(context)，用于解析主题。
    + SpringMVC中一个主题对应一个properties文件，里面存放着跟当前主题相关的所有资源，如图片、css样式等。
    + SpringMVC的主题也支持国际化。
 + 4、HandlerMapping（处理器映射器）
    + 对应的初始化方法是initHandlerMappings(context)，这就是根据用户请求的资源uri来查找Handler的。
    + 在SpringMVC中会有很多请求，每个请求都需要一个Handler处理，具体接收到一个请求之后使用哪个Handler进行处理呢？这就是HandlerMapping需要做的事。
 + 5、HandlerAdapters（处理器适配器）
    + 对应的初始化方法是initHandlerAdapters(context)，从名字上看，它就是一个适配器。
    + Servlet需要的处理方法的结构却是固定的，都是以request和response为参数的方法。
    + 如何让固定的Servlet处理方法调用灵活的Handler来进行处理呢？这就是HandlerAdapters要做的事情。
 + 6、HandlerExceptionResolvers（异常处理器）
    + 对应的初始化方法是initHandlerExceptionResolvers(context)，其它组件都是用来干活的。
    + 在干活的过程中难免会出现问题，出问题后怎么办呢？这就要有一个专门的角色对异常情况进行处理，在SpringMVC中就是HandlerExceptionResolver。
 + 7、RequestToViewNameTranslator（视图名称翻译器），
    + 对应的初始化方法是initRequestToViewNameTranslator(context)，有的Handler处理完后并没有设置View也没有设置ViewName，这时就需要从request获取ViewName了，如何从request中获取ViewName就是RequestToViewNameTranslator要做的事情了。
 + 8、ViewResolvers（页面渲染处理器）
    + 对应的初始化方法是initViewResolvers(context)，ViewResolver用来将String类型的视图名和Locale解析为View类型的视图。
    + View是用来渲染页面的，也就是将程序返回的参数填入模板里，生成html（也可能是其它类型）文件。
 + 9、FlashMapManager（参数传递管理器）
    + 对应的初始化方法是initFlashMapManager(context)，用来管理FlashMap的，FlashMap主要用在redirect重定向中传递参数。



## url-pattern 的使用

![image-20210912010659980](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210912010659980.png)



这种方法，静态资源只能放在webapp 根下；

方案一

![image-20210912011112824](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210912011112824.png)





方案二

![image-20210912011621038](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210912011621038.png)



## model、modelMap、Map 封装数据 

![image-20210912154634929](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210912154634929.png)



## springMVC 数据绑定方式

![image-20210913091324111](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210913091324111.png)



## 数据模型绑定

1. Model
2. ModelMap
3. ModelAndView



## 视图解析器

```xml
 <!-- 配置试图解析器 -->
    <bean
            class="org.springframework.web.servlet.view.InternalResourceViewResolver">
        <property name="viewClass"
                  value="org.springframework.web.servlet.view.JstlView"></property>
        <!-- 配置jsp文件前缀及后缀 -->
        <property name="prefix" value="/"></property>
        <property name="suffix" value=".jsp"></property>
    </bean>
```

### 单一功能的视图解析器

+ InternalResourceViewResolver 

+ FreeMarkerViewResolver 

+ BeanNameViewResolver 

+ XmlViewResolver 



### 协商的视图解析器

+ ContentNegotiatingViewResolver



## springMVC 数据获取（使用JDBC）

![image-20210913140826138](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210913140826138.png)



## springMVC 实现图片上传

### 1、新建项目工程

### 2、配置Tomcat

### 3、编写pom.xml文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.xiang</groupId>
    <artifactId>viewDemo</artifactId>
    <version>1.0-SNAPSHOT</version>
    <name>viewDemo</name>
    <packaging>war</packaging>

    <!-- 依赖软件包版本 -->
    <properties>
        <commons-lang.version>2.6</commons-lang.version>
        <spring.version>4.1.3.RELEASE</spring.version>
        <slf4j.version>1.7.6</slf4j.version>
        <maven.compiler.target>1.8</maven.compiler.target>
        <maven.compiler.source>1.8</maven.compiler.source>
        <junit.version>5.7.1</junit.version>
    </properties>


    <!-- 配置依赖管理包 -->
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-framework-bom</artifactId>
                <version>${spring.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>


    <!-- sprint MVC 依赖包 -->
    <dependencies>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
            <version>3.2.3.RELEASE</version>
        </dependency>


        <!-- 通用依赖包  -->
        <dependency>
            <groupId>commons-lang</groupId>
            <artifactId>commons-lang</artifactId>
            <version>${commons-lang.version}</version>
        </dependency>

        <!-- log4j依赖包 -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-log4j12</artifactId>
            <version>1.7.6</version>
            <exclusions>
                <exclusion>
                    <groupId>org.slf4j</groupId>
                    <artifactId>slf4j-api</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>${slf4j.version}</version>
        </dependency>
        <!-- 文件上传 -->
        <dependency>
            <groupId>commons-fileupload</groupId>
            <artifactId>commons-fileupload</artifactId>
            <version>1.3.1</version>
        </dependency>

        <!-- jstl标签包 -->
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>jstl</artifactId>
            <version>1.2</version>
        </dependency>
        <!--		xml与实体转换依赖-->
        <dependency>
            <groupId>javax.xml.bind</groupId>
            <artifactId>jaxb-api</artifactId>
            <version>2.2.7</version>
        </dependency>

        <dependency>
            <groupId>com.sun.xml.bind</groupId>
            <artifactId>jaxb-impl</artifactId>
            <version>2.2.7</version>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-oxm</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <!-- json依赖包 -->
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-core</artifactId>
            <version>2.7.4</version>
        </dependency>

        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-annotations</artifactId>
            <version>2.7.4</version>
        </dependency>
        <dependency>
            <groupId>org.codehaus.jackson</groupId>
            <artifactId>jackson-mapper-asl</artifactId>
            <version>1.9.13</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>2.7.4</version>
        </dependency>


        <!-- mysql连接 -->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>8.0.25</version>
        </dependency>

        <!-- 数据源 c3p0 -->
        <dependency>
            <groupId>c3p0</groupId>
            <artifactId>c3p0</artifactId>
            <version>0.9.1.2</version>
        </dependency>

        <!--Spring dao管理  -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-dao</artifactId>
            <version>2.0.3</version>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.13</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <version>3.2.3.RELEASE</version>
        </dependency>
        <!--mybatis核心包-->
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis</artifactId>
            <version>3.2.8</version>
        </dependency>
        <!--		mybatis和spring集成包-->
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis-spring</artifactId>
            <version>1.3.0</version>
        </dependency>
        <!--      spring创建数据源的依赖包-->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-jdbc</artifactId>
            <version>4.2.5.RELEASE</version>
        </dependency>

        <!-- AspectJ -->
        <dependency>
            <groupId>org.aspectj</groupId>
            <artifactId>aspectjweaver</artifactId>
            <version>1.8.6</version>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-war-plugin</artifactId>
                <version>3.3.1</version>
            </plugin>
        </plugins>
    </build>
</project>
```

### 4、编写mvc-dispatcher-servlet.xml文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:context="http://www.springframework.org/schema/context"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xsi:schemaLocation="
        http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context
        http://www.springframework.org/schema/context/spring-context.xsd
        http://www.springframework.org/schema/mvc
        http://www.springframework.org/schema/mvc/spring-mvc.xsd">




    <!-- 扫描控制器 -->
    <context:component-scan base-package="com.xiang.controller">
        <context:include-filter type="annotation"
                                expression="org.springframework.stereotype.Controller" />
    </context:component-scan>

    <!-- 扩充了注解驱动，可以将请求参数绑定到控制器参数 -->
    <mvc:annotation-driven />

    <!-- 静态资源处理， css， js， imgs -->
<!--    <mvc:resources mapping="/resources/css/**" location="/resources/css/" />-->
<!--    <mvc:resources mapping="/resources/video/**" location="/resources/video/" />-->
<!--    <mvc:resources mapping="/resources/pic/**" location="/resources/pic/" />-->
    <mvc:resources mapping="/upload/**" location="/upload/" />

    <!--协商视图器，可以自定义解析器-->
    <bean class="org.springframework.web.servlet.view.ContentNegotiatingViewResolver">
        <property name="order" value="1"></property>
        <property name="mediaTypes">
            <map>
                <entry key="json" value="application/json"/>
                <entry key="xml" value="application/xml" />

            </map>
        </property>
        <property name="defaultViews">
            <list>
                <bean class="org.springframework.web.servlet.view.json.MappingJackson2JsonView"></bean>
                <bean class="org.springframework.web.servlet.view.xml.MarshallingView">
                    <constructor-arg>
                        <bean class="org.springframework.oxm.jaxb.Jaxb2Marshaller">
                            <property name="classesToBeBound">
                                <list>
                                    <value>com.xiang.pojo.User</value>
                                </list>
                            </property>
                        </bean>
                    </constructor-arg>
                </bean>

            </list>
        </property>
        <!--打开请求头-->
        <property name="ignoreAcceptHeader" value="false"></property>
    </bean>
    <!--文件上传解析器, resolveLazily属性启用是为了推迟文件解析，以便捕获文件大小异常 -->
    <bean id="multipartResolver"
          class="org.springframework.web.multipart.commons.CommonsMultipartResolver">
        <property name="maxUploadSize" value="209715200" />
        <property name="defaultEncoding" value="UTF-8" />
        <property name="resolveLazily" value="true" />
    </bean>
    <!-- 配置试图解析器 -->
    <bean
            class="org.springframework.web.servlet.view.InternalResourceViewResolver">
        <property name="viewClass"
                  value="org.springframework.web.servlet.view.JstlView"></property>
        <!-- 配置jsp文件前缀及后缀 -->
        <property name="prefix" value="/"></property>
        <property name="suffix" value=".jsp"></property>
    </bean>


</beans>
```

### 5、编写springApplication.xml文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
		http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
		">
</beans>
```

### 6、配置web.xml文件

```xml
<!DOCTYPE web-app>

<web-app>
    <display-name>Archetype Created Web Application</display-name>
    <!-- 欢迎页面 -->
    <welcome-file-list>
        <welcome-file>index.jsp</welcome-file>
    </welcome-file-list>

    <!-- 配置spring 上下文 -->
    <context-param>
        <param-name>contextConfigLocation</param-name>
        <param-value>classpath:springApplication.xml</param-value>
    </context-param>

    <!-- 配置监听器 -->
    <listener>
        <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
    </listener>

    <!-- spring mvc的配置上下文 -->
    <servlet>
        <servlet-name>mvc-dispatcher</servlet-name>
        <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>

        <init-param>
            <param-name>contextConfigLocation</param-name>
            <param-value>classpath:mvc-dispatcher-servlet.xml</param-value>
        </init-param>

        <load-on-startup>1</load-on-startup>
    </servlet>

    <servlet-mapping>
        <servlet-name>mvc-dispatcher</servlet-name>
        <!-- 拦截所有请求 -->
        <url-pattern>/</url-pattern>
    </servlet-mapping>

    <!-- 编码过滤器 -->
    <filter>
        <filter-name>characterEncodingFilter</filter-name>
        <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
        <init-param>
            <param-name>encoding</param-name>
            <param-value>UTF-8</param-value>
        </init-param>
        <init-param>
            <param-name>forceEncoding</param-name>
            <param-value>true</param-value>
        </init-param>
    </filter>
    <filter-mapping>
        <filter-name>characterEncodingFilter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
</web-app>

```

### 7、 编写controller.FileUploadController类

```java
package com.xiang.controller;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/13 11:02
 */
@Controller
@RequestMapping(value = "/file")
public class FileUploadController {
//    @RequestMapping(value = "/up", method = RequestMethod.POST)
//    public String uploadFile(@RequestParam("imgPath") MultipartFile file, Model model, MultipartHttpServletRequest request) throws Exception {
//        //判断目录是否在服务器上，如果没有我们创建一个目录，并把上传文件放在目录下面
//        if (!file.isEmpty()) {
//            String imgPath =
//                    request.getSession().getServletContext().getRealPath("")
//                    + file.getOriginalFilename();
//            System.out.println(imgPath);
//            //上传文件到指定目录
//            FileUtils.copyInputStreamToFile(file.getInputStream(), new File(imgPath));
//            model.addAttribute("url",imgPath);
//        }
//        return "view/res";
//    }
    @RequestMapping(value = "/up2", method = RequestMethod.POST)
    public  String  uploadFile2(@RequestParam("imgPath") MultipartFile imgPath, Model model, HttpServletRequest request) throws IOException {
        String realPath = request.getSession().getServletContext().getRealPath("/upload/");
        File dir = new File(realPath);
        if(!dir.exists()){
            dir.mkdirs();
        }
        // 获取文件名
        String originalFilename = imgPath.getOriginalFilename();
        // 文件存储位置
        File file =new File(dir,originalFilename);
        //  文件保存
        imgPath.transferTo(file);
            /*//上传图片
            FileUtils.copyInputStreamToFile(file.getInputStream(),new File(imgPath));*/
        System.out.println(file);
//            model.addAttribute("")
        model.addAttribute("url",file);
        model.addAttribute("name",originalFilename);
        return "view/res";
    }

}

```

### 8、编写controller.FileController类

```java
package com.xiang.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/13 11:12
 */
@Controller
public class FileController {
@RequestMapping(value = "/vi",method = RequestMethod.GET)
    public  String getToFile(){

    return  "view/toFile";
}
}
```

### 9、编写view.toFile.jsp文件

```jsp
<%--
  Created by IntelliJ IDEA.
  User: Xiang
  Date: 2021/9/13
  Time: 11:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>toFile</title>
</head>
<body>
<div>
<%--    <form action="<%=request.getContextPath()%>/file/up"  enctype="multipart/form-data" method="post">--%>
    <form action="<%=request.getContextPath()%>/file/up2"  enctype="multipart/form-data" method="post">
        <input type="file" name="imgPath">
        <input type="submit" value="上传">
    </form>
</div>
</body>
</html>

```

### 10、编写view.res.jsp文件

```jsp
<%--
  Created by IntelliJ IDEA.
  User: Xiang
  Date: 2021/9/13
  Time: 11:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <title>res</title>
</head>
<body>
<h3 style="color: red">
  success
</h3>
<%--<img src="${url}" width="500px" height="500px">--%>
<img src="http://127.0.0.1:8001/upload/${name}"/>
</body>
</html>
```

### 11、项目工程运行截图

https://img2020.cnblogs.com/blog/1868218/202109/1868218-20210914200852783-1305508728.png

https://img2020.cnblogs.com/blog/1868218/202109/1868218-20210914200906807-1070996790.png

![image-20210914200655990](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210914200655990.png)

![image-20210914200722419](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210914200722419.png)

![image-20210914200610767](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210914200610767.png)

## springMVC 视图解析

### 1、新建项目工程

### 2、配置Tomcat

### 3、编写pom.xml文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.xiang</groupId>
    <artifactId>viewDemo</artifactId>
    <version>1.0-SNAPSHOT</version>
    <name>viewDemo</name>
    <packaging>war</packaging>

    <!-- 依赖软件包版本 -->
    <properties>
        <commons-lang.version>2.6</commons-lang.version>
        <spring.version>4.1.3.RELEASE</spring.version>
        <slf4j.version>1.7.6</slf4j.version>
        <maven.compiler.target>1.8</maven.compiler.target>
        <maven.compiler.source>1.8</maven.compiler.source>
        <junit.version>5.7.1</junit.version>
    </properties>


    <!-- 配置依赖管理包 -->
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-framework-bom</artifactId>
                <version>${spring.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>


    <!-- sprint MVC 依赖包 -->
    <dependencies>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
            <version>3.2.3.RELEASE</version>
        </dependency>


        <!-- 通用依赖包  -->
        <dependency>
            <groupId>commons-lang</groupId>
            <artifactId>commons-lang</artifactId>
            <version>${commons-lang.version}</version>
        </dependency>

        <!-- log4j依赖包 -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-log4j12</artifactId>
            <version>1.7.6</version>
            <exclusions>
                <exclusion>
                    <groupId>org.slf4j</groupId>
                    <artifactId>slf4j-api</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>${slf4j.version}</version>
        </dependency>
        <!-- 文件上传 -->
        <dependency>
            <groupId>commons-fileupload</groupId>
            <artifactId>commons-fileupload</artifactId>
            <version>1.3.1</version>
        </dependency>

        <!-- jstl标签包 -->
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>jstl</artifactId>
            <version>1.2</version>
        </dependency>
        <!--		xml与实体转换依赖-->
        <dependency>
            <groupId>javax.xml.bind</groupId>
            <artifactId>jaxb-api</artifactId>
            <version>2.2.7</version>
        </dependency>

        <dependency>
            <groupId>com.sun.xml.bind</groupId>
            <artifactId>jaxb-impl</artifactId>
            <version>2.2.7</version>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-oxm</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <!-- json依赖包 -->
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-core</artifactId>
            <version>2.7.4</version>
        </dependency>

        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-annotations</artifactId>
            <version>2.7.4</version>
        </dependency>
        <dependency>
            <groupId>org.codehaus.jackson</groupId>
            <artifactId>jackson-mapper-asl</artifactId>
            <version>1.9.13</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>2.7.4</version>
        </dependency>


        <!-- mysql连接 -->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>8.0.25</version>
        </dependency>

        <!-- 数据源 c3p0 -->
        <dependency>
            <groupId>c3p0</groupId>
            <artifactId>c3p0</artifactId>
            <version>0.9.1.2</version>
        </dependency>

        <!--Spring dao管理  -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-dao</artifactId>
            <version>2.0.3</version>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.13</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <version>3.2.3.RELEASE</version>
        </dependency>
        <!--mybatis核心包-->
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis</artifactId>
            <version>3.2.8</version>
        </dependency>
        <!--		mybatis和spring集成包-->
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis-spring</artifactId>
            <version>1.3.0</version>
        </dependency>
        <!--      spring创建数据源的依赖包-->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-jdbc</artifactId>
            <version>4.2.5.RELEASE</version>
        </dependency>

        <!-- AspectJ -->
        <dependency>
            <groupId>org.aspectj</groupId>
            <artifactId>aspectjweaver</artifactId>
            <version>1.8.6</version>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-war-plugin</artifactId>
                <version>3.3.1</version>
            </plugin>
        </plugins>
    </build>
</project>
```

### 4、编写mvc-dispatcher-servlet.xml文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:context="http://www.springframework.org/schema/context"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xsi:schemaLocation="
        http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context
        http://www.springframework.org/schema/context/spring-context.xsd
        http://www.springframework.org/schema/mvc
        http://www.springframework.org/schema/mvc/spring-mvc.xsd">




    <!-- 扫描控制器 -->
    <context:component-scan base-package="com.xiang.controller">
        <context:include-filter type="annotation"
                                expression="org.springframework.stereotype.Controller" />
    </context:component-scan>

    <!-- 扩充了注解驱动，可以将请求参数绑定到控制器参数 -->
    <mvc:annotation-driven />

    <!-- 静态资源处理， css， js， imgs -->
<!--    <mvc:resources mapping="/resources/css/**" location="/resources/css/" />-->
<!--    <mvc:resources mapping="/resources/video/**" location="/resources/video/" />-->
<!--    <mvc:resources mapping="/resources/pic/**" location="/resources/pic/" />-->
    <mvc:resources mapping="/upload/**" location="/upload/" />

    <!--协商视图器，可以自定义解析器-->
    <bean class="org.springframework.web.servlet.view.ContentNegotiatingViewResolver">
        <property name="order" value="1"></property>
        <property name="mediaTypes">
            <map>
                <entry key="json" value="application/json"/>
                <entry key="xml" value="application/xml" />

            </map>
        </property>
        <property name="defaultViews">
            <list>
                <bean class="org.springframework.web.servlet.view.json.MappingJackson2JsonView"></bean>
                <bean class="org.springframework.web.servlet.view.xml.MarshallingView">
                    <constructor-arg>
                        <bean class="org.springframework.oxm.jaxb.Jaxb2Marshaller">
                            <property name="classesToBeBound">
                                <list>
                                    <value>com.xiang.pojo.User</value>
                                </list>
                            </property>
                        </bean>
                    </constructor-arg>
                </bean>

            </list>
        </property>
        <!--打开请求头-->
        <property name="ignoreAcceptHeader" value="false"></property>
    </bean>
    <!--文件上传解析器, resolveLazily属性启用是为了推迟文件解析，以便捕获文件大小异常 -->
    <bean id="multipartResolver"
          class="org.springframework.web.multipart.commons.CommonsMultipartResolver">
        <property name="maxUploadSize" value="209715200" />
        <property name="defaultEncoding" value="UTF-8" />
        <property name="resolveLazily" value="true" />
    </bean>
    <!-- 配置试图解析器 -->
    <bean
            class="org.springframework.web.servlet.view.InternalResourceViewResolver">
        <property name="viewClass"
                  value="org.springframework.web.servlet.view.JstlView"></property>
        <!-- 配置jsp文件前缀及后缀 -->
        <property name="prefix" value="/"></property>
        <property name="suffix" value=".jsp"></property>
    </bean>


</beans>
```

### 5、编写springApplication.xml文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
		http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
		">
</beans>
```

### 6、配置web.xml文件

```xml
<!DOCTYPE web-app>

<web-app>
    <display-name>Archetype Created Web Application</display-name>
    <!-- 欢迎页面 -->
    <welcome-file-list>
        <welcome-file>index.jsp</welcome-file>
    </welcome-file-list>

    <!-- 配置spring 上下文 -->
    <context-param>
        <param-name>contextConfigLocation</param-name>
        <param-value>classpath:springApplication.xml</param-value>
    </context-param>

    <!-- 配置监听器 -->
    <listener>
        <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
    </listener>

    <!-- spring mvc的配置上下文 -->
    <servlet>
        <servlet-name>mvc-dispatcher</servlet-name>
        <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>

        <init-param>
            <param-name>contextConfigLocation</param-name>
            <param-value>classpath:mvc-dispatcher-servlet.xml</param-value>
        </init-param>

        <load-on-startup>1</load-on-startup>
    </servlet>

    <servlet-mapping>
        <servlet-name>mvc-dispatcher</servlet-name>
        <!-- 拦截所有请求 -->
        <url-pattern>/</url-pattern>
    </servlet-mapping>

    <!-- 编码过滤器 -->
    <filter>
        <filter-name>characterEncodingFilter</filter-name>
        <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
        <init-param>
            <param-name>encoding</param-name>
            <param-value>UTF-8</param-value>
        </init-param>
        <init-param>
            <param-name>forceEncoding</param-name>
            <param-value>true</param-value>
        </init-param>
    </filter>
    <filter-mapping>
        <filter-name>characterEncodingFilter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
</web-app>

```

### 7、 编写pojo.User类

```java
package com.xiang.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/13 10:30
 */


@XmlRootElement
public class User {
    private int id;
    private String username;
    private String sex;
    private int age;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthday;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", sex='" + sex + '\'' +
                ", age=" + age +
                ", birthday=" + birthday +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
}
```

### 8、编写pojo.ViewController类

```java
package com.xiang.controller;

import com.xiang.pojo.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/13 10:31
 */
@Controller
public class ViewController {
    private static  User user = new User();

    @RequestMapping("/view")
    public String toView(Model model) {

        user.setId(123465);
        user.setUsername("小向");
        model.addAttribute("user",user);
        System.out.println("user:"+user);
        return  "view/user";
    }
}
```

### 9、编写view.user.jsp文件

```jsp
<%--
  Created by IntelliJ IDEA.
  User: Xiang
  Date: 2021/9/13
  Time: 10:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>user</title>
</head>
<body>
<h4>
    ContentNegotiatingViewResolver
</h4>
<p>
    ${user}
</p>


</body>
</html>
```

### 10、项目工程运行截图

https://img2020.cnblogs.com/blog/1868218/202109/1868218-20210914201931871-895155157.png

https://img2020.cnblogs.com/blog/1868218/202109/1868218-20210914201942429-1467747867.png

![image-20210914201805194](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210914201805194.png)

![image-20210914201746059](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210914201746059.png)

![image-20210914201715137](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210914201715137.png)

