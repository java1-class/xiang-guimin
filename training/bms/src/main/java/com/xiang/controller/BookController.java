package com.xiang.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xiang.pojo.Book;
import com.xiang.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * book: xiang
 * Date: 2021/9/28 1:01
 */
@Controller
public class BookController {
    @Autowired
    private BookService bookServiceImpl;

    //查询所有并分页显示
    @GetMapping("/list")
    public String queryAllbook(@RequestParam(value = "page", defaultValue = "1") Integer page, Model model){
        //获取指定页数据，每页显示5条数据
        PageHelper.startPage(page, 5);
        //紧跟的第一个select方法被分页
        List<Book> books = bookServiceImpl.queryAllBook();
        model.addAttribute("books",books);
        //使用PageInfo包装数据 navigatePages表示导航标签的数量
        PageInfo pageInfo = new PageInfo(books, 5);
        model.addAttribute("pageInfo", pageInfo);
        return null;
    }



    @PostMapping("/add")
    public String addbook(Book book) {
        int i = bookServiceImpl.addBook(book);
        return "redirect:/list";  // redirect 重定向/跳转
    }

    @GetMapping("/queryBookById/{bookId}")
    public String querybookById(@PathVariable("bookId") Integer bookId, Model model) {
        Book book = bookServiceImpl.queryBookById(bookId);
        model.addAttribute("book",book);
        return "update";  //转发（默认为）
    }

    @PostMapping("/update")
    public String queryBookById(Book book) {
        System.out.println(book);
        int i = bookServiceImpl.updateBook(book);
        return "redirect:/list";
    }

    @GetMapping("/delete/{bookId}")
    public String deletebook(@PathVariable("bookId") int bookId) {
        int i = bookServiceImpl.deleteBook(bookId);
        return "redirect:/list";
    }

    @GetMapping("/conditionQuery")
    public String deletebook(Book book,Model model) {
        List<Book> books = bookServiceImpl.conditionQuery(book);
        model.addAttribute("books",books);
        return "list";
    }




}
