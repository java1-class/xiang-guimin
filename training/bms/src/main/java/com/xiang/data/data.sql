-- 创建数据库
create
database webapp3 charset utf8mb4;
-- 创建用户名、密码
create
user'webapp3'@'localhost'identified by'webapp3';
-- 授权
grant all
on webapp3.*to'webapp3'@'localhost';
-- 用用户名、密码登录
mysql
-uwebapp3 -pwebapp3;

create table user
(
    id       int not null primary key auto_increment,
    username varchar(50), --用户名
    password varchar(50)  -- 密码
);

insert into user
    (`username`, `password`)
values ("xiang", "123456");

create table book
(
    bookId int not null primary key auto_increment, --图书id
    name   varchar(50),                             --图书名
    author varchar(50),                             --图书作者
    press  varchar(50),                             --图书出版社
    price  double(6, 2                              --图书单价
)
    );
insert into book (`name`, `author`, `press`, `price`)
values ("数据库系统实现", "加西亚-莫利纳(Hector Garcia-Molina)", "机械工业出版社", "99.99"),
       ("数据库系统基础教程", "（美）厄尔曼", "机械工业出版社", "89.99"),
       ("Three.js入门指南", "余振华", "人民邮电出版社", "89.99"),
       ("术与道 移动应用UI设计必修课", "张雯莉", "图灵社区", "89.99"),
       ("数学思维导论", "[美] Keith Devlin", "人民邮电出版社", "89.99"),
       ("JSON必知必会", "[美] Lindsay Bassett", "人民邮电出版社", "89.99"),
       ("第一本Docker书", "[澳]詹姆斯•特恩布尔（James Turnbull）", "人民邮电出版社", "89.99"),
       ("HTML5与WebGL编程", "[美] Tony Parisi", "人民邮电出版社", "89.99"),
       ("智能Web算法", "Haralambos Marmanis / Dmitry Babenko", "电子工业出版社", "89.99");
