package com.xiang.mapper;

import com.xiang.pojo.Book;
import com.xiang.pojo.User;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/28 1:04
 */
@Component
public interface BookMapper {
    /**
     * * bookId int not null primary key auto_increment, --图书id
     * * name   varchar(50),                             --图书名
     * * author varchar(50),                             --图书作者
     * * press  varchar(50),                             --图书出版社
     * * price  double(6, 2                              --图书单价
     *
     * @param book
     * @return
     */

    // 增
    int addBook(Book book);

    // 删
    int deleteBook(int bookId);

    // 改
    int updateBook(Book book);

    // 查所有
    List<Book> queryAllBook();

    // 根据id查一个
    Book queryBookById(int bookId);

    //条件查询
    List<Book> conditionQuery(Book book);

}
