package com.xiang.mapper;

import com.xiang.pojo.User;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/28 1:04
 */
@Component
public interface UserMapper {
    //登录验证
    User logincheck(User user);
    //注册
    void register(User user);

    //检查是否用户存在
    String checkUserName(String username);
}
