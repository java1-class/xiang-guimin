package com.xiang.service;

import com.xiang.pojo.Book;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/28 0:58
 */
public interface BookService {
    // 增
    int addBook(Book book);

    // 删
    int deleteBook(int bookId);

    // 改
    int updateBook(Book book);

    // 查所有
    List<Book> queryAllBook();

    // 根据id查一个
    Book queryBookById(int bookId);

    //条件查询
    List<Book> conditionQuery(Book book);
}
