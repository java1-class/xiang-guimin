package com.xiang.service;

import com.xiang.pojo.User;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/28 0:58
 */
public interface UserService {
    //登录验证
    User logincheck(User user);
    //注册
    void register(User user);


    //检查是否用户存在
    String checkUserName(String username);
}
