package com.xiang.service.impl;

import com.xiang.mapper.UserMapper;
import com.xiang.pojo.User;
import com.xiang.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/28 0:59
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public User logincheck(User user) {
        return userMapper.logincheck(user);

    }

    @Override
    public void register(User user) {
        userMapper.register(user);

    }

    @Override
    public String checkUserName(String username) {
        return userMapper.checkUserName(username);
    }

}
