package com.xiang.controller;

import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.HttpServletBean;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.resource.HttpResource;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/11 17:30
 */

@Controller

public class HomeController {
    private static Date date = new Date();

    @RequestMapping("/date")
    public ModelAndView test01() {
        date = new Date(); //服务器时间
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:ss:mm");
        String format = dateFormat.format(date);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
        String s = df.format(date);

        new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        date = new Date(System.currentTimeMillis());

        LocalDateTime.now(); // get the current date and time
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

        Calendar calendar = Calendar.getInstance(); // get current instance of the calendar
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        formatter.format(calendar.getTime());


//        返回服务器时间到前端
//        封装了数据和页面信息 ModelAndView
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("format", format);
        modelAndView.addObject("date", date);
        modelAndView.addObject("df", df);
        modelAndView.addObject("timeFormatter", timeFormatter);
        modelAndView.addObject("formatter", formatter);
        modelAndView.addObject("s", s);

//        视图解析
        modelAndView.setViewName("date/date");
        return modelAndView;
    }

    @RequestMapping("/date2")
    public String test02(ModelMap modelMap){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date2 = format.format(date);
        modelMap.addAttribute("date2",date2);
        return "date/date2";
    }

    @RequestMapping("/date3")
    public  String test03(Model model){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date3 = dateFormat.format(date);
        model.addAttribute("date3",date3);
        return "date/date3";
    }

    @RequestMapping("/date4")
    public  String test04(Map<String, Object> map){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date4 = simpleDateFormat.format(date);
        map.put("date4",date4);
        return "date/date4";
    }

}
