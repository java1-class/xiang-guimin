<%--
  Created by IntelliJ IDEA.
  User: Xiang
  Date: 2021/9/11
  Time: 17:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>date</title>
</head>
<body>
<b style="color:orangered">使用参数 ModelAndView   封装数据</b>
<h3>
    跳转成功，当前服务器时间: ${format}
</h3>
<h3>
    跳转成功，当前服务器时间: ${date}
</h3>
<h3>
    跳转成功，当前服务器时间: ${df}
</h3>
<h3>
    跳转成功，当前服务器时间: ${timeFormatter}
</h3>
<h3>
    跳转成功，当前服务器时间: ${formatter}
</h3>
<h3 style="color: red">
    跳转成功，当前服务器时间: ${s}
</h3>

</body>
</html>
