package com.xiang.controller;

import com.xiang.mapper.StudentMapper;
import com.xiang.pojo.Student;
import com.xiang.pojo.Users;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/18 10:02
 */
@Controller
@RequestMapping(value = "/student")
public class StudentController {

    @Autowired
    private  StudentMapper studentMapper;

    @RequestMapping(value = "/list")
    public String list(Model model) {
       model.addAttribute("list",studentMapper.selectStudent());
        return "student/list";
    }


    @RequestMapping(value = "/del/{s_id}",method = RequestMethod.POST)
    @ResponseBody
    public String del(@PathVariable("s_id") int s_id )  {
        boolean b = studentMapper.deleteStudent(s_id);
        if (b) {
            return "success";
        }
        return "fail";
    }

    @RequestMapping(value = "/add" ,method = RequestMethod.POST)
    @ResponseBody
    public  String add(@RequestBody Student student){
        System.out.println(student);
        boolean b = studentMapper.addStudent(student);
        if (b){
            return "success";
//            return "{\"success\":true,\"id\":\"1\"}";
        }
        return "fail";
//        return "{\"success\":true,\"id\":\"1\"}";
    }


    @RequestMapping("/getid/{s_id}")
    public String getId(@PathVariable("s_id") int s_id, Model model) throws SQLException {
        Student student = new Student();
        student.setS_id(s_id);
        model.addAttribute("model", studentMapper.list_id(student));
        return "student/update";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public Object save(@RequestBody Student student) {
        Integer integer = studentMapper.updateStudent(student);
        if (integer>0){
            return "success";
        }
        return null;
    }












}
