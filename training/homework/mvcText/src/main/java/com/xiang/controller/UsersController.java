package com.xiang.controller;

import cn.hwadee.utils.Page;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONPObject;
import com.xiang.DBTest;
import com.xiang.pojo.OrderQueryVo;
import com.xiang.pojo.Users;
import com.xiang.service.UsersService;
import com.xiang.service.impl.UsersServiceImpl;
import jdk.nashorn.api.scripting.JSObject;
import org.codehaus.jackson.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/13 15:16
 */
@Controller
@RequestMapping("/users")
public class UsersController {
//    UsersService usersService = new UsersServiceImpl();
    @Autowired
    private UsersService usersService;


    @RequestMapping(value = "/login")
    public String getLogin(Model model, Users users,HttpServletRequest request) throws Exception {
        HttpSession session = request.getSession();
        session.setAttribute("UsersSession",users);
        boolean res = usersService.res(users);
        if (res) {
            System.out.println("ok");
            List<Users> all = usersService.getAll();
            model.addAttribute("all", all);

            return "users/all";

        } else {
            System.out.println("NO");
        }
        return "users/fail";
    }

    @RequestMapping(value = "/logout")
    public  String getLogout(HttpServletRequest request){
        HttpSession session = request.getSession();
        if (session != null){
            session.removeAttribute("UsersSession");
        }
        return "users/login";

    }


    @RequestMapping(value = "/list")
//    @RequestMapping("/query")
    public String getAll(Model model, OrderQueryVo vo, HttpServletRequest request) throws SQLException {
        List<Users> all = usersService.getAll();
//        System.out.println(all);
        model.addAttribute("all", all);
        Page<Users> list = usersService.getPageList(vo);

        model.addAttribute("page",list);
        model.addAttribute("usersList",list.getRows());
        return "/users/all";
    }

//    @RequestMapping({"/slist","/showList"})
////    @RequestMapping({"/userlist","/showList"})
//    public String list(OrderQueryVo vo, Model model) throws Exception{
//
//        Page<Users> pageList = usersService.getPageList(vo);
//        model.addAttribute("page",pageList);
//        model.addAttribute("orderList",pageList.getRows());
//
//        return "users/all";
//    }


    @RequestMapping("/getid/{id}")
    public String getId(@PathVariable("id") int id, Model model) throws SQLException {
        System.out.println("id===============>" + id);
        Users id1 = usersService.getId(id);
        model.addAttribute("id1", id1);
        return "users/update";
    }

//    @RequestMapping(value = "/save" ,method = RequestMethod.POST)
//    public String save(Users users,Model model) throws SQLException {
//        boolean save = usersService.save(users);
//        if (save) {
//            List<Users> all = usersService.getAll();
//            model.addAttribute("all", all);
//            return "/users/all";
//        }
//        model.addAttribute("all", "fail");
//        return "users/fail";
//    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public Object save2(@RequestBody Users users) {
        System.out.println(users);
        boolean save = usersService.save(users);
        if (save) {
//            List<Users> all = usersService.getAll();
//            model.addAttribute("all", all);
////            return "{\"success\":true,\"id\":\"1\"}";
////            return "/users/all";
//
//            JSONObject object = new JSONObject();
//            object.put("id", "id");
            return "修改成功";


        } else {
//            model.addAttribute("all", "fail");
//            return "{\"success\":true,\"id\":\"1\"}";

            return "修改失败";
        }

    }


//    @RequestMapping(value = "/getdel/{id}")
//    public String getdel(@PathVariable("id") int id, Model model) throws SQLException {
//        boolean delId = usersService.delId(id);
//        if (delId) {
//            List<Users> all = usersService.getAll();
//            model.addAttribute("all", all);
//            return "/users/all";
//        }
//        model.addAttribute("all", "fail");
//        return "users/fail";
//    }

    @RequestMapping(value = "/getdel/{id}",method = RequestMethod.POST)
    @ResponseBody
    public String getdel(@PathVariable("id") int id )  {
        boolean delId = usersService.delId(id);
        if (delId) {
//            List<Users> all = usersService.getAll();
//            model.addAttribute("all", all);
            return "success";
        }
//        model.addAttribute("all", "fail");
        return "fail";
    }

//    @RequestMapping(value = "/add",method = RequestMethod.POST)
//    public  String getAdd(Users users,Model model) throws SQLException {
//        boolean serviceAdd = usersService.getAdd(users);
//        if (serviceAdd){
//            List<Users> all = usersService.getAll();
//            model.addAttribute("all", all);
//            return "/users/all";
//        }
//        model.addAttribute("all", "fail");
//        return "users/fail";
//    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public Object addOrder(@RequestBody Users users) {
        System.out.println(users);
        boolean result = usersService.getAdd(users);
        if (result) {
            return "{\"success\":true,\"id\":\"1\"}";
        } else {
            return "{\"success\":true,\"id\":\"1\"}";
        }
    }

    @RequestMapping(value = "/showList")
    public String showList(Model model) {
        List<Users> list = usersService.getAll();
        model.addAttribute("list", list);
        return "/users/all";
    }




}
