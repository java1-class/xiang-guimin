package com.xiang.dao;

import com.xiang.DBTest;
import com.xiang.pojo.OrderQueryVo;
import com.xiang.pojo.Users;
import org.springframework.ui.ModelMap;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/13 15:10
 */
public interface UsersDao {
    public boolean getLogin(Users users);
    public List<Users> getAll();
    public Users getId(int id);
    public boolean save(Users users);
    public boolean delId(int id);
    public boolean getAdd(Users users);

    public List<Users> findOrderByVo(OrderQueryVo vo);
    public Integer findOrderByVoCount(OrderQueryVo vo);

}
