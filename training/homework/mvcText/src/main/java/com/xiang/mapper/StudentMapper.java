package com.xiang.mapper;

import com.xiang.pojo.Student;
import com.xiang.pojo.User;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/18 9:28
 */
public interface StudentMapper {
    /**
     * 增
     */
    public boolean addStudent(Student student);

    /**
     * 删
     *
     * @param s_id
     */
    public boolean deleteStudent(Integer s_id);

    /**
     * 改
     */
    public Integer updateStudent(Student student);

    /**
     * 查
     */
    public List<Student> selectStudent();

    /**
     * 查询 id
     * @param student
     * @return
     */
    public Student list_id(Student student);


}
