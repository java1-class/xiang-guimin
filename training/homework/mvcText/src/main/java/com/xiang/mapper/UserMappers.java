package com.xiang.mapper;

import com.xiang.pojo.User;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/16 14:27
 */
public interface UserMappers {
    public boolean insertUser(User user);

    public boolean delUser(Integer id);

    public boolean updateUser(User user);

    public User queryUser(User user);

    public List<User> selectUser(User user);

    public List<User> selectUserList(User user);

    public List<User> listOr(User user);

    public List<User> listLike(String username);

    public User list_id(User user);

    public Integer list_idRandom(User user);

    //where元素
    public List<User> listAll(User user);

    //    trim元素
    public List<User> listTrim(User user);

    //    set元素
    public Integer updateList(User user);
}
