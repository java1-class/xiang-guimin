package com.xiang.mapper;

import com.xiang.pojo.Users;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/16 11:01
 */
public interface UsersMapper {
    //查询用户
    public Users getLoginUser(Users users);

    //注册用户
    public void registerUser(Users users);

}
