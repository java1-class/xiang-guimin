package com.xiang.pojo;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/15 22:37
 */
public class OrderQueryVo {
    private Integer page = 1;
    private Integer start;
    private Integer size = 5;

    @Override
    public String toString() {
        return "OrderQueryVo{" +
                "page=" + page +
                ", start=" + start +
                ", size=" + size +
                '}';
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }
}
