package com.xiang.pojo;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/18 9:19
 */

/**
 * s_id      int not null primary key auto_increment,
 * s_number  int,
 * s_name    varchar(10),
 * s_age     int,
 * s_tel     varchar(11),
 * s_address varchar(20),
 * s_score   double(6, 2
 */
public class Student {
    private  Integer s_id;
    private  int s_number;
    private  String s_name;
    private  int s_age;
    private  String s_tel;
    private  String s_address;
    private  Double s_score;

    @Override
    public String toString() {
        return "Student{" +
                "s_id=" + s_id +
                ", s_number=" + s_number +
                ", s_name='" + s_name + '\'' +
                ", s_age=" + s_age +
                ", s_tel='" + s_tel + '\'' +
                ", s_address='" + s_address + '\'' +
                ", s_score=" + s_score +
                '}';
    }

    public int getS_id() {
        return s_id;
    }

    public void setS_id(int s_id) {
        this.s_id = s_id;
    }

    public int getS_number() {
        return s_number;
    }

    public void setS_number(int s_number) {
        this.s_number = s_number;
    }

    public String getS_name() {
        return s_name;
    }

    public void setS_name(String s_name) {
        this.s_name = s_name;
    }

    public int getS_age() {
        return s_age;
    }

    public void setS_age(int s_age) {
        this.s_age = s_age;
    }

    public String getS_tel() {
        return s_tel;
    }

    public void setS_tel(String s_tel) {
        this.s_tel = s_tel;
    }

    public String getS_address() {
        return s_address;
    }

    public void setS_address(String s_address) {
        this.s_address = s_address;
    }

    public Double getS_score() {
        return s_score;
    }

    public void setS_score(Double s_score) {
        this.s_score = s_score;
    }
}
