package com.xiang.service;

import com.xiang.pojo.User;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/16 14:40
 */
public interface QueryUserService {

    public User queryUser(User user);



    public boolean insertUser(User user);

    public boolean delUser(Integer id);

    public boolean updateUser(User user);


    public List<User> selectUser(User user);

    public List<User> selectUserList(User user);

    public List<User> listOr(User user);

    public List<User> listLike(String username);

    public User list_id(User user);

    public Integer list_idRandom(User user);

}
