package com.xiang.service;

import com.xiang.pojo.Users;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/16 11:06
 */
public interface UserLoginService {
    public Users getLoginUser(Users users);  //用户登录

    public void registerUser(Users users);  //用户注册

}
