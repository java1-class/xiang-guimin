package com.xiang.service;

import cn.hwadee.utils.Page;
import com.xiang.dao.UsersDao;
import com.xiang.pojo.OrderQueryVo;
import com.xiang.pojo.Users;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/13 15:09
 */
public interface UsersService {
    public boolean res(Users users);
    public List<Users> getAll();
    public Users getId(int id);
    public boolean save(Users users);
    public boolean delId(int id);
    public boolean getAdd(Users users);

    public List<Users> findOrderByVo(OrderQueryVo vo);

    Page<Users> getPageList(OrderQueryVo vo);

}
