package com.xiang.service.impl;

import com.xiang.mapper.UserMappers;
import com.xiang.pojo.User;
import com.xiang.service.QueryUserService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/16 14:42
 */
public class QueryUserServiceImpl implements QueryUserService {
    @Autowired
    private UserMappers userMappers;

    @Override
    public User queryUser(User user) {
        return userMappers.queryUser(user);
    }

    @Override
    public boolean insertUser(User user) {
        return userMappers.insertUser(user);
    }

    @Override
    public boolean delUser(Integer id) {
        return userMappers.delUser(id);
    }

    @Override
    public boolean updateUser(User user) {
        return userMappers.updateUser(user);
    }

    @Override
    public List<User> selectUser(User user) {
        return (List<User>) userMappers.selectUser(user);
    }

    @Override
    public List<User> selectUserList(User user) {
        return userMappers.selectUserList(user);
    }

    @Override
    public List<User> listOr(User user) {
        return userMappers.listOr(user);
    }

    @Override
    public List<User> listLike(String user) {
        return userMappers.listLike(user);
    }

    @Override
    public User list_id(User user) {
        return userMappers.list_id(user);
    }

    @Override
    public Integer list_idRandom(User user) {
        return userMappers.list_idRandom(user);
    }


}
