package com.xiang.service.impl;

import com.xiang.mapper.UsersMapper;
import com.xiang.pojo.Users;
import com.xiang.service.UserLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/16 11:07
 */

public class UserLoginServiceImpl implements UserLoginService {
    @Autowired
    private UsersMapper usersMapper;


    @Override
    public Users getLoginUser(Users users) {
        return usersMapper.getLoginUser(users);
    }

    @Override
    public void registerUser(Users users) {
        usersMapper.registerUser(users);

    }
}
