package com.xiang.service.impl;

import cn.hwadee.utils.Page;
import com.xiang.dao.UsersDao;
import com.xiang.dao.impl.UserDaoImpl;
import com.xiang.pojo.OrderQueryVo;
import com.xiang.pojo.Users;
import com.xiang.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/14 11:14
 */
public class UsersServiceImpl implements UsersService {
//    UsersDao usersDao = new UserDaoImpl();
    @Autowired
    private UsersDao usersDao;

    /*
    登录验证
     */
    public boolean res(Users users)  {
        return usersDao.getLogin(users);

    }

    /*
    查询所有
     */
    public List<Users> getAll() {
        return usersDao.getAll();
    }

    /*
    修改
     */
    public Users getId(int id)  {
        return usersDao.getId(id);
    }

    /*
    修改 执行
     */
    public boolean save(Users users) {
        return usersDao.save(users);
    }

    /*
    删除
     */
    public boolean delId(int id) {
        return usersDao.delId(id);
    }

    /*
    新增

     */
    public boolean getAdd(Users users) {
        return usersDao.getAdd(users);
    }


    public Page<Users> getPageList(OrderQueryVo vo) {
        if (vo.getPage() == null){
            vo.setPage(1);
        }
        // 设置查询的起始记录数
        vo.setStart((vo.getPage() - 1) * vo.getSize());

        // 查询数据列表和数据总数
        List<Users> orderByVo = usersDao.findOrderByVo(vo);
        Integer count = usersDao.findOrderByVoCount(vo);

        Page<Users> page = new Page<>();
        page.setTotal(count);
        page.setSize(vo.getSize());
        page.setPage(vo.getPage());
        page.setRows(orderByVo);
        return page;
    }

    /*
    分页
     */
//    public Page<Users> getPageList(Users users){
//        if (users.getPage() == null){
//            users.setPage(1);
//        }

//    }

    public List<Users> findOrderByVo(OrderQueryVo vo){
       return   usersDao.findOrderByVo(vo);
    }
}
