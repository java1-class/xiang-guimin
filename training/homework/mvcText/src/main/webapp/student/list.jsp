<%--
  Created by IntelliJ IDEA.
  User: Xiang
  Date: 2021/9/18
  Time: 10:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>list</title>
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://cdn.staticfile.org/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdn.staticfile.org/popper.js/1.15.0/umd/popper.min.js"></script>
    <script src="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<script type="text/javascript">
    function deleteId(s_id) {
        let url = "http://localhost:8001/mvcText/student/del/" + s_id
            // /del/{s_id}

        $.ajax({
            type: "POST",
            // dataType: "json",
            contentType: "application/json;charset=UTF-8",
            url: url,
            data: JSON.stringify(
                {
                    s_id: s_id
                }
            ),
            success: function (data) {
                console.log(data)
                alert("success")
                window.location.href = "http://localhost:8001/mvcText/student/list";
            },
            error: function (error) {
                console.log(error)
                alert("fail");
            }
        })
    }
</script>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <%--    <a class="navbar-brand" href="#">关于</a>--%>
    <button type="button" class="btn btn-secondary">关于</button>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <%--                <a class="nav-link" href="#">主页 <span class="sr-only">(current)</span></a>--%>
                <a href="<%=request.getContextPath()%>/student/list" type="button" class="btn btn-info">主页</a>
            </li>
            <li class="nav-item">
                <%--                <a class="nav-link" href="#">新增</a>--%>
                <a href="<%=request.getContextPath()%>/student/add.jsp" class="btn btn-primary">新增</a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">搜索</button>
            <a href="<%=request.getContextPath()%>/student/logout" class="btn btn-outline-success my-2 my-sm-0"
               type="submit">注销</a>
        </form>
    </div>
</nav>
<%--private  int s_id;--%>
<%--private  int s_number;--%>
<%--private  String s_name;--%>
<%--private  int s_age;--%>
<%--private  String s_tel;--%>
<%--private  String s_address;--%>
<%--private  Double s_score;--%>

<table class="table table-bordered">
    <tr>
        <th>s_id</th>
        <th>s_number</th>
        <th>s_name</th>
        <th>s_age</th>
        <th>s_tel</th>
        <th>s_address</th>
        <th>s_score</th>
        <th colspan="2">operation</th>
    </tr>
    <c:forEach items="${list}" var="model">
        <tr>
            <td>
                    ${model.s_id}
            </td>
            <td>
                    ${model.s_number}
            </td>
            <td>
                    ${model.s_name}
            </td>
            <td>
                    ${model.s_age}
            </td>
            <td>
                    ${model.s_tel}
            </td>
            <td>
                    ${model.s_address}
            </td>
            <td>
                    ${model.s_score}
            </td>
            <td><a href="<%=request.getContextPath()%>/student/getid/${model.s_id}" class="btn btn-success">修改</a></td>
                <%--                                <td><a  href="<%=request.getContextPath()%>/del/${model.s_id} " class="btn btn-danger">删除</a></td>--%>
            <td><a class="btn btn-danger" onclick="deleteId('${model.s_id}')">删除</a></td>
                <%--                <td><a href="<%=request.getContextPath()%>/users/add.jsp" class="btn btn-primary">新增</a></td>--%>

        </tr>
    </c:forEach>
</table>
</body>
</html>
