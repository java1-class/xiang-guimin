<%--
  Created by IntelliJ IDEA.
  User: Xiang
  Date: 2021/9/18
  Time: 15:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>update</title>
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://cdn.staticfile.org/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdn.staticfile.org/popper.js/1.15.0/umd/popper.min.js"></script>
    <script src="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<script type="text/javascript">
    function add() {
        let url = "http://localhost:8001/mvcText/student/save"

        $.ajax({
            type: "POST",
            // dataType: "json",
            contentType: "application/json;charset=UTF-8",
            url: url,
            data: JSON.stringify(
                {
                    <%--        (`s_number`, `s_name`, `s_age`, `s_tel`, `s_address`, `s_score`)--%>
                    s_id: $('#s_id').val(),
                    s_number: $('#s_number').val(),
                    s_name: $('#s_name').val(),
                    s_age: $('#s_age').val(),
                    s_tel: $('#s_tel').val(),
                    s_address: $('#s_address').val(),
                    s_score: $('#s_score').val(),
                }
            ),
            success: function (data) {
                window.location.href = "http://localhost:8001/mvcText/student/list";
                console.log(data)
                alert("success")
            },
            error: function (error) {
                console.log(error)
                alert("fail");
            }
        })
    }
</script>
<body>
${model}

<div class="container">
    <table border="1">
        <%--        (`s_number`, `s_name`, `s_age`, `s_tel`, `s_address`, `s_score`)--%>

        <tr>
            <td>
                编号
            </td>
            <td>
                <input type="text" id="s_id" name="s_id" value="${model.s_id}" readonly >
            </td>
        </tr>
            <tr>
            <td>
                学号
            </td>
            <td>
                <input type="text" id="s_number" name="s_number" value="${model.s_number}" >
            </td>
        </tr>
        <tr>
            <td>
                姓名
            </td>
            <td>
                <input type="text" id="s_name" name="s_name" value="${model.s_name}">
            </td>
        </tr>
        <tr>
            <td>
                年龄
            </td>
            <td>
                <input type="text" id="s_age" name="s_age" value="${model.s_age}">
            </td>
        </tr>
        <tr>
            <td>
                电话
            </td>
            <td>
                <input type="text" id="s_tel" name="s_tel" value="${model.s_tel}">
            </td>
        </tr>
        <tr>
            <td>
                地址
            </td>
            <td>
                <input type="text" id="s_address" name="s_address" value="${model.s_address}">
            </td>
        </tr>
        <tr>
            <td>
                成绩
            </td>
            <td>
                <input type="text" id="s_score" name="s_score" value="${model.s_score}">
            </td>
        </tr>
        <tr>
            <td>操作</td>
            <td><input type="submit" value="保存" onclick="add()" style="margin-left: 30px"/>

                <input type="button" onclick="history.go(-1)" value="取消"/>
            </td>
        </tr>


    </table>
</div>
</body>
</html>
