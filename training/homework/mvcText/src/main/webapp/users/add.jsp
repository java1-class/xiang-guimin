<%--
  Created by IntelliJ IDEA.
  User: Xiang
  Date: 2021/9/14
  Time: 9:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://cdn.staticfile.org/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdn.staticfile.org/popper.js/1.15.0/umd/popper.min.js"></script>
    <script src="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>

</head>

<script type="text/javascript">
    function add() {
        let url = "http://localhost:8001/mvcText/users/add"
        // alert(url)
        // alert("   function add() ")

        $.ajax({
            type: "POST",
            dataType: "json",

            contentType: "application/json;charset=UTF-8",
            url: url,
            data: JSON.stringify(
                {
                    account: $('#account').val(),
                    password: $('#password').val(),
                    username: $('#username').val(),
                    sex: $('#sex').val(),
                    age: $('#age').val(),
                    birthday: $('#birthday').val(),
                }
            ),
            success: function (data) {
                // console.log(data.id);
                // window.location.href = "http://localhost:8001/mvcText/users/all.jsp";
                window.location.href = "http://localhost:8001/mvcText/users/list";
                // window.location.href = "http://localhost:8001/mvcText/query";
                // window.location.href = "query";
                // alert(data.id)
                console.log(data)
                alert("success")
            },
            error:function (error) {
                console.log(error)
                alert("fail");
            }
        })
    }
</script>

<body>
<%--add--%>

<div class="container">
    <table border="1">
        <%--        //(`account`, `password`, `username`, `sex`, `age`, `birthday`)--%>

        <tr>
            <td>
                账号
            </td>
            <td>
                <input type="text" id="account" name="account">
            </td>
        </tr>
        <tr>
            <td>
                密码
            </td>
            <td>
                <input type="text" id="password" name="password">
            </td>
        </tr>
        <tr>
            <td>
                姓名
            </td>
            <td>
                <input type="text" id="username" name="username">
            </td>
        </tr>
        <tr>
            <td>
                性别
            </td>
            <td>
                <input type="text" id="sex" name="sex">
            </td>
        </tr>
        <tr>
            <td>
                年龄
            </td>
            <td>
                <input type="number" id="age" name="age">
            </td>
        </tr>
        <tr>
            <td>
                生日
            </td>
            <td>
                <input type="date" id="birthday" name="birthday">
            </td>
        </tr>
        <tr>
            <td>操作</td>
            <td><input type="submit" value="保存" onclick="add()" style="margin-left: 30px"/>

                <input type="button" onclick="history.go(-1)" value="取消"/>
            </td>
        </tr>


    </table>
</div>

<%--<hwadee:page url="${pageContext.request.contextPath }/order/list.action" />--%>
</body>
</html>
