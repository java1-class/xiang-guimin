package com.xiang;

import com.xiang.mapper.StudentMapper;
import com.xiang.mapper.UserMappers;
import com.xiang.mapper.UsersMapper;
import com.xiang.pojo.OrderQueryVo;
import com.xiang.pojo.Student;
import com.xiang.pojo.User;
import com.xiang.pojo.Users;
import com.xiang.service.QueryUserService;
import com.xiang.service.UserLoginService;
import com.xiang.service.UsersService;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/15 15:25
 */

@ContextConfiguration("classpath:springApplication.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class UsersTest {
//    private UsersService userService = null;

    //获取容器中的 beanFactory
    @Autowired
    private BeanFactory beanFactory;

    //获取容器中的   sqlSessionFactory;
    @Autowired
    private SqlSessionFactory sqlSessionFactory;


    @Test
    public  void  list_id2(){
        SqlSession sqlSession = sqlSessionFactory.openSession();
        StudentMapper mapper = sqlSession.getMapper(StudentMapper.class);
        Student student = new Student();
        student.setS_id(50);
        System.out.println(mapper.list_id(student));
    }

    @Test
//    s_number, s_name,s_age,s_tel,s_address,s_score
    public  void  addStudent(){
        SqlSession session = sqlSessionFactory.openSession();
        StudentMapper mapper = session.getMapper(StudentMapper.class);
        Student student = new Student();
        student.setS_number(193);
        student.setS_name("向");
        student.setS_age(19);
        student.setS_tel("135****");
        student.setS_address("四川");
        student.setS_score(92.2);
        System.out.println(mapper.addStudent(student));

    }

    @Test
    public void selectStudent() {
        SqlSession session = sqlSessionFactory.openSession();
        StudentMapper mapper = session.getMapper(StudentMapper.class);
        Student student = new Student();
        List<Student> list = mapper.selectStudent();
        for (Student s : list
        ) {
            System.out.println(s);
        }
    }

    @Test
//    set元素
//      public  Integer  updateList(User user);
    public void updateList() {
        SqlSession session = sqlSessionFactory.openSession();
        UserMappers mapper = session.getMapper(UserMappers.class);
        User user = new User();
        user.setId(15);
        user.setUsername("小不儿");
        user.setSex("男");
        Integer integer = mapper.updateList(user);
        if (integer > 0) {
            System.out.println("修改成功");
        }
        session.close();
    }


    @Test
//          //    trim元素
//     public List<User> listTrim(User user);
    public void listTrim() {
        SqlSession session = sqlSessionFactory.openSession();
        UserMappers mapper = session.getMapper(UserMappers.class);
        User user = new User();
        user.setUsername("小小");
        user.setSex(null);
        List<User> list = mapper.listAll(user);
        for (User u : list
        ) {
            System.out.println(u);
        }
    }

    @Test
//    where元素  查询，username sex;
    public void listAll() {
        SqlSession session = sqlSessionFactory.openSession();
        UserMappers mapper = session.getMapper(UserMappers.class);
        User user = new User();
        user.setUsername("小向");
        user.setSex(null);
        List<User> list = mapper.listAll(user);
        for (User u : list
        ) {
            System.out.println(u);
        }
    }

//     <!--    public List<User> listLike(User user);-->
//
//    <!--    public User list_id(User user);-->

    @Test
    public void list_idRandom() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMappers mapper = sqlSession.getMapper(UserMappers.class);
        User user = new User();
        user.setUsername("小小");
        user.setSex("女");
        user.setAge(18);
        user.setBirthday(Date.valueOf("2020-2-2"));
        System.out.println(mapper.list_idRandom(user));
    }

    @Test
    public void listLike() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMappers mapper = sqlSession.getMapper(UserMappers.class);
        System.out.println(mapper.listLike("小向"));
    }

    @Test
    public void list_id() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMappers mapper = sqlSession.getMapper(UserMappers.class);
        User user = new User();
        user.setUsername("小小小");
        user.setSex("男");
        user.setAge(18);
        user.setBirthday(Date.valueOf("2020-2-2"));
        System.out.println(mapper.list_id(user));
    }


    //    或 查询
    @Test
    public void listOr() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMappers mapper = sqlSession.getMapper(UserMappers.class);
        User user = new User();
        user.setUsername(null);
        user.setSex("女");
        user.setAge(19);
        System.out.println(mapper.listOr(user));
    }

    //    动态sql 测试  与查询
    @Test
    public void selectUserList() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMappers mapper = sqlSession.getMapper(UserMappers.class);
        User user = new User();
        user.setUsername("小向");
        user.setSex("男");
        user.setAge(19);
        System.out.println(mapper.selectUserList(user));
    }

    //    动态sql 测试 与查询
    @Test
    public void selectUserList2() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMappers mapper = sqlSession.getMapper(UserMappers.class);
        User user = new User();
        user.setUsername(null);
        user.setSex("男");
        user.setAge(19);
        System.out.println(mapper.selectUserList(user));
    }

    //    动态sql 测试 多条件 与查询
    @Test
    public void selectUserList3() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMappers mapper = sqlSession.getMapper(UserMappers.class);
        User user = new User();
        user.setUsername("小二");
        user.setSex(null);
        user.setAge(19);

        System.out.println(mapper.selectUserList(user));
    }

    //    动态sql 测试
    @Test
    public void select() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMappers mapper = sqlSession.getMapper(UserMappers.class);
        User user = new User();
        List<User> selectUser = mapper.selectUser(user);
//        System.out.println(selectUser);
        for (User u : selectUser
        ) {
            System.out.println(u);
        }
    }

    //查询 id、username
    @Test
    public void selectUser() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMappers mapper = sqlSession.getMapper(UserMappers.class);
        User user = new User();
        user.setId(1);
        user.setUsername("小向");
        System.out.println(mapper.queryUser(user));
    }

    //    更新
    @Test
    public void upUser() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMappers mapper = sqlSession.getMapper(UserMappers.class);
        User user = new User();
        user.setId(10);
        user.setUsername("小向");
        user.setAge(19);
        user.setSex("男");
        user.setBirthday(Date.valueOf("2020-1-1"));
        System.out.println(mapper.updateUser(user));


    }


    /**
     * public boolean insertUser(User user);
     * <p>
     * public boolean delUser(Integer id);
     * <p>
     * public boolean updateUser(Integer id);
     * <p>
     * public User queryUser(User user);
     */

//    删除
    @Test
    public void delUser() {
        QueryUserService queryUserService = beanFactory.getBean("queryUserService", QueryUserService.class);
        boolean delUser = queryUserService.delUser(6);
        System.out.println(delUser);

    }

    //    新增
    @Test
    public void insertUser() {
        QueryUserService queryUserService = beanFactory.getBean("queryUserService", QueryUserService.class);
        User user = new User();
        user.setUsername("***");
        user.setSex("男");
        System.out.println(queryUserService.insertUser(user));
    }

    //    修改
    @Test
    public void updateUser() {
        QueryUserService queryUserService = beanFactory.getBean("queryUserService", QueryUserService.class);
        User user = new User();
        user.setId(2);
        user.setUsername("嗨嗨嗨");
        user.setSex("女");
        System.out.println(queryUserService.updateUser(user));
    }

    //    查询
    @Test
    public void queryUser() {
        QueryUserService queryUserService = beanFactory.getBean("queryUserService", QueryUserService.class);
        User user = new User();
        user.setUsername("小向");
        user.setId(1);
        System.out.println(queryUserService.queryUser(user));
    }


    //    新增 ；注册
    @Test
    public void register() {
        UserLoginService userLoginService = beanFactory.getBean("userLoginService", UserLoginService.class);
        Users users = new Users();
        users.setAccount("xiang");
        users.setPassword("xiang");
        userLoginService.registerUser(users);
        System.out.println(users);
        System.out.println(userLoginService);

    }

    //    查询
    @Test
    public void login() {
        UserLoginService userLoginService = beanFactory.getBean("userLoginService", UserLoginService.class);
        Users users = new Users();
        users.setAccount("xiang");
        users.setPassword("wdeda");
        System.out.println(userLoginService.getLoginUser(users));

    }

    //    查询 所有
    @Test
    public void list() {

        UsersService userService = beanFactory.getBean("usersService", UsersService.class);
        List<Users> list = userService.getAll();
        for (Users users : list) {
            System.out.println(users);
        }
    }

    //    添加、新增
    @Test
    public void add() {
        UsersService serService = beanFactory.getBean("usersService", UsersService.class);
        Users users = new Users();
        users.setAccount("134");
        users.setPassword("***");
        users.setUsername("嗨");
        users.setSex("女");
        users.setAge(18);
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd");
//        try {
//            java.util.Date date = simpleDateFormat.parse("2020-1-1");
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        users.setBirthday(new Date(20210915));
        users.setBirthday(Date.valueOf("2020-1-1"));
//        userService.getAdd(users);

    }

    //    更新；
    @Test
    public void updated() {
        UsersService userService = beanFactory.getBean("usersService", UsersService.class);
        Users users = new Users();
        users.setId(57);
        users.setAccount("134");
        users.setPassword("***");
        users.setUsername("嗨");
        users.setSex("女");
        users.setAge(18);
        userService.save(users);
    }

    //    删除；
    @Test
    public void delete() {
        UsersService userService = beanFactory.getBean("usersService", UsersService.class);
        userService.delId(58);
    }


    //    分页；展示
    @Test
    public void test() {
        OrderQueryVo vo = new OrderQueryVo();
        UsersService userService = beanFactory.getBean("usersService", UsersService.class);
        vo.setStart(1);
        vo.setSize(3);
        List<Users> byVo = userService.findOrderByVo(vo);
        for (Users users : byVo) {
            System.out.println(users);
        }
    }

}
