-- 创建数据库
create
database webapp1 charset utf8mb4;
-- 创建用户名、密码
create
user'webapp1'@'localhost'identified by'webapp1';
-- 授权
grant all
on webapp1.*to'webapp1'@'localhost';
-- 用用户名、密码登录
mysql
-uwebapp1 -pwebapp1;
-- 创建表
-- id,username,sex,age,birthday
create table user
(
    id       int not null primary key auto_increment,
    username varchar(12),
    sex      varchar(2),
    age      int,
    birthday date
);

-- 查看表结构
describe user;
-- 添加数据
insert into user
    (`username`, `sex`, `age`, `birthday`)
values ("小向", "男", 19, "2000-1-1");
-- 添加多条数据
insert into user
    (`username`, `sex`, `age`, `birthday`)
values ("小向", "男", 19, "2000-1-1"),
       ("小李", "男", 19, "2000-1-1"),
       ("小张", "男", 19, "2000-1-1"),
       ("小五", "男", 19, "2000-1-1"),
       ("小三", "男", 19, "2000-1-1"),
       ("小二", "男", 19, "2000-1-1"),
       ("小周", "男", 19, "2000-1-1");



create table users
(
    id       int not null primary key auto_increment,
    account  varchar(12),
    password varchar(12),
    username varchar(12),
    sex      varchar(2),
    age      int,
    birthday date
);

insert into users
    (`account`, `password`, `username`, `sex`, `age`, `birthday`)
values ("xiang", "123456", "小向", "男", 19, "2000-1-1"),
       ("dong", "123456", "小向", "男", 19, "2000-1-1"),
       ("liu", "123456", "小向", "男", 19, "2000-1-1"),
       ("wu", "123456", "小向", "男", 19, "2000-1-1");



create table Student
(
    s_id      int not null primary key auto_increment,
    s_number  int,
    s_name    varchar(10),
    s_age     int,
    s_tel     varchar(11),
    s_address varchar(20),
    s_score   double(6, 2
)
    );


insert into Student
    (`s_number`, `s_name`, `s_age`, `s_tel`, `s_address`, `s_score`)
values (19301041, "小一", 18, "1341917145", "四川成都", 89),
       (19301042, "小二", 19, "1341917144", "四川成都", 80),
       (19301043, "小三", 20, "1341917155", "四川成都", 62),
       (19301044, "小四", 21, "1341917456", "四川成都", 77),
       (19301045, "小五", 22, "1341911457", "四川成都", 98),
       (19301046, "小六", 23, "1341971458", "四川成都", 38),
       (19301047, "小七", 17, "1341171459", "四川成都", 79);


-- 创建数据库
create
database webapp3 charset utf8mb4;
-- 创建用户名、密码
create
user'webapp3'@'localhost'identified by'webapp3';
-- 授权
grant all
on webapp3.*to'webapp3'@'localhost';
-- 用用户名、密码登录
mysql
-uwebapp3 -pwebapp3;

create table user
(
    id       int not null primary key auto_increment,
    username varchar(12),
    password varchar(12)
);

insert into user
    (`username`, `password`)
values ("xiang", "123456");



create table T_User
(
    id   int not null primary key auto_increment,
    name varchar(12),
    age  int,
    sex  varchar(2)
) insert into T_User
    (`name`, `age`, `sex`)
values ("xiang", 19, "");