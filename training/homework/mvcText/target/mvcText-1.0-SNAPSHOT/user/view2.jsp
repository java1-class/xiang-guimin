<%--
  Created by IntelliJ IDEA.
  User: Xiang
  Date: 2021/9/10
  Time: 23:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>view2</title>
</head>
<body>
<h4>
    view2
</h4>
<form action="<%=request.getContextPath()%>/view2">
    <table border="1">

        <tr>
            <th scope="col">id</th>
            <th scope="col">username</th>
            <th scope="col">sex</th>
            <th scope="col">age</th>
            <th scope="col">birthday</th>
        </tr>

        <c:forEach items="${list}" var="model">
            <tr>
               <td>${model.id}</td>
               <td>${model.username}</td>
               <td>${model.sex}</td>
               <td>${model.age}</td>
               <td>${model.birthday}</td>

            </tr>
        </c:forEach>

<%--        <c:forEach items="${list}" var="model">--%>
<%--            <tr>--%>
<%--                <td>${model.byteValue()}</td>--%>
<%--            </tr>--%>
<%--        </c:forEach>--%>
        <tr></tr>
        <tr></tr>
        <tr></tr>
        <tr></tr>

        <%--<c:forEach items="${id}" var="model">--%>
        <%--    <tr>--%>
        <%--        --%>
        <%--        --%>
        <%--        <td><c:out value="${model.username}"/></td>--%>
        <%--        <td><c:out value="${model.sex}"/></td>--%>
        <%--        <td><c:out value="${model.age}"/></td>--%>
        <%--        <td><c:out value="${model.birthday}"/></td>--%>
        <%--&lt;%&ndash;    <td><fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${model.birthday}"/></td>&ndash;%&gt;--%>

        <%--</c:forEach>--%>

    </table>
</form>
</body>
</html>
