<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Xiang
  Date: 2021/9/13
  Time: 22:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<%@ taglib prefix="hwadee" uri="http://hwadee.cn/common/"%>--%>
<%@ taglib prefix="hwadee" uri="http://hwadee.cn/common/" %>
<%----%>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://cdn.staticfile.org/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdn.staticfile.org/popper.js/1.15.0/umd/popper.min.js"></script>
    <script src="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>


</head>
<script type="text/javascript">
    function deleteId(id) {
        let url = "http://localhost:8001/mvcText/users/getdel/" + id
        // alert(url)
        // alert("   function add() ")

        $.ajax({
            type: "POST",
            // dataType: "json",
            contentType: "application/json;charset=UTF-8",
            url: url,
            data: JSON.stringify(
                {
                    // "id": $('#id').val()
                    id: id
                }
            ),
            success: function (data) {
                // console.log(data.id);
                // window.location.href = "http://localhost:8001/mvcText/users/all.jsp";
                window.location.href = "http://localhost:8001/mvcText/users/list";
                // window.location.href = "http://localhost:8001/mvcText/query";
                // window.location.href = "query";
                // alert(data.id)
                console.log(data)
                alert("success")
            },
            error: function (error) {
                console.log(error)
                alert("fail");
            }
        })
    }
</script>

<%--<nav>--%>
<%--${result}--%>
<%--all--%>
<%--${all}--%>
<%--<div>--%>
<%--    <a href="<%=request.getContextPath()%>/logout" class="btn btn-warning">注销</a>--%>
<%--</div>--%>
<%--<h3 style="align-content: center">--%>
<%--    管理系统--%>
<%--</h3>--%>
<%--<div class="container">--%>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <%--    <a class="navbar-brand" href="#">关于</a>--%>
    <button type="button" class="btn btn-secondary">关于</button>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <%--                <a class="nav-link" href="#">主页 <span class="sr-only">(current)</span></a>--%>
                <a href="<%=request.getContextPath()%>/users/list" type="button" class="btn btn-info">主页</a>
            </li>
            <li class="nav-item">
                <%--                <a class="nav-link" href="#">新增</a>--%>
                <a href="<%=request.getContextPath()%>/users/add.jsp" class="btn btn-primary">新增</a>
            </li>
            <%--            <li class="nav-item dropdown">--%>
            <%--                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"--%>
            <%--                   aria-haspopup="true" aria-expanded="false">--%>
            <%--                    Dropdown--%>
            <%--                </a>--%>
            <%--                <div class="dropdown-menu" aria-labelledby="navbarDropdown">--%>
            <%--                    <a class="dropdown-item" href="#">Action</a>--%>
            <%--                    <a class="dropdown-item" href="#">Another action</a>--%>
            <%--                    <div class="dropdown-divider"></div>--%>
            <%--                    <a class="dropdown-item" href="#">Something else here</a>--%>
            <%--                </div>--%>
            <%--            </li>--%>
            <%--            <li class="nav-item">--%>
            <%--                <a class="nav-link disabled" href="#">Disabled</a>--%>
            <%--            </li>--%>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">搜索</button>
            <a href="<%=request.getContextPath()%>/users/logout" class="btn btn-outline-success my-2 my-sm-0"
               type="submit">注销</a>
        </form>
    </div>
</nav>


<%--<form>--%>
<table class="table table-striped">
    <tr>
        <th>
            序号
        </th>
        <th>
            账号
        </th>
        <th>
            密码
        </th>
        <th>
            姓名
        </th>
        <th>
            性别
        </th>
        <th>
            年龄
        </th>
        <th>
            生日
        </th>
        <th colspan="3">
            操作
        </th>
    </tr>
    <%--        //(`account`, `password`, `username`, `sex`, `age`, `birthday`)--%>
    <c:forEach items="${usersList}" var="model">
        <tr>
            <td>
                    ${model.id}
            </td>
            <td>
                    ${model.account}
            </td>
            <td>
                    ${model.password}
            </td>
            <td>
                    ${model.username}
            </td>
            <td>
                    ${model.sex}
            </td>
            <td>
                    ${model.age}
            </td>
            <td>
                    ${model.birthday}
            </td>
            <td><a href="<%=request.getContextPath()%>/users/getid/${model.id}" class="btn btn-success">修改</a></td>
                <%--                <td><a href="<%=request.getContextPath()%>/getdel/${model.id}">删除</a></td>--%>
            <td><a href="#" class="btn btn-danger" onclick="deleteId('${model.id}')">删除</a></td>
                <%--                <td><a href="<%=request.getContextPath()%>/users/add.jsp" class="btn btn-primary">新增</a></td>--%>
        </tr>
    </c:forEach>


</table>

<%--</form>--%>
<%--<div class="col-md-12 text-center" id="aaa">--%>
<%--<div class="btn btn-success>--%>
<%--<nav aria-label="Page navigation example">--%>
<%--    <ul class="pagination">--%>
        <hwadee:page url="${pageContext.request.contextPath }/users/list"></hwadee:page>
<%--    </ul>--%>
<%--</nav>--%>

    <%--        <hwadee:page url="${pageContext.request.contextPath }/users/list.action"></hwadee:page>--%>

<%--</div>--%>
<%--</div>--%>
<%--<script>--%>
<%--    window.onload = function (){--%>
<%--        document.getElementById("nav").classList.add("Page navigation example");--%>
<%--    }--%>
<%--</script>--%>
<%--<hwadee:page url="${pageContext.request.contextPath }/order/list.action" />--%>
</body>
</html>
