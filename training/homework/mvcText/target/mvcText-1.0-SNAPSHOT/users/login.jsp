<%--
  Created by IntelliJ IDEA.
  User: Xiang
  Date: 2021/9/13
  Time: 21:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resource/css/login.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body class="text-center">
<%--<h3 align="center">--%>
<%--    login 登录页--%>
<%--</h3>--%>

<%--<form class="form-signin" action="<%=request.getContextPath()%>/login" method="post">--%>
<%--    <table class="tab-close-button">--%>
<%--        <tr>--%>
<%--            <td><input type="text" name="account"></td>--%>
<%--        </tr>--%>
<%--        <tr>--%>
<%--            <td><input type="password" name="password" class="form-control"></td>--%>
<%--        </tr>--%>
<%--        <tr>--%>
<%--            <td><input type="submit" class="btn btn-lg btn-primary btn-block"></td>--%>
<%--        </tr>--%>

<%--    </table>--%>

<%--</form>--%>
<form class="form-signin" action="<%=request.getContextPath()%>/users/list">
    <img class="mb-4" src="<%=request.getContextPath()%>/resource/image/images.png" alt="" width="100" height="100">
    <h1 class="h3 mb-3 font-weight-normal">请登录</h1>
    <label for="account" class="sr-only">账号</label>
    <input type="text" name="account" id="account" class="form-control" placeholder="账号" required="" autofocus="">
    <label for="inputPassword" class="sr-only">密码</label>
    <input type="password" name="password" id="inputPassword" class="form-control" placeholder="密码" required="">
    <div class="checkbox mb-3">
        <label>
            <input type="checkbox" value="remember-me"> 记住我
        </label>
    </div>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    <p class="mt-5 mb-3 text-muted">© 2021</p>
</form>


</body>
</html>
