package com.xiang.lesson01;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet(urlPatterns = "/index")
public class DBMaven extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Connection connection = DBTest.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select  * from student");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                System.out.println(
                        resultSet.getInt("s_id") + "\t" +
                                resultSet.getString("s_name") + "\t" +
                                resultSet.getString("s_sex") + "\t" +
                                resultSet.getDate("s_birthday") + "\t" +
                                resultSet.getInt("s_professional"));
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        req.getRequestDispatcher("/index.jsp").forward(req,resp);
    }

}
