package com.xiang.lesson01.controller;

import com.xiang.lesson01.pojo.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.text.SimpleDateFormat;
import java.util.Date;

@Controller

public class LoginController {

    @RequestMapping(value = "/login")
    public String register() {
        return "user2/login";
    }

//@RequestMapping(value = "/login2", method = RequestMethod.POST)
//    public String getLogin(@RequestParam("user")String user,
//                           @RequestParam("pwd")String pwd){
//    if (user.equals("qqq") && pwd.equals("qqq")) {
//        return "/user2/success";
//    } else {
//        return "/user2/fail";
//    }
//}


//    @RequestMapping(value = "/login2",method = RequestMethod.POST)
//    public  String getLogin3(User user, HttpServletResponse response, HttpServletRequest request) throws ServletException, IOException {
//        System.out.println(user);
//        if (user != null){
//            request.getRequestDispatcher("/user2/success.jsp").forward(request,response);
//        }else {
//            request.getRequestDispatcher("/user2/fail.jsp").forward(request,response);
//        }
//        return  null;
//    }


// 绑定响应体
//    @ResponseBody
//    @RequestMapping(value = "/login2")
//    public User getLogin2(User user){
//        user = new User();
//        user.setUser("向向向");
//        user.setPwd("123456");
//        return  user;
//    }


//    @RequestMapping(value = "/login2")
//    public String getLogin2(User user){
//        System.out.println(user);
//        return "user2/success";
//    }


//    绑定请求体
//    @RequestMapping(value = "/login2")
//    public  String getLogin2(@RequestBody String requestBody){
//        System.out.println(requestBody);
//        return "user2/success";
//    }


//    Model
//    @RequestMapping(value = "/login2")
//    public String getLogin2(Model model) {
//        User user = new User();
//        user.setUser("向向向");
//        user.setPwd("*****");
//        Date date = new Date();
//        System.out.println(date);
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ");
//        String format = sdf.format(date);
//        System.out.println(sdf.format(date));
//        user.setBirthday(format);
////format 封装；
//        user.setBirthday(format);
//        model.addAttribute("user", user);
//        return "user2/succ";
//    }

/*
//    外国日期转换成中国日期
//        import datetime
//            s = 'December 8, 2016'
//    ss=datetime.datetime.strptime(s.strip(),'%B %d, %Y')
//
//    print(ss)
 */

//    ModelAndView
//    @RequestMapping(value = "/login2")
//    public  ModelAndView getLogin2(){
//        User user = new User();
//        user.setUser("xiang");
//        user.setPwd("***********");
//        user.setBirthday("2020-9-9");
//        ModelAndView view = new ModelAndView();
//        view.setViewName("user2/succ");
//        view.addObject("user",user);
//        return view;
//    }

@RequestMapping(value = "/login2")
    public  String getLogin2(ModelMap modelMap){
    User user = new User();
    user.setUser("**********");
    user.setPwd("***********");
    user.setBirthday("2021-9-9");
    modelMap.addAttribute("user",user);
    return "user2/succ";
}

}
