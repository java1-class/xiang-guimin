package com.xiang.lesson01.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/user")
public class UserController {

    @RequestMapping("/login")
    public String register() {
        return "user/login";
    }


    @RequestMapping(value = "/dologin/{userId}", method = RequestMethod.POST)
    public String login(@PathVariable("userId") int userId) {
//       PathVariable 前台 传入后台
        System.out.println("userid====>" + userId);
        return "/user/success";
    }

    @RequestMapping(value = "/logintwo", method = RequestMethod.POST)
    public String login2(@RequestParam("user") String user, @RequestParam("pwd") String pwd) {
        if (user.equals("xiang") && pwd.equals("123")) {
            return "/user/success";
        } else {
            return "/user/fail";
        }
    }

//    @RequestMapping(value = "/login",method = RequestMethod.POST)
//    public  ModelAndView createUser(User user){
//
//    }
}
