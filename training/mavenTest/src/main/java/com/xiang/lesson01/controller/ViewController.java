package com.xiang.lesson01.controller;

import com.xiang.lesson01.pojo.User;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/13 9:49
 */

/*
视图解析器
 */
public class ViewController {
    @RequestMapping("/view")
    public  String toView(User user, HttpServletRequest request){
        user = new User();
        user.setUser("xiang");
        user.setPwd("***");
        return  "view/user";

    }
}
