package com.xiang;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/14 20:41
 */
public class DBTest {
    private static String URL = "jdbc:mysql://localhost:3307/webapp3";
    private static String DriverClass = "com.mysql.cj.jdbc.Driver";
    private static String UserName = "webapp3";
    private static String Password = "webapp3";
    private static Connection connection = null;

    public static Connection getConnection() {
        try {
            Class.forName(DriverClass);
            connection = DriverManager.getConnection(URL, UserName, Password);

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public static void closeResource() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }


    public static void main(String[] args) {
        DBTest.getConnection();
        if (connection != null) {
            System.out.println("数据库连接成功");
        } else {
            System.out.println("数据库连接失败");
        }
        closeResource();
    }

}


