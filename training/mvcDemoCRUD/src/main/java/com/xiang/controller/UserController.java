package com.xiang.controller;

import com.xiang.pojo.User;
import com.xiang.service.UserService;
import com.xiang.service.UserServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/14 21:14
 */
@Controller
public class UserController {
    UserService userService = new UserServiceImpl();

    @RequestMapping(value = "/list",method = RequestMethod.POST)
    public String getLogin(User user, Model model) {
        System.out.println("验证-----------");
        boolean login = userService.getLogin(user);
        if (login) {
            List<User> list = userService.getList();
            model.addAttribute("list",list);
            System.out.println("验证ok");
            return "user/success";
        } else {
            System.out.println("验证失败");
            return "user/fail";
        }
    }


}
