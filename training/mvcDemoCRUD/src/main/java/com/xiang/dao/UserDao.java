package com.xiang.dao;

import com.xiang.pojo.User;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/14 21:01
 */
public interface UserDao {
    public boolean getLogin(User user);

    public List<User> getList();
}
