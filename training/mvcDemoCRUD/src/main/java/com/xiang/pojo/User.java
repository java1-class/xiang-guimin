package com.xiang.pojo;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/14 20:56
 */

/**
 * id       int not null primary key auto_increment,
 * username varchar(12),
 * password varchar(12)
 */
public class User {
    private int id;
    private String username;
    private String password;

    @Override
    public String toString() {
        return "user{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
