package com.xiang.service;

import com.xiang.pojo.User;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/14 21:12
 */
public interface UserService {
    public boolean getLogin(User user);

    public List<User> getList();
}
