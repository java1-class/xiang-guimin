package com.xiang.service;

import com.xiang.dao.UserDao;
import com.xiang.dao.UserDaoImpl;
import com.xiang.pojo.User;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/14 21:11
 */
public class UserServiceImpl implements UserService {
    UserDao userDao = new UserDaoImpl();

    @Override
    public boolean getLogin(User user) {
        return userDao.getLogin(user);
    }

    @Override
    public List<User> getList() {
        return userDao.getList();
    }
}
