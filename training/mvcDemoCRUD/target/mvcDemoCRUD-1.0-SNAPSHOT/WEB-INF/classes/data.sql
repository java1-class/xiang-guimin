-- 创建数据库
create
    database webapp3 charset utf8mb4;
-- 创建用户名、密码
create
    user 'webapp3'@'localhost' identified by 'webapp3';
-- 授权
grant all
    on webapp3.*to 'webapp3'@'localhost';
-- 用用户名、密码登录
mysql
-uwebapp3 -pwebapp3;

create table user
(
    id       int not null primary key auto_increment,
    username varchar(12),
    password varchar(12)
);

insert into user
    (`username`, `password`)
values ("xiang", "123456");