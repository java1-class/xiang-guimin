package com.xiang;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/23 18:56
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 引导类
 * spring Boot 项目的入口
 */

@SpringBootApplication
public class HelloApplication {
    public static void main(String[] args) {

        SpringApplication.run(HelloApplication.class, args);
    }
}
