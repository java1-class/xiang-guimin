package com.xiang.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/23 18:53
 */
@RestController

public class HomeController {
    @RequestMapping("/hello")
    public  String hello(){
        return "Hello spring Boot";

    }
}
