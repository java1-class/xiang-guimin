package com.xiang.controller;

import com.xiang.pojo.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/23 19:17
 */
@RestController
public class HomeController {

    /**
     * 使用  @Value 注解获取 配置文件
     */
    @Value("${name}")
    private String name;
    @Value("${person.name}")
    private String name2;
    @Value("${person.age}")
    private Integer age;

    @Value("${address[0]},${address[1]}")
    private String address;

    @Value("${msg1}")
    private String msg1;
    @Value("${msg2}")
    private String msg2;


    /**
     * 注入一个对象 Environment 获取 配置文件
     */
    @Autowired
    private Environment env;

    /**
     * 注入 person
     * 使用  ConfigurationProperties 方式注入 获取 配置文件
     */
    @Autowired
    private Person person;

    @RequestMapping("/name")
    public List<Object> name() {
        System.out.println(name);
        System.out.println(name2);
        System.out.println(age);
        System.out.println(address);
        System.out.println(msg1);
        System.out.println(msg2);
        System.out.println("/***************************/");

        /**
         * 注入一个对象 Environment
         */
        System.out.println(env.getProperty("person.name"));
        System.out.println(env.getProperty("address[0]"));
        System.out.println(env.getProperty("name"));
        System.out.println(env.getProperty("person.age"));
        System.out.println(env.getProperty("msg1"));

        System.out.println("/***************************/");

        /**
         * 注入 person
         * 使用  ConfigurationProperties 方式注入
         */
        System.out.println(person);

        String[] address = person.getAddress();
        for (String s : address
        ) {
            System.out.println(s);
        }


        List<Object> list = new LinkedList<>();
        list.add(name);
        list.add(name2);
        list.add(age);

        list.add(this.address);

        list.add(msg1);
        list.add(msg2);
        list.add("/*************/");

        /**
         * 注入一个对象 Environment
         */
        list.add(env.getProperty("person.name"));
        list.add(env.getProperty("address[0]"));
        list.add(env.getProperty("name"));
        list.add(env.getProperty("person.age"));
        list.add(env.getProperty("msg1"));

        list.add("/*************/");

        /**
         * 注入 person
         * 使用  ConfigurationProperties 方式注入
         */
        list.add(person);
        list.add(address);


        return list;

    }

    /**
     * xiang
     * xiang
     * 20
     * beijing,shanghai
     * hello \n world
     * hello
     * world
     * <p>
     * xiang
     * beijing
     * xiang
     * 20
     * hello \n world
     * <p>
     * Person(name=xiang, age=20, gender=男, address=[beijing, shanghai])
     * beijing
     * shanghai
     */

    @RequestMapping("/hello")
    public String hello() {
        return "Hello spring Boot!!!";
    }
}
