package com.xiang.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/23 23:54
 */
//Person 类被spring所识别
@Component
// ConfigurationProperties 方式 获取 配置文件
@ConfigurationProperties(prefix = "person")

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class Person {
    private String name;
    private Integer age;
    private String gender;
    private  String[] address;


}
