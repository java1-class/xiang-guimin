package com.xiang.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/25 13:48
 */
@RestController
public class HelloController {
    @RequestMapping("/hello")
    public String hello() {
        return "HelloController";
    }
}
