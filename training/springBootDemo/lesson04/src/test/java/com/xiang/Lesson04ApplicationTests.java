package com.xiang;

import com.xiang.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
class Lesson04ApplicationTests {
    @Autowired
    private UserService userService;

    @Test
    public void addTest() {
        userService.add();
    }

}
