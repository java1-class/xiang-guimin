package com.xiang.servicre;

import com.xiang.Lesson04Application;
import com.xiang.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/25 14:14
 */

/**
 * UserService 测试类；
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Lesson04Application.class)

public class UserServiceTest {
    @Autowired
    private UserService userService;

    @Test
    public  void  AddTest(){
        userService.add(); //add...........
    }
}
