package com.xiang;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

@SpringBootTest
class Lesson05ApplicationTests {
    /**
     * 注入 RedisTemplate
     */
    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    void setTest() {
        //调用模板对象操作
        /**
         * 1，存数据
         */
        redisTemplate.boundValueOps("name").set("xiang");
    }
    @Test
    void getTest() {
        /**
         * 1,获取数据
         */
        Object name = redisTemplate.boundValueOps("name").get();
        System.out.println(name); //xiang
    }
    /**
     * 要先启动 redis
     */

}
