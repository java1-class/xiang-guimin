package com.xiang.mapper;

import com.xiang.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/25 15:03
 */
@Mapper
@Repository
public interface UserMapper {
    @Select("select * from user")
    public List<User> findAll();
}
