package com.xiang;

import com.xiang.mapper.UserMapper;
import com.xiang.mapper.UserXmlMapper;
import com.xiang.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class Lesson06ApplicationTests {

    @Autowired
    private UserMapper userMapper;

    @Autowired
     private UserXmlMapper userXmlMapper;


    /**
     * 利用注解  方式；
     */
    @Test
    void findAll() {

        List<User> list = userMapper.findAll();
        for (User user : list) {
            System.out.println(user);
        }
    }


    /**
     * 利用 xml 方式；
     */
    @Test
    void findAllList() {

        List<User> list = userXmlMapper.findAllList();
        for (User user : list) {
            System.out.println(user);
        }
    }




}
