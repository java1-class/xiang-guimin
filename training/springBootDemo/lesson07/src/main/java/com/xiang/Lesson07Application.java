package com.xiang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Lesson07Application {

    public static void main(String[] args) {
        //        启动spring Boot  的应用，返回spring IOC 容器
        ConfigurableApplicationContext context = SpringApplication.run(Lesson07Application.class, args);
        // 获取 bean  ,redisTemplate
        Object redisTemplate = context.getBean("redisTemplate");
        System.out.println(redisTemplate); //org.springframework.data.redis.core.RedisTemplate@1e287667

        // 获取 user
        Object user = context.getBean("user");
        System.out.println(user); //com.xiang.pojo.User@54336c81 // 获取 user

        // 获取 user2
        Object user2 = context.getBean("user2");
        System.out.println(user2); // com.xiang.pojo.User@1af1347d

    }


}
