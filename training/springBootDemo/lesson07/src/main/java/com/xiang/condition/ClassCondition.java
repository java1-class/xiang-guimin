package com.xiang.condition;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.type.AnnotatedTypeMetadata;
import org.springframework.util.MultiValueMap;
import redis.clients.jedis.Jedis;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/25 15:54
 */
public class ClassCondition implements Condition {
    /***
     *
     * @param context  上下文对象
     * @param metadata 注解原对象；获取注解定义的属性值
     * @return
     */
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        /**
         * 需求 1 ；
         * 导入 Jedis 坐标后 创建bean
         *<!--        导入jedis 坐标-->
         *         <dependency>
         *             <groupId>redis.clients</groupId>
         *             <artifactId>jedis</artifactId>
         *         </dependency>
         *
         *         思路
         *         判断  redis.clients.jedis.Jedis.class 文件 是否 存在；
         *
         */


//        Jedis

        boolean flag = true;
        try {
            Class<?> name = Class.forName("redis.clients.jedis.Jedis");
            System.out.println(name);
        } catch (ClassNotFoundException e) {
            flag = false;
            e.printStackTrace();

        }
        return flag;



        /**
         * 需求 2 ；
         * 导入通过注解属性值value指定坐标后创建Bean
         *
         * 获取注解属性值 ；value
         */
        /* 有bug
        MultiValueMap<String, Object> map = metadata.getAllAnnotationAttributes(ConditionOnClass.class.getName());
        System.out.println(map); //{value=[[Ljava.lang.String;@4e07b95f]}
        List<Object> list = map.get("value");

        boolean flag = true;
        try {
//            遍历一个数组

            for ( Object  s : list) {

                Class<?> name = Class.forName(s);
                System.out.println(name);
            }


        } catch (ClassNotFoundException e) {
            flag = false;
            e.printStackTrace();

        }
        return flag;
        */
    }
}
