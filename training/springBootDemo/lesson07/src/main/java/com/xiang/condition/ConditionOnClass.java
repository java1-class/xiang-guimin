package com.xiang.condition;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/25 16:15
 */

import org.springframework.context.annotation.Conditional;

import java.lang.annotation.*;

/**
 * 动态的；
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented

@Conditional(ClassCondition.class) //条件接口；
public @interface ConditionOnClass {
    String[] value();

}
