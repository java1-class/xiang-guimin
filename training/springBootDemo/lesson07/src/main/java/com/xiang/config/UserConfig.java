package com.xiang.config;

import com.xiang.condition.ClassCondition;
import com.xiang.condition.ConditionOnClass;
import com.xiang.pojo.User;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/25 15:49
 */
@Configuration
public class UserConfig {
    // 创建一个bean
    @Bean
//    @Conditional(ClassCondition.class) //条件接口；
    /* 这个user 对应 的 bean 将不会被  spring 所创建出来
    * 因 这个类 ClassCondition  里边  return false; -------》 报错；  No bean named 'user' available
    *这个类 ClassCondition  里边   return true;   则会被 创建；
    * 根据条件 而来；
     *  */

//    动态的
    @ConditionOnClass("redis.clients.jedis.Jedis")
    public User user(){
        return  new User();
    }

    @Bean
    @ConditionalOnProperty(name = "xiang",havingValue = "xiang")
    public User user2(){
        return  new User();
    }




}
