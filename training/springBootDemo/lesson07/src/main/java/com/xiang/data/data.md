## Condition (条件) 判断

![img.png](img.png)



> SpringBoot是如何知道要创建哪个Bean的?比如SpringBoot是如何知道要创建Redis Template的?
>

## Condition (条件) 小结

![img_1.png](img_1.png)

##  切换内置web服务器 

> SpringBoot的web环境中默认使用tpmcat作为内置服务器，
> 其实SpringBoot提供 了4中内置服务器供我们选择，我们可
> 以很方便的进行切换。
```properties
<!--        引入 web 依赖-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
```
 ![img_2.png](img_2.png)
 

![img_4.png](img_4.png)
![img_3.png](img_3.png)