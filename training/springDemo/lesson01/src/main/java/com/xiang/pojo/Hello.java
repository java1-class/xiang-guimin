package com.xiang.pojo;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/22 13:20
 */
public class Hello {
    private String talk;

    @Override
    public String toString() {
        return "Hello{" +
                "talk='" + talk + '\'' +
                '}';
    }

    public String getTalk() {
        return talk;
    }

    public void setTalk(String talk) {
        this.talk = talk;
    }
}
