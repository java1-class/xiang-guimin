package com.xiang.pojo;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/22 14:49
 */
public class User {
    private String user;

    public User(String user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "User{" +
                "user='" + user + '\'' +
                '}';
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
