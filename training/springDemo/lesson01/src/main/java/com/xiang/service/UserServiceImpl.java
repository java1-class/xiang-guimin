package com.xiang.service;

import com.xiang.dao.UserDao;
import com.xiang.dao.impl.UserDaoImpl1;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/22 10:57
 */
public class UserServiceImpl implements UserService {
    //    private UserDaoImpl1 userDaoImpl1 = new UserDaoImpl1();
    private UserDao userDao;

    /**
     * set 注入
     *
     * @param userDao
     */
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    /**
     * 构造方法
     * 构造器 注入
     * @param userDao
     * 注意，构造方法与set注入不能同时连用，否则会报错；  package com.xiang.service; UserServiceImplTest Test05 测试类
     * Error creating bean with name 'userServiceImpl' defined in class path resource [applicationContext.xml]:
     */
//    public UserServiceImpl(UserDao userDao) {
//        this.userDao = userDao;
//    }

    /**
     * 调方法
     *
     * @return
     */
    @Override
    public String show() {
        return userDao.show();
    }
}
