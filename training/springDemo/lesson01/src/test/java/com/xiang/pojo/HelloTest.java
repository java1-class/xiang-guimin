package com.xiang.pojo;

import com.xiang.service.UserService;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/22 13:23
 */
public class HelloTest {
    @Test
    public void Test01() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Hello hello = (Hello) context.getBean("hello");
        System.out.println(hello.toString());
        System.out.println(hello.getTalk());
        System.out.println("/**************************/");
        hello.setTalk("你好，这儿是……");
        System.out.println(hello.getTalk());
    }

}
