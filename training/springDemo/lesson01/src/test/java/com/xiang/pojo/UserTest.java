package com.xiang.pojo;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;


import static org.junit.jupiter.api.Assertions.*;

public class UserTest {
    /**
     * 有参构造赋值：1.下标赋值
     */
    @Test
    public void Test01() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        System.out.println(context.getBean("user1"));
    }

    /**
     * 有参构造赋值：2.数据类型赋值（不建议使用）
     */
    @Test
    public void Test02() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        System.out.println(context.getBean("user2"));
    }

    /**
     * 有参构造赋值：3.参数名赋值（常用）
     */
    @Test
    public void Test03() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        System.out.println(context.getBean("user3"));
    }

    /**
     * 当读取配置文件，创建容器时就会创建容器中的所有bean
     * 无论获取几次相同的bean都是同一个，创建bena的方式是单例的
     */
    @Test
    public void Test04() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        User user3 = (User) context.getBean("user3");
        User user4 = (User) context.getBean("user3");
        System.out.println(user3 == user4);
        System.out.println(user3.equals(user4));
    }

    /**
     * 设置别名：相当于多了一个名字，两个名字都可以用
     * 别名
     */
    @Test
    public void Test05() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        User s4 = (User) context.getBean("s4");
        User user4 = (User) context.getBean("user4");
        System.out.println(s4 == user4);
        System.out.println(s4.equals(user4));
    }

    /**
     * name:在标签内设置name属性也可以设置别名，并且可以设置多个别名；
     * 多个别名可以用“逗号”，“空格”，“分号”分割
     * 别名
     */
    @Test
    public void Test06() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        User user5 = (User) context.getBean("user5");
        User u1 = (User) context.getBean("u1");
        User u2 = (User) context.getBean("u2");
        User u3 = (User) context.getBean("u3");
        User u4 = (User) context.getBean("u5");
        System.out.println(u1);
        System.out.println(u2);
        System.out.println(u3);
        System.out.println(u4);
        System.out.println(user5);
    }

    /**
     * 1. import 它可以将多个配置文件导入合并为一个
     * 2. 假设，现在项目中有多个人进行开发，这三个人负责不同的类开发，不同的类需要注册在不同的bean中，我们可以用import将多个bean.xml合并为一个总的
     * 注意：bean1.xml和bean2.xml要有所区别，否则有可能会报错
     */
    @Test
    public  void  Test07(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        User user6 = (User) context.getBean("user6");
        User u6 = (User) context.getBean("u6");
        System.out.println(u6);
        System.out.println(user6);
    }

}