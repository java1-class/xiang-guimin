package com.xiang.service;

import com.xiang.dao.impl.UserDaoImpl1;
import com.xiang.dao.impl.UserDaoImpl2;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/22 11:00
 */
public class UserServiceImplTest {

    /**
     * 普通方式查询
     */
//    @Test
//    public void Test01() {
//        UserServiceImpl service = new UserServiceImpl();
//        System.out.println("普通方式查询");
//        System.out.println(service.show());
//    }

    /**
     * set 注入
     */
//    @Test
//    public void Test02() {
//        UserServiceImpl service = new UserServiceImpl();
//        service.setUserDao(new UserDaoImpl2());
//        System.out.println("set 注入");
//        System.out.println(service);
//        System.out.println(service.show());
//    }

    /**
     * set 注入
     */
//    @Test
//    public void Test03() {
//        UserServiceImpl service = new UserServiceImpl();
//        service.setUserDao(new UserDaoImpl1());
//        System.out.println("set 注入");
//        System.out.println(service);
//        System.out.println(service.show());
//    }

    /**
     * 构造器 注入
     */
//    @Test
//    public void Test04() {
//        UserServiceImpl service = new UserServiceImpl(new UserDaoImpl2());
//        System.out.println("构造器 注入");
//        System.out.println(service);
//        System.out.println(service.show());
//    }


    @Test
    public  void  Test05(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserService userServiceImpl = (UserService) context.getBean("userServiceImpl");
        System.out.println(userServiceImpl.show());
    }



}
