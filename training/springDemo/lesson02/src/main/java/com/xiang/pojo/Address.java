package com.xiang.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/22 16:40
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class Address {
    private String address;

}
