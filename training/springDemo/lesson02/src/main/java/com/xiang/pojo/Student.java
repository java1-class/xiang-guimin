package com.xiang.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/22 16:57
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class Student {
    private String name;
    private Address address;
    private String[] books;
    private List<String> hobbys;
    private Map<String, String> card; //学籍信息
    private Set<String> games;
    private String wife; //妻子
    private Properties info; //信息

}
