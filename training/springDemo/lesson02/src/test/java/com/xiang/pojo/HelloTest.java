package com.xiang.pojo;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/22 13:11
 */
public class HelloTest {
    @Test
    public  void  Test01(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Hello bean = (Hello) context.getBean("hello");
        System.out.println(bean.toString());
    }

}
