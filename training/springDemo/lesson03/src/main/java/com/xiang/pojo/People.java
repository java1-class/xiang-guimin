package com.xiang.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/23 10:02
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class People {
    private  String name;
    @Autowired
    private  Dog dog;
    @Autowired
    private  Cat cat;

    /**
     * 1. @Autowired直接在属性上使用即可，也可以直接在set方法上使用
     * 2. 使用@Autowired可以不再编写set方法了，前提是你这个自动装配的的属性在IOC（Spring）容器中存在，且配置文件中的bean标签的id值要与实体类的属性名一致
     */
}
