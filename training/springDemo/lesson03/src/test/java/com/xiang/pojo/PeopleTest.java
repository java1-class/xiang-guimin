package com.xiang.pojo;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/23 10:05
 */
public class PeopleTest {
    /**
     * 在xml中显式配置(这是我们刚才一直使用的方式)
     */
    @Test
    public void Test01() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        People people = context.getBean("people", People.class);
        System.out.println(people.getName());
        people.getCat().eat();
        people.getDog().eat();
    }

    /**
     * ByName和byType自动装配
     */
    /**
     * byName:会自动在容器的上下文中查找和自己对象set方法后面的属性名对应的beanid
     */
    @Test
    public void Test02() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        People people = context.getBean("people1", People.class);
        System.out.println(people.getName());
        people.getCat().eat();
        people.getDog().eat();
    }

    /**
     * byType:会自动在容器的上下文中查找和自己对象属性类型相同的bean
     */
    @Test
    public void Test03() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        People people = context.getBean("people2", People.class);
        System.out.println(people.getName());
        people.getCat().eat();
        people.getDog().eat();
    }

    /**
     * 1. @Autowired直接在属性上使用即可，也可以直接在set方法上使用
     * 2. 使用@Autowired可以不再编写set方法了，前提是你这个自动装配的的属性在IOC（Spring）容器中存在，且配置文件中的bean标签的id值要与实体类的属性名一致
     */
    @Test
    public void Test04() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        People people = context.getBean("p1", People.class);
        System.out.println(people.getName());
        people.getCat().eat();
        people.getDog().eat();
    }


}
