package com.xiang.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/23 10:37
 */
/**
 * Component:组件
 * 该注解相当于配置文件中的：<bean id="user" class="com.bao.pojo.User"/>
 * 说明该类被Spring容器管理了，实体类添加了该注解才能被扫描到
 */
@Component

/**
 * 作用域
 */
@Scope("prototype")/*值可以设置相应的作用域*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class User {
    private String user="向向";
///*简单的属性使用注解更加方便*/
    @Value("***********")
    private String password;

    /**
     * 衍生的注解
     *  @Component有几个衍生的注解，我们在web开发中，会按照mvc三层架构分层
     *
     * 1. mapper [ @Repository]
     * 2. service[@Service]
     * 3. controller [@Controller]
     * 4. 注：这四个注解功能是一样的，都是代表将某个类注册到Spring容器中
     */


    /**
     *  小结
     *
     * xml和注解
     *
     * 1. xml更加万能，适用于任何场合，维护简单方便
     * 2. 注解维护相对复杂
     *
     *  xml与注解的最佳实践
     *
     * 1. xml用来管理bean
     * 2. 注解只负责完成属性的注入
     * 3. 我们在使用过程中，只需要注意一个问题：必须让注解生效，所以一定要开启注解支持
     */
}
