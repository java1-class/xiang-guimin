package com.xiang.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/23 11:32
 */
/*
导入其他配置类
 */

@Configuration/*该注解说明该类是一个配置类*/
@Import(UserConfig.class)/*可以使用该注解导入其他的配置类*/
@ComponentScan("com.xiang.pojo")/*可以使用该注解设置扫描的包*/
public class AllConfig {

}
