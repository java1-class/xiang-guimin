package com.xiang.config;

import com.xiang.pojo.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/23 11:26
 */

/**
 * 该注解说明该类是一个配置类
 */
@Configuration
public class UserConfig {
    /**
     * 当有多个bean时，都需要在这里进行注册
     * 使用该注解相当于在配置文件中写了一个<bean>标签
     * 方法名相当于bean标签中的id属性
     * 方法的返回值相当于bean标签中的class属性
     */

    @Bean
    @Scope("prototype")
    public User getUser() {
        /*
         *返回要注入到bean的对象
         */
        return new User();

    }

}
