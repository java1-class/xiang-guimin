package com.xiang.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/23 11:08
 */
@Component

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class User {
    @Value("小向")
    private  String name;
    @Value("****")
    private  String password;
    @Value("男")
    private  String sex;
    @Value("19")
    private  Integer age;

}
