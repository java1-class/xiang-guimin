## 使用java的方式配置bean
### 思路

1. 完全不使用Spring的xml 配置，全权交给Java来做！
2. JavaConfig 是Spring的一个子项目
3. JavaConfig 原来是 Spring 的一个子项目，它通过 Java 类的方式提供 Bean 的定义信息，在 Spring4 的版本， JavaConfig 已正式成为 Spring4 的核心功能 。
 