package com.xiang.config;

import com.xiang.pojo.User;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/23 11:30
 */
public class UserConfigTest {
    @Test
    public  void  Test01(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(UserConfig.class);
        User getUser = (User) context.getBean("getUser");
        System.out.println(getUser);
    }

    /**
     * 导入其他配置类
     */
    @Test
    public  void  Test02(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AllConfig.class);
        User getUser = (User) context.getBean("getUser");
        User user =  context.getBean("user",User.class);
        System.out.println(getUser);
        System.out.println(user);
    }
}
