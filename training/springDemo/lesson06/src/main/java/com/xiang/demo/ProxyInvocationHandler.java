package com.xiang.demo;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/23 16:19
 */

/***
 * 自定义一个代理类，需要实现InvocationHandler接口
 */
public class ProxyInvocationHandler implements InvocationHandler {
    /**
     * 代理类
     */
//被代理类代理的接口(出租房子的接口)
    private Rent rent;

    /***
     * set 注入
     * @param rent
     */
    public void setRent(Rent rent) {
        this.rent = rent;
    }

    /***
     * getProxy jdk
     * 通过 jdk 的这个 类（Proxy）
     * 调用 类加载器
     * 调用接口
     * 与当前类
     * @return
     */
    public Object getProxy() {
        return Proxy.newProxyInstance(this.getClass().getClassLoader(), rent.getClass().getInterfaces(), this);
    }


    /***
     *重写该方法，处理代理实例，返回结果
     * @param proxy
     * @param method
     * @param args
     * @return
     * @throws Throwable
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        lookHost();
        contract();
        Object invoke = method.invoke(rent, args);
        fee();
        return invoke;
    }


    //看房
    public void lookHost(){
        System.out.println("中介带你看房");
    }

    //签合同
    public void contract(){
        System.out.println("中介带你签合同");
    }

    //收中介费
    public void fee(){
        System.out.println("中介收中介费");
    }
}
