package com.xiang.pojo;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/23 12:33
 */
public class Host implements Rent {
    /**
     * 房东
     */
    public  void  rents(){
        System.out.println("房东出租房子");
    }

    /**
     * 外卖商家
     */
    @Override
    public void orders() {
        System.out.println("外卖商家卖东西");
    }

}
