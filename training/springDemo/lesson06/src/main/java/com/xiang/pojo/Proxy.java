package com.xiang.pojo;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/23 12:42
 */
//代理角色也有出租的功能
public class Proxy implements Rent {

    //但是代理角色需要代理房东的房子才能出租
    private Host host;

    public Proxy(Host host) {
        this.host = host;
    }

    //看房
    public void lookHost() {
        System.out.println("中介带你看房");
    }

    //签合同
    public void contract() {
        System.out.println("中介带你签合同");
    }

    //收中介费
    public void fee() {
        System.out.println("中介收中介费");
    }

    //售后
    public void after() {
        System.out.println("售后服务");
    }

    /**
     * 房东---中介
     */
    @Override
    public void rents() {
        host.rents();
        lookHost();
        contract();
        fee();
        after();
    }

    //    外卖小哥给你点餐
    public void order() {
        System.out.println("外卖小哥给你点餐");
    }

    //    外卖小哥给你送餐
    public void meals() {
        System.out.println("外卖小哥给你送餐");
    }

    //    外卖小哥给赚差价
    public void make() {
        System.out.println("外卖小哥给赚差价");
    }

    //    你给外卖小哥好评
    public void comment() {
        System.out.println("你给外卖小哥好评");
    }

    //    外卖小哥再接下一单
    public void under() {
        System.out.println("外卖小哥再接下一单");
    }


    /**
     * 外卖商家---中介  外卖小哥
     */
    @Override
    public void orders() {
        host.orders();
        order();
        meals();
        make();
        comment();
        under();
    }
}

