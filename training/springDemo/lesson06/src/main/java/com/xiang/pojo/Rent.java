package com.xiang.pojo;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/23 12:41
 */
public interface Rent {
    /**
     * 出租的接口
     */
    void  rents();

    /**
     * 外卖点餐接口
     */
    void  orders();
}
