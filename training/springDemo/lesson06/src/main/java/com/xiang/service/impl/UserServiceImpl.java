package com.xiang.service.impl;

import com.xiang.service.UserService;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/23 15:10
 */
public class UserServiceImpl implements UserService {
    @Override
    public void add() {
        System.out.println("增加");
        logos();
    }

    @Override
    public void delete() {
        System.out.println("删除");
        logos();
    }

    @Override
    public void update() {
        System.out.println("更新");
        logos();
    }

    @Override
    public void query() {
        System.out.println("查询");
        logos();
    }

    public void logos() {
        System.out.println("打印日志信息");
    }
}
