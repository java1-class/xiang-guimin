package com.xiang.service.proxy;

import com.xiang.service.UserService;
import com.xiang.service.impl.UserServiceImpl;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/23 15:12
 */
public class UserServiceProxy implements UserService {
    private UserServiceImpl userService;

    public UserServiceProxy(UserServiceImpl userService) {
        this.userService = userService;
    }

    @Override
    public void add() {
        userService.add();

    }

    @Override
    public void delete() {
        userService.delete();
    }

    @Override
    public void update() {
        userService.update();
    }

    @Override
    public void query() {
        userService.query();
    }
}
