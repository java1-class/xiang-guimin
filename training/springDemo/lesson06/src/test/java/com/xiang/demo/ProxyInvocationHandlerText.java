package com.xiang.demo;

import org.junit.Test;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/23 16:30
 */
public class ProxyInvocationHandlerText {
    @Test
    /**
     * 动态代理，
     */
    public  void  Test01(){
//        创建得到真实角色
        Host host = new Host();
//        得到可生成代理类的对象
        ProxyInvocationHandler handler = new ProxyInvocationHandler();
//        传入需要生成代理类的真实角色
        handler.setRent(host);
//        得到代理对象
        Rent proxy = (Rent) handler.getProxy();
//        测试
        proxy.rents();
    }
}
