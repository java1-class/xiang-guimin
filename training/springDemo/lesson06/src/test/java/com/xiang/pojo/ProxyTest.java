package com.xiang.pojo;

import org.junit.Test;
import org.w3c.dom.Node;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/23 12:45
 */
public class ProxyTest {
    /**
     * 房东--中介
     */
    @Test
    public  void  Test01(){
        Host host = new Host();
        host.rents();
    }

    /**
     * 房东--中介
     */
    @Test
    public  void  Test02(){
        Proxy proxy = new Proxy(new Host());
        proxy.rents();
        System.out.println("/--------------/");
        proxy.lookHost();
        System.out.println("/***********/");
        proxy.contract();
        System.out.println("/***********/");
        proxy.fee();
        System.out.println("/***********/");
        proxy.after();
    }

    /**
     * 外卖商家--外卖小哥
     */
    @Test
    public  void  Test03(){
        Proxy proxy = new Proxy(new Host());
        proxy.orders();
    }
}
