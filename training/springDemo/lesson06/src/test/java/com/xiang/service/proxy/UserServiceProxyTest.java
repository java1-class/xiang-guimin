package com.xiang.service.proxy;

import com.xiang.service.impl.UserServiceImpl;
import org.junit.Test;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/23 15:14
 */
public class UserServiceProxyTest {
    @Test
    public  void  Test01(){
        UserServiceProxy proxy = new UserServiceProxy(new UserServiceImpl());
        proxy.add();
        proxy.delete();
        proxy.update();
        proxy.query();
        System.out.println(proxy.toString());
    }
}
