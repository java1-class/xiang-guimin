# AOP

## 什么是AOP

1.  AOP 翻译过来就是面向切面编程
2.  AOP是维护的一种技术，底层使用的是动态代理，是Spring框架中一个重要内容
3.  作用：利用AOP 可以使业务逻辑之间的耦合度降低，提高程序的可重用性，同时提高开发的效率

> 理解：在原有的业务逻辑中添加验证参数、前置日志、后置日志等功能，并且要求不能改变原有的代码
>


## AOP在Spring中的作用

> 作用：提供声明式事务，允许用户自定义切面

> 名词了解：
>
> > 横切关注点：跨越应用程序多个模块的方法或功能。即是，与我们业务逻辑无关的，但是我们需要关注的部分，就是横切关注点。如日志 , 安全 , 缓存 , 事务等等 ....(就是你要横切进去的内容)
>
> > 切面（ASPECT）：横切关注点被模块化 的特殊对象。(就是需要横切进去的内容的类)
>
> > 通知（Advice）：切面必须要完成的工作。(就是类中的方法)
>
> > 目标（Target）：被通知对象。(就是插入的位置的对象)
>
> > 代理（Proxy）：向目标对象应用通知之后创建的对象。(代理对象)
>
> > 切入点（PointCut）：切面通知执行的 “地点”的定义。(就是插入的位置，比如某个方法)
>
> > 连接点（JointPoint）：与切入点匹配的执行点。(切入点和连接点基本是一个意思)
>
SpringAOP中，通过Advice定义横切逻辑，Spring中支持5种类型的Advice

前置增强：org.springframework.aop.BeforeAdvice代表前置增强，因为Spring只支持方法级的增强，故MethodBeforeAdvice是目前可实现的前置增强，**表示在目标方法执行前实施增强**，而BeforeAdvice是为了将来版本扩展需要而定义的；
后置增强：org.springframework.aop.AfterReturningAdvice代表后增强，**表示在目标方法执行后实施增强**；
环绕增强：org.aopalliance.intercept.MethodInterceptor代表环绕增强，**表示在目标方法执行前后实施增强**；(了解)
异常抛出增强：org.springframework.aop.ThrowsAdvice代表抛出异常增强，**表示在目标方法抛出异常后实施增强**；(了解)
引介增强：org.springframework.aop.InteoductionInterceptor代表引介增强，**表示在目标类中添加一些新的方法和属性**；(了解)


## execution切入点表达式

```
1.切入点表达式的作用：知道对哪个类里面的哪个方法进行增强
2.语法结构：
	execution([权限修饰符] [返回类型] [类的全路径][方法名](参数列表) [异常模式])
	除了返回类型、方法名和参数外，其它项都是可选的。
	* 代表所有
3.练习：
	(1)对com.bao.dao.UserDao类里面的add方法进行增强
		execution(* com.bao.dao.UserDao.add(..))
		修饰符类型省略，返回类型为任意类型，com.bao.dao.UserDao类的add方法，(..)表示参数
    (2)对com.bao.dao.UserDao类里面的所有的方法进行增强
    	execution(* com.bao.dao.UserDao.*(..))
    (3)对com.bao.dao包下的所有类里面的所有的方法进行增强
    	execution(* com.bao.dao.*.*(..))
```

