package com.xiang.diy;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/24 15:05
 */
@Component
//注解实现aop
@Aspect
public class AnnotationPointCut {
    @Before("execution(* com.xiang.service.impl.UserServiceImpl3.*(..))")
    public void before() {
        System.out.println("执行前");
    }

    @After("execution(* com.xiang.service.impl.UserServiceImpl3.*(..))")
    public void after() {
        System.out.println("执行后");
    }
}
