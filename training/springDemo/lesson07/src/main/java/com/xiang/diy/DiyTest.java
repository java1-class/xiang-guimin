package com.xiang.diy;

import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/24 14:55
 */
@Component
//方法二 自定义类实现aop  创建自定义类
public class DiyTest {
    public void before() {
        System.out.println("执行前");
    }

    public void after() {
        System.out.println("执行后");
    }

}
