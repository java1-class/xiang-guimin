package com.xiang.log;

import org.springframework.aop.AfterReturningAdvice;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/24 14:45
 */
@Component
////创建后置增强的日志类，需要实现
public class AfterLog implements AfterReturningAdvice {
    /**
     * o：返回值
     * * method:要执行的目标对象的方法
     * * objects:参数
     * * o1: 目标对象
     *
     * @param returnValue
     * @param method
     * @param args
     * @param target
     * @throws Throwable
     */
    @Override
    public void afterReturning(Object returnValue, Method method, Object[] args, Object target) throws Throwable {
        System.out.println("执行后置增强");
        System.out.println("执行的方法是=========>"+method.getName());
        System.out.println("执行的结果是=========>"+returnValue);
    }
}
