package com.xiang.log;

import org.springframework.aop.MethodBeforeAdvice;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/24 12:43
 */
//创建前置增强的日志类，需要实现
@Component
public class BeforLog implements MethodBeforeAdvice {
    /**
     * method:要执行的目标对象的方法
     * * objects:参数
     * * o：目标对象
     * @param method
     * @param args
     * @param target
     * @throws Throwable
     */
    @Override
    public void before(Method method, Object[] args, Object target) throws Throwable {
        System.out.println("执行前置增强");
        System.out.println("执行的类是=========>"+target.getClass().getName());
        System.out.println("执行的方法是=========>"+method.getName());
    }
}
