package com.xiang.service;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/24 12:38
 */
public interface UserService {
    void add();
    void delete();
    void update();
    void query();

}
