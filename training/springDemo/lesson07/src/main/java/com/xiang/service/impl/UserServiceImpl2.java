package com.xiang.service.impl;

import com.xiang.service.UserService;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/24 12:42
 */
@Component
public class UserServiceImpl2 implements UserService {
    @Override
    public void add() {
        System.out.println("增加");
    }

    @Override
    public void delete() {
        System.out.println("删除");
    }

    @Override
    public void update() {
        System.out.println("修改");
    }

    @Override
    public void query() {
        System.out.println("查询");
    }
}
