package com.xiang.service;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/24 13:16
 */
public class UserServiceImplTest {
    /**
     * 使用Spring实现AOP
     * 使用Spring的api接口
     */
    @Test
    public  void  Test01(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContent.xml");
        UserService service = context.getBean("userServiceImpl", UserService.class);
        service.add();
    }

    /**
     * 自定义类实现aop
     */
  @Test
    public  void  Test02(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContent.xml");
        UserService service = context.getBean("userServiceImpl2", UserService.class);
        service.add();
    }

    /**
     * 注解实现aop
     */
    @Test
    public  void  Test03(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContent.xml");
        UserService service = context.getBean("userServiceImpl3", UserService.class);
        service.delete();
    }

}
