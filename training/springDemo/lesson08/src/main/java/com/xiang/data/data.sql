-- 创建数据库
create database webapp1 charset utf8mb4;
-- 创建用户名、密码
create user'webapp1'@'localhost'identified by'webapp1';
-- 授权
grant all on webapp1.*to'webapp1'@'localhost';
-- 用用户名、密码登录
mysql -uwebapp1 -pwebapp1;

create table user
(
    id       int not null primary key auto_increment,
    username varchar(12),
    sex      varchar(2),
    age      int,
    birthday date
);


insert into user
    (`username`, `sex`, `age`, `birthday`)
values ("小向", "男", 19, "2000-1-1");


insert into user
    (`username`, `sex`, `age`, `birthday`)
values ("小向", "男", 19, "2000-1-1"),
       ("小李", "男", 19, "2000-1-1"),
       ("小张", "男", 19, "2000-1-1"),
       ("小五", "男", 19, "2000-1-1"),
       ("小三", "男", 19, "2000-1-1"),
       ("小二", "男", 19, "2000-1-1"),
       ("小周", "男", 19, "2000-1-1");