package com.xiang.mapper.impl;

import com.xiang.mapper.UserMapper;
import com.xiang.pojo.User;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/24 18:23
 */
@Component
public class UserMapperImpl implements UserMapper {
    @Autowired
    private SqlSessionTemplate sessionTemplate;

    @Override
    public List<User> queryAll() {
        UserMapper mapper = sessionTemplate.getMapper(UserMapper.class);

        return mapper.queryAll();
    }
}
