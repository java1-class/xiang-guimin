package com.xiang.mapper;

import com.xiang.pojo.User;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.Resource;

import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/24 17:21
 */
public class UserMapperTest {
    @Test
    public void queryAll() {
        int count = 1;
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserMapper mapper = context.getBean("userMapperImpl", UserMapper.class);
        List<User> list = mapper.queryAll();
        for (User user : list) {
            System.out.println("第"+count++ +"条记录");
            System.out.println(user);
        }
        System.out.println("/******************/");

    }

}
