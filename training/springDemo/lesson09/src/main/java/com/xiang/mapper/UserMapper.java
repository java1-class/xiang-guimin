package com.xiang.mapper;

import com.xiang.pojo.User;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/26 9:44
 */

public interface UserMapper {
    List<User> queryAll();
}
