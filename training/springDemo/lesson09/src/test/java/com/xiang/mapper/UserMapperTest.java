package com.xiang.mapper;

import com.xiang.pojo.User;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/26 9:52
 */
public class UserMapperTest {
    @Test
    public  void  Test01(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserMapper mapper = context.getBean("userMapper", UserMapper.class);
        List<User> list = mapper.queryAll();
        for (User user : list) {
            System.out.println(user);
        }
    }
}
