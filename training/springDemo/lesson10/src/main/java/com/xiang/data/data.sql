-- 创建数据库
create
    database webapp2 charset utf8mb4;
-- 创建用户名、密码
create
    user 'webapp2'@'localhost' identified by 'webapp2';
-- 授权
grant all
    on webapp2.*to 'webapp2'@'localhost';
-- 用用户名、密码登录
mysql -uwebapp2 -pwebapp2;
-- 创建表
create table account
(
    id    int not null primary key auto_increment,
    name  varchar(20),
    money DOUBLE(10, 2)
);



insert into account(`name`, money)
VALUES ("小向", "1000.00");