package com.xiang.maper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/26 10:53
 */
//@Component
public interface AccountMapper {
    int addMoney(@Param("id") int id, @Param("money") Integer money);

    int delMoney(@Param("id") int id, @Param("money") Integer money);

}
