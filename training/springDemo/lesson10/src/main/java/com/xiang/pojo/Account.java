package com.xiang.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/26 10:43
 */

@Component
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Account {
    /**
     * id    int not null primary key auto_increment,
     * name  varchar(20),
     * money DOUBLE(10, 2)
     */

    private int id;
    private String name;
    private int money;

}
