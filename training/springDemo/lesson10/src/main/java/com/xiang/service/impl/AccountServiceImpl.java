package com.xiang.service.impl;

import com.xiang.maper.AccountMapper;
import com.xiang.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/26 11:01
 */
@Service
@Transactional
/**
 * @Transactional
 * 这是事务的注解，可以加在类上，说明这个类的所有方法都被事务管理；
 * 也可以加在方法上，说明只有该方法被事务管理
 */
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountMapper accountMapper;

    @Override
    public void turnMoney() {
        accountMapper.addMoney(1,200);

//        异常
//        int a = 1/0;

        accountMapper.delMoney(2,200);

        accountMapper.addMoney(3,200);

        accountMapper.delMoney(4,200);
    }

}
