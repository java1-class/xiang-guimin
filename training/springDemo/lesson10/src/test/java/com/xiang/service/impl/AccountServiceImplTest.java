package com.xiang.service.impl;

import com.xiang.service.AccountService;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/26 11:17
 */
public class AccountServiceImplTest {
    @Test
    public  void  Test01(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        AccountService service = context.getBean("accountServiceImpl", AccountService.class);
        service.turnMoney();
        System.out.println(service);
        System.out.println("支付方：放款成功");
        System.out.println("收款方：收款成功");
    }
}
