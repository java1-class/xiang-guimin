package com.xiang.controller;

import com.xiang.pojo.User;
import com.xiang.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/26 19:06
 */
@Controller
@RequestMapping("/user")
public class UserControllerTest {

    @Autowired
    private UserServiceImpl userServiceImpl;

    @GetMapping("/queryAllUser")
    @ResponseBody
    public List<User> queryAllUser(){
        List<User> users = userServiceImpl.queryAllUser();
        for (User user : users) {
            System.out.println(user);
        }
        return users;
    }
}
