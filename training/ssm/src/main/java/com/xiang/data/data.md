```java
@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserServiceImpl userServiceImpl;

    @GetMapping("/queryAllUser")
    @ResponseBody
    public List<User> queryAllUser(){
        List<User> users = userServiceImpl.queryAllUser();
        for (User user : users) {
            System.out.println(user);
        }
        return users; 
    }
```
![img.png](img.png)


```java
public class UserServiceTest {
    @Test
    public  void  queryAllUser(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserService service = context.getBean("userServiceImpl", UserService.class);
        List<User> list = service.queryAllUser();
        for (User user : list) {
            System.out.println(user);
        }
    }
}
```
![img_1.png](img_1.png)

![img_2.png](img_2.png)

![img_3.png](img_3.png)
