-- 创建数据库
create
database webapp2 charset utf8mb4;
-- 创建用户名、密码
create
user'webapp2'@'localhost'identified by'webapp2';
-- 授权
grant all
on webapp2.*to'webapp2'@'localhost';
-- 用用户名、密码登录
mysql
-uwebapp2 -pwebapp2;
-- 创建表

-- `id`,`username`,`password`,`name`,`gender`,`age`,`address`,`qq`,`email`
create table user
(
    id       int primary key auto_increment,
    username varchar(50),
    password varchar(50),
    name     varchar(20) not null,
    gender   varchar(5),
    age      int,
    address  varchar(32),
    qq       varchar(20),
    email    varchar(50)
);

insert into user (`username`, `password`, `name`, `gender`, `age`, `address`, `qq`, `email`)
values ("xiang", "123456", "小向", "男", "19", "四川成都", "2103717751", "xiangdada1@gmail.com"),
       ("xiang", "123456", "小一", "男", "19", "四川成都", "2103717751", "xiangdada1@gmail.com"),
       ("xiang", "123456", "小二", "男", "19", "四川成都", "2103717751", "xiangdada1@gmail.com"),
       ("xiang", "123456", "小三", "男", "19", "四川成都", "2103717751", "xiangdada1@gmail.com"),
       ("xiang", "123456", "小四", "男", "19", "四川成都", "2103717751", "xiangdada1@gmail.com"),
       ("xiang", "123456", "小五", "男", "19", "四川成都", "2103717751", "xiangdada1@gmail.com"),
       ("xiang", "123456", "小六", "男", "19", "四川成都", "2103717751", "xiangdada1@gmail.com"),
       ("xiang", "123456", "小七", "男", "19", "四川成都", "2103717751", "xiangdada1@gmail.com"),
       ("xiang", "123456", "小八", "男", "19", "四川成都", "2103717751", "xiangdada1@gmail.com"),
       ("xiang", "123456", "小九", "男", "19", "四川成都", "2103717751", "xiangdada1@gmail.com");