package com.xiang.mapper;


import com.xiang.pojo.User;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/26 19:05
 */
@Component
public interface UserMapper {

    // 增
    int addUser(User user);

    // 删
    int deleteUser(int id);

    // 改
    int updateUser(User user);

    // 查所有
    List<User> queryAllUser();

    // 根据id查一个
    User queryUserById(int id);

    //条件查询
    List<User> conditionQuery(User user);



}
