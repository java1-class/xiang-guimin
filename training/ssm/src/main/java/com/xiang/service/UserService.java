package com.xiang.service;

import com.xiang.pojo.User;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/26 19:07
 */

public interface UserService {
    // 增
    int addUser(User user);

    // 删
    int deleteUser(int id);

    // 改
    int updateUser(User user);

    // 查所有
    List<User> queryAllUser();

    // 根据id查一个
    User queryUserById(int id);

    //条件查询
    List<User> conditionQuery(User user);

}
