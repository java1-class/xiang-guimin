<%--
  Created by IntelliJ IDEA.
  User: Xiang
  Date: 2021/9/26
  Time: 21:03
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>add</title>

    <!-- 新 Bootstrap 核心 CSS 文件 -->
    <link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.0/css/bootstrap.min.css">
    <!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
    <script src="http://cdn.bootcss.com/jquery/1.11.1/jquery.min.js"></script>
    <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
    <script src="http://cdn.bootcss.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

</head>
<body>
<div style="width: 1200px;margin: auto" >
    <div>
        <h4 align="center">添加用户</h4>
    </div>
    <form class="form-horizontal" action="${pageContext.request.contextPath}/user/add" method="post">
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">姓名</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="name" name="name" placeholder="姓名">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">性别</label>
            <div class="col-sm-10" style="margin-top: 3px;">
                男：<input type="radio" name="gender" value="男" checked="checked" >
                女：<input type="radio" name="gender" value="女">
            </div>
        </div>
        <div class="form-group">
            <label for="age" class="col-sm-2 control-label">年龄</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="age" name="age" placeholder="年龄">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">城市</label>
            <div class="col-sm-1">
                <select class="form-control input-sm" name="address">
                    <option value="成都">成都</option>
                    <option value="重庆">重庆</option>
                    <option value="北京">北京</option>
                    <option value="上海">上海</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="qq" class="col-sm-2 control-label">QQ</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="qq" name="qq" placeholder="QQ">
            </div>
        </div>
        <div class="form-group">
            <label for="email" class="col-sm-2 control-label">邮箱</label>
            <div class="col-sm-10">
                <input type="email" class="form-control" id="email" name="email" placeholder="邮箱">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-success">确认添加</button>

                <input type="button" onclick="history.go(-1)"  class="btn btn-warning"  value="取消添加"/>
            </div>
        </div>
    </form>
</div>

</body>
</html>

