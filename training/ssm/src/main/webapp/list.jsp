<%--
  Created by IntelliJ IDEA.
  User: Xiang
  Date: 2021/9/26
  Time: 21:02
  To change this template use File | Settings | File Templates.
    --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>list</title>

    <!-- 新 Bootstrap 核心 CSS 文件 -->
    <link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.0/css/bootstrap.min.css">
    <!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
    <script src="http://cdn.bootcss.com/jquery/1.11.1/jquery.min.js"></script>
    <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
    <script src="http://cdn.bootcss.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

</head>
<body>

<div style="width: 1200px;margin: auto">
    <div>
        <h4 align="center">用户展示</h4>
    </div>
    <div>
        <form style="float: left;margin: 5px;" class="form-inline"
              action="${pageContext.request.contextPath}/user/conditionQuery" method="get">
            <div class="form-group">
                <label for="name">姓名</label>
                <input type="text" name="name" class="form-control" id="name">
            </div>
            <div class="form-group">
                <label for="gender">性别</label>
                <input type="text" name="gender" class="form-control" id="gender">
            </div>
            <div class="form-group">
                <label for="address">城市</label>
                <input type="text" name="address" class="form-control" id="address">
            </div>

            <button type="submit" class="btn btn-info">查询</button>
        </form>

        <a style="float: right;margin: 5px;" class="btn btn-primary"
           href="${pageContext.request.contextPath}/index.jsp">欢迎页面</a>
        <a style="float: right;margin: 5px;" class="btn btn-primary"
           href="${pageContext.request.contextPath}/user/list">用户列表页</a>
        <a style="float: right;margin: 5px;" class="btn btn-primary" href="${pageContext.request.contextPath}/add.jsp">添加联系人</a>
    </div>

    <%--    <table class="table table-hover">--%>
    <table class="table table-striped">
        <tr>
            <th>序号</th>
            <th>ID</th>
            <th>账号</th>
            <th>密码</th>
            <th>姓名</th>
            <th>性别</th>
            <th>年龄</th>
            <th>城市</th>
            <th>qq</th>
            <th>邮箱</th>
            <th>操作</th>
        </tr>
        <c:forEach items="${users}" var="user" varStatus="s">
            <tr>
                <td>${s.count}</td>
                <td>${user.id}</td>
                <td>${user.username}</td>
                <td>***</td>
                <td>${user.name}</td>
                <td>${user.gender}</td>
                <td>${user.age}</td>
                <td>${user.address}</td>
                <td>${user.qq}</td>
                <td>${user.email}</td>
                <td>
                    <a href="${pageContext.request.contextPath}/user/delete/${user.id}" class="btn btn-danger">删除</a>
                    <a href="${pageContext.request.contextPath}/user/queryUserById/${user.id}" class="btn btn-success">修改</a>
                </td>
            </tr>
        </c:forEach>

    </table>

    <%--分页导航标签--%>
    <div class="row">
        <div class="col-md-6">
            第${pageInfo.pageNum}页，共${pageInfo.pages}页，共${pageInfo.total}条记录
        </div>
        <div class="col-md-6 offset-md-4">
            <nav aria-label="Page navigation example">
                <ul class="pagination pagination-sm">
                    <li class="page-item"><a class="page-link"
                                             href="${pageContext.request.contextPath}/user/list?page=1">首页</a></li>
                    <c:if test="${pageInfo.hasPreviousPage}">
                        <li class="page-item"><a class="page-link"
                                                 href="${pageContext.request.contextPath}/user/list?page=${pageInfo.pageNum-1}">上一页</a>
                        </li>
                    </c:if>
                    <c:forEach items="${pageInfo.navigatepageNums}" var="page">
                        <c:if test="${page==pageInfo.pageNum}">
                            <li class="page-item active"><a class="page-link" href="#">${page}</a></li>
                        </c:if>
                        <c:if test="${page!=pageInfo.pageNum}">
                            <li class="page-item"><a class="page-link"
                                                     href="${pageContext.request.contextPath}/user/list?page=${page}">${page}</a>
                            </li>
                        </c:if>
                    </c:forEach>
                    <c:if test="${pageInfo.hasNextPage}">
                        <li class="page-item"><a class="page-link"
                                                 href="${pageContext.request.contextPath}/user/list?page=${pageInfo.pageNum+1}">下一页</a>
                        </li>
                    </c:if>
                    <li class="page-item"><a class="page-link"
                                             href="${pageContext.request.contextPath}/user/list?page=${pageInfo.pages}">末页</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>


</div>

</body>
</html>

