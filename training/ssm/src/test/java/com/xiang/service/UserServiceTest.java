package com.xiang.service;

import com.xiang.pojo.User;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/26 19:49
 */
public class UserServiceTest {
    @Test
    public  void  queryAllUser(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserService service = context.getBean("userServiceImpl", UserService.class);
        List<User> list = service.queryAllUser();
        for (User user : list) {
            System.out.println(user);
        }
    }

}
