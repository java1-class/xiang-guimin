package com.xiang.controller;

import com.xiang.pojo.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by IntelliJ IDEA.
 * User: xiang
 * Date: 2021/9/13 10:31
 */
@Controller
public class ViewController {
    private static  User user = new User();

    @RequestMapping("/view")
    public String toView(Model model) {

        user.setId(123465);
        user.setUsername("小向");
        model.addAttribute("user",user);
        System.out.println("user:"+user);
        return  "view/user";
    }
}
