# Vue (读音 /vjuː/，类似于 view) 是一套用于构建用户界面的渐进式框架。

## Vue 被设计为可以自底向上逐层应用。

```html
<!-- 开发环境版本，包含了有帮助的命令行警告 -->
<script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
```

## Vue.js 的核心是一个允许采用简洁的模板语法来声明式地将数据渲染进 DOM 的系统：